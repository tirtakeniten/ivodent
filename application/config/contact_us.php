<?php

/* FROM */
$config['contact_email_from'] = 'no-reply@ivodent.com';
$config['contact_name_from'] = 'Ivodent';

/* REPLY TO */
$config['contact_email_reply'] = 'tirta@balibagus.com';

/* ADMIN */
$config['contact_admin_to'] = 'tirta@balibagus.com';