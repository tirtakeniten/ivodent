<?php

/* dashboard */
$config['url']['admin']['_base'] = 'admin/';
$config['url']['admin']['home'] = 'home/';

/* Products */
$config['url']['admin']['product'] = 'product/';
$config['url']['admin']['product_form'] = 'product/form/';
$config['url']['admin']['product_delete'] = 'product/delete/';
$config['url']['admin']['product_varian'] = 'varians/index/';
$config['url']['admin']['product_varian_form'] = 'varians/form/';
$config['url']['admin']['product_varian_delete'] = 'varians/delete/';

/* Category */
$config['url']['admin']['category'] = 'category/';
$config['url']['admin']['category_form'] = 'category/form/';
$config['url']['admin']['category_delete'] = 'category/delete/';
$config['url']['admin']['category_detail'] = 'category/detail/';

/* Setting */
$config['url']['admin']['setting'] = 'setting/index/';

/* pages */
$config['url']['admin']['page'] = 'page';
$config['url']['admin']['page_form'] = 'page/form/';
$config['url']['admin']['page_delete'] = 'page/delete/';

/* media */
$config['url']['media']['_base'] = 'media/manager/';
$config['url']['media']['form'] = 'form/';
$config['url']['media']['upload'] = 'upload/';

/* order */
$config['url']['admin']['order'] = 'order/';
$config['url']['admin']['order_details'] = 'order/details/';
$config['url']['admin']['print_order'] = 'order/print_order/';