<?php

class Account extends Member_Controller
{
    var $user;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'user_model',
            'user_address_model',
            'order_model',
            'order_detail_model'
        ));
        $this->load->helper(array(
            'config/date'
        ));
        $this->user = $this->auth->user();
    }
    
    public function index()
    {
        $data['account'] = $this->user;
        $data['primary_address'] = $this->user_address_model->get_by(array(
            'user_id' => $data['account']->id,
            'default' => 1
        ));
        
        $this->stencil->data($data);
        
        $this->stencil->paint('account');
    }
    
    /**
     * Account update
     * URI: account/update
     **/
    public function update()
    {
        $data['account'] = $this->user_model->parse_username($this->user);
        $this->stencil->data($data);
        
        if ($this->input->method() == 'get') {
            
            $this->stencil->paint('account-update');
            
        } elseif ($this->input->method() == 'post') {
            
            $this->load->library('form_validation');
            
            /* user data validation */
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('lname', 'Last Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            
            if ($this->form_validation->run() == FALSE){
                $this->stencil->paint('account-update');
            } else {
                $user = array(
                    'username' => $this->input->post('fname').' '.$this->input->post('lname'), 
                	'email'    => $this->input->post('email')
                );
                $this->user_model->update($data['account']->id, $user);
                $this->session->set_flashdata('success', 'Your account has been updated.');
                redirect('account');
            }
            
        } else {
            show_404();
        }
    }
    
    /**
     * Account list address
     * URI: account/address
     **/
    public function address()
    {
        $data['account'] = $this->user;
        $data['addresses'] = $this->user_address_model->order_by('default', 'desc')
                            ->get_many_by(array(
                                'user_id' => $data['account']->id
                            ));
        $this->stencil->data($data);
        $this->stencil->paint('address');
    }
    
    /**
     * Account Address Form (add|edit)
     * URI: account/address-form
     **/
    public function address_form ($address_id = NULL)
    {
        if($address_id == NULL)
        {
            $data['page_title'] = 'Add Address';
        }else{
            $data['page_title'] = 'Update Address';
        }
        
        $data['account'] = $this->user;
        $data['address'] = $this->user_address_model->get_by(array(
            'user_id' => $data['account']->id,
            'id' => $address_id
        ));
        $data['address'] = $this->user_address_model->parse_username($data['address']);
        $this->stencil->data($data);
        
        if ($this->input->method() == 'get') {
            
            $this->stencil->paint('account-address-form');
            
        } elseif ($this->input->method() == 'post') {
            
            $this->load->library('form_validation');
            
            /* address data validation */
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('lname', 'Last Name', 'required');
            $this->form_validation->set_rules('company', 'Company');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('post_code', 'Post Code', 'required');
            $this->form_validation->set_rules('province', 'Province', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('phone', 'Phone');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            
            if ($this->form_validation->run() == FALSE){
                $this->stencil->paint('account-address-form');
            } else {
                $user_address = array(
                    'user_id'  => $this->input->post('user_id'),
                	'name'     => $this->input->post('fname').' '.$this->input->post('lname'), 
                	'company'  => $this->input->post('company'), 
                	'address'  => $this->input->post('address'),
                	'city'     => $this->input->post('city'), 
                	'post_code'=> $this->input->post('post_code'), 
                	'province' => $this->input->post('province'), 
                	'country'  => $this->input->post('country'), 
                	'email'    => $this->input->post('email'), 
                	'phone'    => $this->input->post('phone')
                );
                
                if ($this->input->post('id') == '') {
                    
                    $this->user_address_model->insert($user_address);
                    
                    $this->session->set_flashdata('success', 'Your address has been saved.');
                } else {
                    
                    $this->user_address_model->update($this->input->post('id'), $user_address);
                    
                    $this->session->set_flashdata('success', 'Your address has been updated.');
                }
                
                if ($this->input->get('redirect') != '') {
                    redirect($this->input->get('redirect'));
                } else {
                    redirect('account');
                }
            }
            
        } else {
            show_404();
        }
    }
    
    public function delete_address()
    {
        
    }
    
    public function address_default ($address_id) {
        /* remove all default address */
        $data['account'] = $this->user;
        $this->user_address_model->update_by(array(
            'user_id' => $this->user->id,
            'default' => 1
        ), array(
            'default' => 0
        ));
        $this->user_address_model->update_by(array(
            'user_id' => $this->user->id,
            'id' => $address_id
        ), array(
            'default' => 1
        ));
        $this->session->set_flashdata('success', 'Default address have been saved');
        redirect('account/address');
    }
    
    public function wishlist()
    {
        
    }
    
    public function orders()
    {
        $data['account'] = $this->user;
        $data['orders'] = $this->order_model
                                ->with('status')
                                ->order_by('id', 'desc')
                                ->get_many_by(array(
                                    'user_id' => $this->user->id
                                ));
        
        $this->stencil->data($data);
        
        $this->stencil->paint('account-orders');
    }
    
    public function order ($id = NULL) {
        
        if ($id == NULL) {
            redirect('account/orders');
        }
        
        $data['account'] = $this->user;
        $data['order'] = $this->order_model
                        ->with('user')
                        ->with('status')
                        ->get_by(array(
                            'id' => $id,
                            'user_id' => $data['account']->id
                        ));
        
        $data['order_details'] = $this->order_detail_model
                        ->get_by_order($data['order']->id);
        
        $this->stencil->data($data);
        
        $this->stencil->paint('account-order');
    }
    
    public function change_password () {
        if ($this->input->method() == 'get') {
            $this->stencil->paint('account-change-password');
        } elseif($this->input->method() == 'post') {
                        
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('password', 'Current Password', array(
                'required',
                array('check_password', function ($str) {
                    $password = $this->auth->decrypt($this->user->password);
                    return $password == $str;
                }),
            ), array(
                'check_password' => 'Your password is incorrect.'
            ));
            
            $this->form_validation->set_rules('new_password', 'New Password', array(
                'required',
                'differs[password]'
            ));
            
            $this->form_validation->set_rules('cofirm_new_password', 'Confirm New Password', array(
                'required',
                'matches[new_password]'
            ));
            
            if ($this->form_validation->run() == FALSE) {
                $this->stencil->paint('account-change-password');
            } else {
                $this->user_model->update($this->user->id, array(
                    'password' => $this->input->post('cofirm_new_password')
                ));
                $this->session->set_flashdata('success', 'Your new password is saved.');
                
                // force login after change password
                $user = array(
                    $this->user->email,
                    $this->input->post('new_password'),
                );
                $auth = $this->auth->start($user);
                // force login after change password
                
                redirect('account');
            }
        }
    }
}