<?php

class Account_login extends Front_Controller
{    
    public function __construct()
    {
        parent::__construct();        
        $this->load->library('auth');
        self::_SCAN();
    }
    
    public function index()
    {
        /* Redirection */
        $redirect = $this->input->get('redirect');
        $data['redirect'] = $redirect;
        $data['seo_title'] = 'Login - '.get_site_config('default_title');
        
        /* if post */
        if( $this->input->method() == 'post' )
        {
            /* posted variable */
            $user = array(
                $this->input->post('email'),
                $this->input->post('password'),
            );
            
            /* start authentication */
            $auth = $this->auth->start($user);
            
            /* if valid */
            if($auth)
            {                
                if(empty($redirect))
                {
                    redirect('account');
                }
                
                redirect($redirect);
            }else{
                
                /* user not valid */
                $data['message'] = $this->auth->message();
                //$this->load->view($this->view_file, $data);
                $this->stencil->paint('account-login', $data);
            }
            
        /* else */
        }else{
            $this->stencil->paint('account-login', $data);
        }
    }
    
    protected function _SCAN()
    {
        /* restart */
        if( $this->auth->restart() )
        {
            $redirect = $this->input->get('redirect');
            if(empty($redirect))
            {
                redirect('account');
            }
            redirect($redirect);
        }else{
            
        }
    }
}