<?php

class Cart extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('cart_factory');
    }
    
    public function index()
    {
        $data['cart_factory'] = $this->cart_factory;
        $data['cart_contents'] = $this->cart_factory->contents(TRUE);
        
        $data['seo_title'] = 'Cart - '.get_site_config('default_title');
        
        $this->stencil->data($data);
        $this->stencil->paint('cart');
    }
    
    public function add()
    {
        if ($this->input->method() == 'post') {
            try {
                $cart_insert = $this->cart_factory->insert(
                    $this->input->post('product_id'), 
                    $this->input->post('qty')
                );
            } catch (Exception $e) {
                 $this->session->set_flashdata('error', $e->getMessage());
            }
            
            $this->session->set_flashdata('success', 'A product added to your cart.');
            redirect('cart');
        }
    }
    
    public function update () {
        
        if ($this->input->method() == 'post') {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('qty[]', 'Quantity', 'required|greater_than_equal_to[1]');
            
            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', validation_errors(PHP_EOL, PHP_EOL));
                redirect('cart');
            }
            
            foreach ($this->input->post('qty') as $rowid => $qty) {
                $this->cart->update(array(
                    'rowid'   => $rowid,
                    'qty'     => $qty
                ));
            }
            $this->session->set_flashdata('success', 'Your shopping cart has been updated.');
            
        } else {
            if ($this->input->get('rowid')) {
                $this->cart->update(array(
                    'rowid'   => $this->input->get('rowid'),
                    'qty'     => 0
                ));
                $this->session->set_flashdata('success', 'A product has been removed from your shopping cart.');
            }
        }
        
        redirect('cart');
    }
}