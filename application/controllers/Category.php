<?php

class Category extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'category_model',
            'product_model',
            'product_rel_category_model'
        ));
    }

    public function index($slug = NULL)
    {
        /**
         * ACTIVATE WHEN START CODING
         *
         ***/
        $category = $this->category_model
                            ->get_by(array(
                                'slug' => $slug
                            ));
        $category_hierarchy = $this->category_model
                                ->get_hierarchical($category->id);
        
        if(isset($category_hierarchy[0]) && count($category_hierarchy[0]) > 0)
        {
            $this->_list($slug);
        }else{
            $this->_details($slug);
        }

    }

    private function _list($slug)
    {
        $data['category'] = $this->category_model
            ->with_parent()
            ->get_by(array(
                'slug' => $slug
            ));
        
        $seo = array(
            'seo_title' => $data['category']->seo_title.' - '.get_site_config('default_title'),
            'seo_description' =>  $data['category']->seo_description,
            'seo_og_image' => @$data['category']->featured_image_url['medium'],
            'twitter_image' => @$data['category']->featured_image_url['original'],
        );
        
        $data = array_merge($data, $seo);
        
        $data['current_category'] = $this->category_model->get_hierarchical($data['category']->id);  
        $data['category_tree'] = $this->category_model->get_hierarchical();  
        
        $this->stencil->data($data);

        $this->stencil->paint('category');
    }

    private function _details($slug)
    {        
        /* category */
        $data['category'] = $this->category_model
            ->with_parent()
            ->get_by(array(
                'slug' => $slug
            ));
        
        $seo = array(
            'seo_title' => $data['category']->seo_title.' - '.get_site_config('default_title'),
            'seo_description' =>  $data['category']->seo_description,
            'seo_og_image' => $data['category']->featured_image_url['medium'],
            'twitter_image' => $data['category']->featured_image_url['original'],
        );
        
        $data = array_merge($data, $seo);
        
        if (isset($data['category']->parent)) {
            $data['current_category'] = $this->category_model->get_hierarchical($data['category']->parent->id);
        } else {
            $data['current_category'] = array();
        }
        $data['category_tree'] = $this->category_model->get_hierarchical();    
        
        
        /* products of category */
        $product_ids = $this->product_rel_category_model->product_by_category_id($data['category']->id);
        
        if(count($product_ids) > 0)
        {
            $this->db->where_in('id', $product_ids);
            $data['products'] = $this->product_model->get_all();    
        }
        
        /* other category */
        $this->db->where_not_in('id', array( $data['category']->id ));
        $data['other_categories'] = $this->category_model->get_all();

        /* data */
        $this->stencil->data($data);

        /* view */
        $this->stencil->paint('category-detail');
    }
}
