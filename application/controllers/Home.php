<?php

class Home extends Front_Controller
{
    var $featureProductID = 7;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'product_rel_category_model',
            'product_model',
            'page_model'
        ));
    }

    public function index()
    {
        /* Featured products */
        $featuredProductID = $this->product_rel_category_model
                                    ->product_by_category_id($this->featureProductID);
        if (count($featuredProductID) > 0) {
            $this->db->where_in('id', $this->featureProductID);
            $data['featured_products'] = $this->product_model->get_all();
        }else{
            $data['featured_products'] = $this->product_model
                                                ->limit(6)
                                                ->get_all();
        }

        /* Latest products */
        $data['latest_products'] =
            $this->product_model->order_by('created_at', 'desc')
                                ->limit(4)
                                ->get_all();

        /* data */
        $this->stencil->data($data);

        /* view */
        $this->stencil->paint('home');
    }

    public function page($slug = NULL)
    {
        $data['page'] = $this->page_model->get_by(array(
            'slug' => $slug
        ));
        
        $seo = array(
            'seo_title' => $data['page']->title.' - '.get_site_config('default_title'),
            'seo_description' =>  $data['page']->seo_description,
            'seo_og_image' => $data['page']->featured_image_url['medium'],
            'twitter_image' => $data['page']->featured_image_url['original'],
        );
        
        $data = array_merge($data, $seo);
        
        /* Featured products */
        $featured_id = $this->featureProductID;
        $featuredProductID = $this->product_rel_category_model
                                    ->product_by_category_id($featured_id);
        if (count($featuredProductID) > 0) {
            $this->db->where_in('id', $featuredProductID);
            $data['featured_products'] = $this->product_model->get_all();
        }else{
            $data['featured_products'] = $this->product_model
                                                ->limit(6)
                                                ->get_all();
        }
        
        //echo VIEWPATH.'pages/page-'.$data['page']->slug.'.php';
        
        if (file_exists(VIEWPATH.'pages/page-'.$data['page']->slug.'.php')) {
            $this->stencil->paint('page-'.$data['page']->slug, $data);
        } else {
            $this->stencil->paint('home-page', $data);
        }
    }
    
    public function terms_and_conditions()
    {
        echo "<p>Lorem ipsum dolor sit amet, iudicabit gloriatur persequeris an duo, in zril quaestio his, sit cu amet tollit.</p>";
        echo "<p>At vide delicata sit, nec ei illud appetere, cibo sonet facilisi ad sit. In graecis propriae vim, est eu enim habeo.</p>";
        echo "<p>Wisi dolorem vim at, qui quando graeco at. Adolescens adipiscing philosophia cu has, ne vix quodsi veritus.</p>";
        echo "<p>Semper habemus ei mei, vidit ignota per id, ut eam graeci perpetua philosophia.</p>";
    }

    public function test()
    {
        $httpAdapter = new \Ivory\HttpAdapter\FileGetContentsHttpAdapter();
        
        // Create the Yahoo Finance provider
        $yahooProvider = new \Swap\Provider\GoogleFinanceProvider($httpAdapter);
        
        // Create Swap with the provider
        $swap = new \Swap\Swap($yahooProvider);
        
        $rate = $swap->quote('USD/IDR');

        // 1.187220
        echo $rate.'<br />';
        
        // 1.187220
        echo $rate->getValue().'<br />';
        
        // 15-01-11 21:30:00
        echo $rate->getDate()->format('Y-m-d H:i:s');
    }
}
