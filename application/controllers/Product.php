<?php

class Product extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model(array(
            'category_model',
            'product_model',
            'product_rel_category_model',
            'varian_model',
            'wish_model'
        ));
    }
    
    public function index($slug = NULL, $varian = NULL)
    {
        if($slug != NULL)
        {
            if($varian == NULL)
            {
                $this->_detail($slug);
            }else{
                $this->_varian($varian, $slug);
            }    
        }else{
            redirect('home');
        }
    }
    
    private function _detail( $slug )
    {
        $data['product'] = $this->product_model->get_by(array(
            'slug' => $slug
        ));
        
        if (is_null($data['product'])) show_404();
        
        $data['varians'] = $this->varian_model->get_many_by(array(
            'product_id' => $data['product']->id
        ));
        
        $data['in_wishlist'] = $this->wish_model->is_exist($data['product']->id);
        
        $this->db->where_not_in('id', array( $data['product']->id ));        
        $data['related_products'] = $this->product_model->limit(3)
                                        ->order_by('id', 'random')
                                        ->get_all();
        $seo = array(
            'seo_title' => $data['product']->seo_title.' - '.get_site_config('default_title'),
            'seo_description' =>  $data['product']->seo_description,
            'seo_og_image' => @$data['product']->images[0]['medium'],
            'twitter_image' => @$data['product']->images[0]['original'],
        );
        
        $data = array_merge($data, $seo);
        
        $this->stencil->data($data);
        
        $this->stencil->paint('product');
    }

    public function i($id) {
        $product = $this->product_model->get($id);
        redirect(site_url('product/'.$product->slug));
    }

    private function _varian( $id , $slug)
    {
        
    }
}