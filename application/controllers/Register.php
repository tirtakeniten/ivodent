<?php

class Register extends Front_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
        $this->load->model(array(
            'user_model',
            'user_address_model'
        ));
    }
    
    public function index()
    {        
        $authMember = $this->auth->restart();
        if ($authMember) {
            redirect('account');
        }
        
        $data['seo_title'] = 'Register - '.get_site_config('default_title');
        
        $this->load->library('form_validation');
        
        if($this->input->method() == 'get')
        {
            $this->stencil->paint('register');
        }
        elseif($this->input->method() == 'post')
        {
            $validation = $this->form_validation;            
            
            /* user data validation */
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('lname', 'Last Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'Password', 'required');
            
            /* address data validation */
            $this->form_validation->set_rules('company', 'Company');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('post_code', 'Post Code', 'required');
            $this->form_validation->set_rules('province', 'Province', 'required');
            $this->form_validation->set_rules('phone', 'Phone');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('agree_term', 'Term Agree', 'required', array(
                'required' => 'You must agree our Terms and Conditions'
            ));
            
            if( $this->form_validation->run() == FALSE )
            {
                $this->stencil->paint('register');
            }else{
                $user = array(
                    'username' => $this->input->post('fname').' '.$this->input->post('lname'), 
                	'email'    => $this->input->post('email'), 
                	'role'     => 'member',
                	'password' => $this->input->post('password')
                );
                $user_id = $this->user_model->insert($user);
                
                $user = $this->user_model->get($user_id);
                
                $user_address = array(
                    'user_id'  => $user->id, 
                	'name'     => $user->username, 
                	'company'  => $this->input->post('company'), 
                	'address'  => $this->input->post('address'),
                	'city'     => $this->input->post('city'), 
                	'post_code'=> $this->input->post('post_code'), 
                	'province' => $this->input->post('province'), 
                	'country'  => $this->input->post('country'), 
                	'email'    => $user->email, 
                	'phone'    => $this->input->post('phone'), 
                	'default'  => 1
                );
                
                $user_address_id = $this->user_address_model->insert($user_address);
                
                $this->session->set_flashdata('new_user', 'new_user');
                
                redirect('register/success');
            }
        }else{
            show_404();
        }    
    }
    
    public function success()
    {
        if (!$this->session->flashdata('new_user')) {
            redirect('register');
        }
        
        $this->stencil->paint('register-success');
    }
}