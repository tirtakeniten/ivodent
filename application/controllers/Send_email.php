<?php

class Send_email extends CI_Controller {
    
    public function __construct() {
        parent::__construct(); 
        $this->load->helper(array(
            'config/date' 
        ));   
    }
    
    public function index () {
        
    }
    
    public function contact_us () {
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('name', 'Your name', 'required');
        $this->form_validation->set_rules('email', 'Your Email', 'required');
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');
        $this->form_validation->set_rules('captcha', 'CAPTCHA', array(
            'required',
            array(
                'check_captcha', 
                function ($str) {
                    $strCaptcha = $this->session->flashdata('captcha_str');
                    return TRUE;
                    return $str == $strCaptcha;
                }
            )
        ), array(
            'check_captcha' => 'Invalid CAPTSCHA string'
        ));
        
        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('error', validation_errors());
            redirect('page/contact-us');
            
        } else {
            
            $to_admin = $this->load->view('mail/contact_to_admin', array(
                'data' => $this->input->post()
            ), TRUE);
            
            $to_customer = $this->load->view('mail/contact_to_customer', array(
                'data' => $this->input->post()
            ), TRUE);
            
            $this->load->library('email');
            
            /* to customer */
            $this->email->mailtype = 'html';
            $this->email->from($this->config->item('contact_email_from'), $this->config->item('contact_name_from'));
            $this->email->to($this->input->post('email'));
            $this->email->reply_to($this->config->item('contact_email_reply'));
            
            $this->email->subject('[Ivodent] Thank you for contacting us');
            $this->email->message($to_customer);
            
            $this->email->send();
            
            /* to admin */
            $this->email->initialize(array());
            $this->email->mailtype = 'html';
            $this->email->from($this->config->item('contact_email_from'), $this->input->post('name'));
            $this->email->to($this->config->item('contact_admin_to'));
            $this->email->reply_to($this->input->post('email'));
            
            $this->email->subject('[Ivodent] '.$this->input->post('subject'));
            $this->email->message($to_admin);
            
            $this->email->send();
            
            $this->session->set_flashdata('success', 'Your message has been sent.');
            
            redirect('page/contact-us');
        }
    }
    
}