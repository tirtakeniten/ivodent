<?php

class Wishlist extends Member_Controller {
    
    public $user;
    public $ref;
    
    public function __construct () {
        parent::__construct();
        $this->user = $this->auth->user();
        $this->load->model(array(
            'product_model',
            'wish_model'
        ));
        $this->load->library(array(
            'user_agent'
        ));
    }    
    
    public function index () {
        $data['seo_title'] = 'Your Wishlist - '.get_site_config('default_title');
        $data['user'] = $this->user;
        $data['wishlist'] = $this->wish_model->with('product')
            ->get_many_by(array(
                'user_id' => $data['user']->id
            ));
        $this->stencil->data($data);
        $this->stencil->paint('wishlist');
    }
    
    public function add ($id = NULL) {
        if (is_null($id)) {
            show_404();
        } else {
            $product = $this->product_model->get($id);
            if (is_null($product)) {
                show_404();
            } else {
                $wish = array(
                    'user_id' => $this->user->id,
                    'product_id' => $product->id
                );
                
                if (!$this->wish_model->is_exist($product->id)) {
                    $this->wish_model->insert($wish);
                }
                
                $this->session->set_flashdata('success', 'This product is in your wishlist.');
                
                redirect('product/'.$product->slug);
            }
        }        
    }
    
    public function remove ($id = NULL)
    {
        if (is_null($id)) show_404();
        
        $wish = $this->wish_model->get($id);
        
        if ($this->wish_model->is_exist($wish->product_id)) {
            $this->wish_model->delete($id);
        }
        
        $this->session->set_flashdata('success', 'An item has been removed from your wishlist.');
                
        redirect('wishlist');
    }
}