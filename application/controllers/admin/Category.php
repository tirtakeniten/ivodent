<?php

class Category extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model');
    }
    
    public function index()
    {
        /* page_title */
        $data['page_title'] = 'All Categories';
        
        /* the categories */
        $data['categories'] = $this->category_model->with_parent()->get_all();
        
        /* the category tree */
        $data['category_tree'] = $this->category_model->get_hierarchical();        
        
        /* data */
        $this->stencil->data($data);
        
        /* paint */
        $this->stencil->paint($this->view_prefix.'category');
    }
    
    public function form( $id = NULL )
    {
        /*
        $cats = $this->category_model->get_hierarchical();
        echo '<textarea>';
        print_r($cats);
        exit;
        */
        
        if($id == NULL)
        {
            /* page_title */
            $data['page_title'] = 'Add New Category';
        }
        else
        {
            /* page_title */
            $data['page_title'] = 'Edit Category';
            $data['category'] = $this->category_model->get($id);
        }
        
        $data['parent_categories'] = $this->category_model->list_dropdown($id);
                
        $this->stencil->data($data);
        if( $this->input->method() == 'get' )
        {
            /* paint */
            $this->stencil->paint($this->view_prefix.'category-form');
        }
        elseif($this->input->method() == 'post')
        {
            /* validation */
            $this->load->library('form_validation');
            
            /* validation rules */
            $this->form_validation->set_rules(array(
                'code' => array(
                    'field' => 'code',
                    'label' => 'Code',
                    'rules' => 'required'
                ), 
            	'name' => array(
                    'field' => 'name',
                    'label' => 'Category name',
                    'rules' => 'required'
                ), 
            	'description' => array(
                    'field' => 'description',
                    'label' => 'Description',
                ), 
            	'seo_title' => array(
                    'field' => 'seo_title',
                    'label' => 'SEO Title'
                ), 
            	'seo_description' => array(
                    'field' => 'seo_description',
                    'label' => 'SEO Description'
                ), 
            	'seo_keyword' => array(
                    'field' => 'seo_keyword',
                    'label' => 'SEO Keyword'
                ),
            ));
            
            if($this->form_validation->run() === FALSE)
            {
                /* data not valid */
                $this->stencil->paint($this->view_prefix.'category-form');
            }else{
                
                /* Data */
                $value = array(
                    'code'           => $this->input->post('code'), 
                    'parent_id'      => $this->input->post('parent_id'), 
                	'name'           => $this->input->post('name'), 
                	'description'    => $this->input->post('description'),
                    'slug'           => $this->input->post('slug'),
                    'featured_image' => $this->input->post('featured_image'),
                	'seo_title'      => $this->input->post('seo_title'),  
                	'seo_description'=> $this->input->post('seo_description'),  
                	'seo_keyword'    => $this->input->post('seo_keyword'), 
                	'created_at'     => date("Y-m-d H:i:s"),  
                	'updated_at'     => date("Y-m-d H:i:s"),  
                );
                
                if($this->input->post('id') == '')
                {
                    /* insert */
                    $this->category_model->insert($value);
                    $this->session->set_flashdata('success', 'A category has been created.');
                }else{
                    
                    /* update */
                    unset($value['created_at']);
                    $this->category_model->update($this->input->post('id'), $value);
                    $this->session->set_flashdata('success', 'Category has been updated.');
                }
                redirect($this->url->admin('category'));
            }
        }else{
            /* not found */
            show_404();
        }
    }
    
    public function delete( $id = NULL )
    {
        if($id == NULL)
        {
            /* no ID */
            $this->session->set_flashdata('error', 'Undefined category ID.');
        }else{
            
            /* delete!!! */
            $this->category_model->delete_rel_products($id);
            $this->category_model->delete($id);
            $this->session->set_flashdata('success', 'Category has been deleted.');
        }
        redirect($this->url->admin('category'));
    }
    
    public function detail($id)
    {
        $category = $this->category_model->get($id);
        echo json_encode($category);
    }
}