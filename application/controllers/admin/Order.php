<?php

class Order extends Admin_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'order_model',
            'order_detail_model',
            'order_status_model'
        ));
        $this->load->helper(array(
            'config/date'
        ));
    }
    
    public function index () {
        
        $this->stencil->data(array(
            'page_title'    => 'All Orders',
            'page_subtitle' => 'Manage orders'
        ));
        
        $data['orders'] = $this->order_model->order_by('id', 'desc')
                            ->with('user')
                            ->with('status')
                            ->get_all();
        
        $this->stencil->data($data);
        
        $this->stencil->paint($this->view_prefix.'order');        
    }
    
    public function details($id) {
        $data['order'] = $this->order_model->order_by('id', 'desc')
                            ->with('user')
                            ->with('status')
                            ->get($id);
        
        $this->stencil->data(array(
            'page_title'    => 'Order Details',
            'page_subtitle' => $data['order']->code
        ));
        
        $data['order_details'] = $this->order_detail_model
                        ->get_by_order($id);
        
        $data['order_status_dropdown'] = $this->order_status_model
                                            ->dropdown('id', 'name');
        
        $this->stencil->data($data);
        
        $this->stencil->paint($this->view_prefix.'order-details');
    }
    
    public function update_status () {
        
        if ($this->input->method() == 'post') {
            $this->order_model->update($this->input->post('order_id'), [
                'status_id' => $this->input->post('status_id')
            ]);
            
            $this->order_model->sendEmailStatus($this->input->post('order_id'));
            $this->session->set_flashdata('success', 'Order status has been updated.');
            redirect($this->url->admin('order_details', $this->input->post('order_id')));
        } else {
            $this->load->library('user_agent');
            $ref = $this->agent->referrer();
            redirect($ref);
        }
        
    }
    
    public function print_order ($order_id) {
        
        $data = $this->order_model->getData($order_id);
        
        $this->stencil->data(array(
            'page_title'    => 'All Orders',
            'page_subtitle' => 'Manage orders'
        ));
        
        $this->stencil->layout('print-layout');
        
        $this->stencil->data($data);
        
        $this->stencil->paint($this->view_prefix.'order-print');
        
    }
}