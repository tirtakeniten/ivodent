<?php

class Page extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('page_model');
    }
    
    public function index()
    {
        $data['pages'] = $this->page_model->get_all();
        $data['page_title'] = 'All Pages';
        
        $this->stencil->data($data);
        
        $this->stencil->paint($this->view_prefix.'page');
    }
    
    public function form($id = NULL)
    {
        $data['page_title'] = 'New Page';
        
        if($id != NULL)
        {
            $data['page_title'] = 'Edit Page';
            $data['page'] = $this->page_model->get($id);
        }
        
        $this->stencil->data($data);
        
        if( $this->input->method() == 'get' )
        {
            $this->stencil->paint($this->view_prefix.'page-form');
        }
        elseif( $this->input->method() == 'post' )
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('user_id', 'User ID', 'required');
            $this->form_validation->set_rules('slug', 'Slug');
            $this->form_validation->set_rules('content', 'Content');
            $this->form_validation->set_rules('excerpt', 'Excerpt');
            $this->form_validation->set_rules('parent_id', 'Parent Page');
            $this->form_validation->set_rules('seo_title', 'SEO Title');
            $this->form_validation->set_rules('seo_description', 'SEO Description');
            $this->form_validation->set_rules('seo_keyword', 'SEO Keyword');
            $this->form_validation->set_rules('featured_image', 'Feature Image');
            
        	//'gallery', 
        	//'created_at', 
        	//'updated_at', 
            
            if($this->form_validation->run() == FALSE)
            {
                $this->stencil->paint($this->view_prefix.'page-form');
            }else{
                
                $values = array(
                    'title'        => $this->input->post('title'), 
                	'slug'         => $this->input->post('slug'), 
                	'content'      => $this->input->post('content'), 
                	'excerpt'      => $this->input->post('excerpt'), 
                	'parent_id'    => $this->input->post('parent_id'), 
                	'seo_title'    => $this->input->post('seo_title'), 
                	'seo_description'=> $this->input->post('seo_description'), 
                	'seo_keyword'  => $this->input->post('seo_description'), 
                	'featured_image'=> $this->input->post('featured_image'), 
                	'gallery'      => '',
                	'user_id'      => $this->input->post('user_id')
                );
                
                if( $this->input->post('id') == '' )
                {
                    $this->page_model->insert($values);
                    $this->session->set_flashdata('success', 'A page has been created.');
                }else{
                    $this->page_model->update($this->input->post('id'), $values);
                    $this->session->set_flashdata('success', 'Page has been updated.');
                }
                
                redirect('admin/page');
            }            
        }else{
            show_404();
        }
    }
    
    public function delete()
    {
        
    }
}