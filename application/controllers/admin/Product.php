<?php

class Product extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'category_model',
            'product_model',
            'product_image_model'
        ));
    }

    public function index()
    {
        /* page_title */
        $data['page_title'] = 'All Product';

        /* the categories */
        $data['products'] = $this->product_model->get_all();

        /* data */
        $this->stencil->data($data);

        /* paint */
        $this->stencil->paint($this->view_prefix.'product');
    }

    public function form( $id = NULL )
    {
        if($id == NULL)
        {
            /* page_title */
            $data['page_title'] = 'Add New Product';
        }
        else
        {
            /* page_title */
            $data['page_title'] = 'Edit Product';
            $data['product'] = $this->product_model->get($id);
            $data['product']->categories = $this->product_rel_category_model->get_category_id_of($id);
            $data['product']->images = $this->product_image_model->get_images_id_of($id);
        }

        $data['inventory_options'] = $this->product_model->get_inventory_options();
        $data['categories'] = $this->category_model->dropdown('id', 'name');
        $data['category_tree'] = $this->category_model->get_hierarchical();    
        
        $this->stencil->data($data);

        if( $this->input->method() == 'get' )
        {
            /* paint */
            $this->stencil->paint($this->view_prefix.'product-form');
        }
        elseif($this->input->method() == 'post')
        {
            /* validation */
            $this->load->library('form_validation');

            /* validation rules */
            $this->form_validation->set_rules(array(
                'sku' => array(
                    'field' => 'sku',
                    'label' => 'SKU'
                ),
            	'barcode' => array(
                    'field' => 'barcode',
                    'label' => 'barcode'
                ),
            	'name' => array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required'
                ),
            	'sequence' => array(
                    'field' => 'sequence',
                    'label' => 'sequence'
                ),
                'category_id[]' => array(
                    'field' => 'category_id[]',
                    'label' => 'Category'
                ),
            	'description' => array(
                    'field' => 'description',
                    'label' => 'description'
                ),
            	'excerpt' => array(
                    'field' => 'excerpt',
                    'label' => 'excerpt'
                ),
            	'price' => array(
                    'field' => 'price',
                    'label' => 'price'
                ),
            	'sale_price' => array(
                    'field' => 'sale_price',
                    'label' => 'sale_price'
                ),
            	'note' => array(
                    'field' => 'note',
                    'label' => 'note'
                ),
            	'slug' => array(
                    'field' => 'slug',
                    'label' => 'slug'
                ),
            	'inventory_tracking' => array(
                    'field' => 'inventory_tracking',
                    'label' => 'inventory_tracking'
                ),
            	'quantity' => array(
                    'field' => 'quantity',
                    'label' => 'quantity'
                ),
            	'seo_title' => array(
                    'field' => 'seo_title',
                    'label' => 'SEO title'
                ),
            	'seo_description' => array(
                    'field' => 'seo_description',
                    'label' => 'SEO description'
                ),
            	'seo_keyword' => array(
                    'field' => 'seo_keyword',
                    'label' => 'SEO keyword'
                )
            ));

            if($this->form_validation->run() === FALSE)
            {
                /* data not valid */
                $this->stencil->paint($this->view_prefix.'product-form');
            }else{

                /* Data */
                $value = array(
                    'sku'               => $this->input->post('sku'),
                    'barcode'           => $this->input->post('barcode'),
                    'name'              => $this->input->post('name'),
                    'sequence'          => $this->input->post('sequence'),
                    //'category_id'       => $this->input->post('category_id'),
                    'description'       => $this->input->post('description'),
                    'excerpt'           => $this->input->post('excerpt'),
                    'price'             => $this->input->post('price'),
                    'sale_price'        => $this->input->post('sale_price'),
                    'note'              => $this->input->post('note'),
                    //'status'            => $this->input->post('status'),
                    'slug'              => $this->input->post('slug'),
                    'inventory_tracking'=> $this->input->post('inventory_tracking'),
                    'quantity'          => $this->input->post('quantity'),
                    'seo_title'         => $this->input->post('seo_title'),
                    'seo_description'   => $this->input->post('seo_description'),
                    'seo_keyword'       => $this->input->post('seo_keyword'),
                    'created_at'        => date("Y-m-d H:i:s"),
                	'updated_at'        => date("Y-m-d H:i:s")
                );

                //print_r($value);
                $id = $this->input->post('id');

                if($id == '')
                {
                    /* insert */
                    $id = $this->product_model->insert($value);
                    $this->session->set_flashdata('success', 'A product has been created.');
                }else{

                    /* update */
                    unset($value['created_at']);
                    $this->product_model->update($id, $value);
                    $this->session->set_flashdata('success', 'Product has been updated.');
                }

                /* set category */
                $cat_id = $this->input->post('category_id');
                if(!empty($cat_id))
                {
                    $this->product_model->set_categories($id, $this->input->post('category_id'));
                }

                /* set images */
                if($this->input->post('image'))
                {
                    $images = array_filter($this->input->post('image'));
                    $this->product_model->set_images($id, $images);
                }

                redirect($this->url->admin('product'));
            }
        }else{
            /* not found */
            show_404();
        }
    }

    public function delete( $id = NULL )
    {
        if($id == NULL)
        {
            /* no ID */
            $this->session->set_flashdata('error', 'Undefined category ID.');
        }else{

            /* delete!!! */
            $this->product_model->delete_rel_categories($id);
            $this->product_model->delete_varians($id);
            $this->product_model->delete($id);
            $this->session->set_flashdata('success', 'Category has been deleted.');
        }
        redirect($this->url->admin('product'));
    }
}
