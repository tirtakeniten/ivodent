<?php

use Tirta\Adminlte as Lte;

class Setting extends Admin_Controller
{    
    public function __construct()
    {
        parent::__construct();
        
        $sidebar = Lte\Sidebar::getInstance();
        $sidebar->addMenu(
            'setting', 
            'Setting', 
            $this->url->admin());
        $this->_generate_menu();
    }
    
    public function index($name)
    {
        /* include the base setting controller */
        require APPPATH.'controllers/'.$this->router->directory.'/_setting/'.ucfirst($name).'.php';
        
        $classname = ucfirst($name);
        $class = new $classname();
        
        $this->stencil->data(array(
            'fields' => $class->fields(),
            'validations' => $class->validations(),
            'page_title' => $class->page_title(),
            'page_subtitle' => $class->page_subtitle(),
        ));
        
        /* Replace default value */
        $fields = $class->fields();
        foreach($fields as $key => $field)
        {
            $conf = $this->siteconfig->getConfig($key);
            if($conf != NULL) $fields[$key]['value'] = $conf;
        }
        
        $this->stencil->data('fields', $fields);
        /* ================================================= */
        
        if($this->input->method() == 'get')
        {
            $this->stencil->paint($this->view_prefix.'setting');
        }
        elseif($this->input->method() == 'post')
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules($class->validations());
            
            if($this->form_validation->run() === FALSE)
            {                
                foreach($fields as $key => $field)
                {
                    $fields[$key]['value'] = $this->input->post($key);
                }
                
                $this->stencil->data('fields', $fields);
                
                $this->stencil->paint($this->view_prefix.'setting');
            }else{
                
                foreach($fields as $key => $field)
                {
                    $this->siteconfig->setConfig($key, array(
                        'label' => $field['label'], 
                    	'key' => $key, 
                    	'value' => $this->input->post($key)
                    ), true);
                }
                
                $this->session->set_flashdata('success', 'Settings saved.');   
                
                redirect(current_url());         
            }
        }else{
            echo show_404();
        }
    }
    
    protected function _generate_menu()
    {
        
    }
}