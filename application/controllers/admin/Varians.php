<?php

class Varians extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'product_model',
            'varian_model'
        ));
    }
    
    public function index($product_id, $id = NULL)
    {
        $data['product'] = $this->product_model->get($product_id);
        $data['varians'] = $this->varian_model->get_many_by(array(
            'product_id' => $product_id
        ));
        
        /* page_title */
        $data['page_title'] = 'Varians of '.$data['product']->name;
        
        /* active product varians */
        if($id != NULL) $data['var'] = $this->varian_model->get($id);
        
        /* check the request method */
        $request_method = $this->input->method();
        
        if($request_method == 'post')
        {
            $this->_form($data);
        }
        elseif($request_method == 'get')
        {
            /* varian form */
            $data['form'] = $this->load->view('pages/'.$this->view_prefix.'varians-form', $data, TRUE);
            
            $this->stencil->data($data);
            $this->stencil->paint($this->view_prefix.'varians');
        }
    }
    
    private function _form($data)
    {
        $request_method = $this->input->method();
        
        if($request_method == 'post')
        {
            /* validation */
            $this->load->library('form_validation');
            
            /* validation rules */
            $this->form_validation->set_rules(array(
                'product_id' => array(
                    'field' => 'product_id',
                    'label' => 'Product ID'
                ), 
            	'varian1_key' => array(
                    'field' => 'varian1_key',
                    'label' => 'Option 1',
                    'rules' => 'required'
                ), 
            	'varian1_value' => array(
                    'field' => 'varian1_value',
                    'label' => 'Value 1',
                    'rules' => 'required'
                ), 
            	'varian2_key' => array(
                    'field' => 'varian2_key',
                    'label' => 'Option 2'
                ), 
            	'varian2_value' => array(
                    'field' => 'varian2_value',
                    'label' => 'sequence'
                ), 
            	'varian3_key' => array(
                    'field' => 'varian3_key',
                    'label' => 'Option 3'
                ), 
            	'varian3_value' => array(
                    'field' => 'varian3_value',
                    'label' => 'Value 3'
                ), 
            	'sku' => array(
                    'field' => 'sku',
                    'label' => 'SKU'
                ), 
            	'barcode' => array(
                    'field' => 'barcode',
                    'label' => 'Barcode'
                ), 
            	'quantity' => array(
                    'field' => 'quantity',
                    'label' => 'Quantity'
                ), 
            	'price' => array(
                    'field' => 'price',
                    'label' => 'Price'
                ), 
            	'sale_price' => array(
                    'field' => 'sale_price',
                    'label' => 'Sale Price'
                )
            ));
            
            if($this->form_validation->run() === FALSE)
            {
                /* data not valid */
                
                /* varian form */
                $data['form'] = $this->load->view('pages/'.$this->view_prefix.'varians-form', $data, TRUE);
                
                $this->stencil->data($data);
                $this->stencil->paint($this->view_prefix.'varians');
                /* varian form */
                
            }else{
                
                /* Data */
                $value = array(
                    'product_id'   => $this->input->post('product_id'), 
                	'varian1_key'  => $this->input->post('varian1_key'), 
                	'varian1_value'=> $this->input->post('varian1_value'), 
                	'varian2_key'  => $this->input->post('varian2_key'), 
                	'varian2_value'=> $this->input->post('varian2_value'), 
                	'varian3_key'  => $this->input->post('varian3_key'), 
                	'varian3_value'=> $this->input->post('varian3_value'), 
                	'sku'          => $this->input->post('sku'), 
                	'barcode'      => $this->input->post('barcode'), 
                	'quantity'     => $this->input->post('quantity'), 
                	'price'        => $this->input->post('price'), 
                	'sale_price'   => $this->input->post('sale_price')
                );
                
                $id = $this->input->post('id');
                
                if($id == '')
                {
                    /* insert */
                    $this->varian_model->insert($value);
                    $this->session->set_flashdata('success', 'A product varian has been created.');
                }else{
                    
                    /* update */
                    $this->varian_model->update($id, $value);
                    $this->session->set_flashdata('success', 'Product varian has been updated.');
                }
                
                redirect($this->url->admin('product_varian', $this->input->post('product_id')));
            }
        }
        else
        {
            echo show_404();            
        }
    }
    
    public function delete($product_id = NULL, $id = NULL)
    {
        if($product_id == NULL)
        {
            /* no ID */
            $this->session->set_flashdata('error', 'Undefined product ID.');
        }
        elseif($id == NULL)
        {
            /* no varian ID */
            $this->session->set_flashdata('error', 'Undefined product varian ID.');
        }else{
            
            /* delete!!! */
            $this->varian_model->delete_by(array(
                'product_id' => $product_id,
                'id' => $id,
            ));
            $this->session->set_flashdata('success', 'A product varian has been deleted.');
        }
        
        echo $this->url->admin('product_varian', $product_id);
        redirect($this->url->admin('product_varian', $product_id));
    }
}