<?php

require 'SettingInterface.php';

class Currency implements SettingInterface
{
    var $_ci;
    
    public function __construct()
    {
        $this->_ci =&get_instance();
    }
    
    public function fields()
    {
        return array(
            'usd_idr' => array(
                'type' => 'text',
                'value' => '10000',
                'label' => 'Conversion value (1 USD = ? IDR)'
            )
        );
    }
    
    public function validations()
    {
        return array(
            'usd_idr' => array(
                'field' => 'usd_idr',
                'label' => 'Conversion value',
                'rules' => 'required|numeric'
            )
        );
    }
    
    public function page_title()
    {
        return 'Currency Setting';
    }
    
    public function page_subtitle()
    {
        return '';
    }
}