<?php

require 'SettingInterface.php';

class General implements SettingInterface
{
    var $_ci;
    
    public function __construct()
    {
        $this->_ci =&get_instance();
    }
    
    public function fields()
    {
        return array(
            'site_name' => array(
                'type' => 'text',
                'value' => 'Sitename',
                'label' => 'Site Name'
            ),
            'site_tagline' => array(
                'type' => 'text',
                'value' => '',
                'label' => 'Tagline'
            ),
            'date_format' => array(
                'type' => 'select',
                'value' => '',
                'label' => 'Date Format',
                'options' => $this->dateFormat()
            ),
            'time_format' => array(
                'type' => 'select',
                'value' => '',
                'label' => 'Time Format',
                'options' => $this->timeFormat()
            ),
            'currency' => array(
                'type' => 'text',
                'value' => 'IDR',
                'label' => 'Currency'
            ),
            'currency_position' => array(
                'type' => 'select',
                'value' => '',
                'label' => 'Currency Position',
                'options' => $this->currencyPosition()
            ),
            'thousand_separator' => array(
                'type' => 'text',
                'value' => ',',
                'label' => 'Thousand Separator'
            ),
            'decimal_separator' => array(
                'type' => 'text',
                'value' => '.',
                'label' => 'Decimal Separator'
            ),
            'number_decimal' => array(
                'type' => 'text',
                'value' => '0',
                'label' => 'Number of Decimals'
            ),
        );
    }
    
    public function validations()
    {
        return array(
            'site_name' => array(
                'field' => 'site_name',
                'label' => 'Site Name',
                'rules' => 'required'
            ),
            'site_tagline' => array(
                'field' => 'site_tagline',
                'label' => 'Tagline',
                'rules' => 'required'
            ),
            'date_format' => array(
                'field' => 'date_format',
                'label' => 'Date Format',
                'rules' => 'required'
            ),
            'time_format' => array(
                'field' => 'time_format',
                'label' => 'Time Format',
                'rules' => 'required'
            ),
            'currency' => array(
                'field' => 'currency',
                'label' => 'Currency',
                'rules' => 'required'
            ),
            'currency_position' => array(
                'field' => 'currency_position',
                'label' => 'Currency Position',
                'rules' => 'required'
            ),
            'thousand_separator' => array(
                'field' => 'thousand_separator',
                'label' => 'Thousand Separator',
                'rules' => 'required'
            ),
            'decimal_separator' => array(
                'field' => 'decimal_separator',
                'label' => 'Decimal Separator',
                'rules' => 'required'
            ),
            'number_decimal' => array(
                'field' => 'number_decimal',
                'label' => 'Number of Decimals',
                'rules' => 'required'
            ),
        );
    }
    
    public function page_title()
    {
        return 'General Setting';
    }
    
    public function page_subtitle()
    {
        return '';
    }
    
    protected function dateFormat()
    {
        return array(
            'j F Y' => date('j F Y'),
            'Y-m-d' => date('Y-m-d'),
            'F j, Y' => date('F j, Y'),
        );
    }
    
    protected function timeFormat()
    {
        return array(
            'g:i a' => date('g:i a'),
            'g:i A' => date('g:i A'),
            'H:i' => date('H:i'),
        );
    }
    
    protected function currencyPosition()
    {
        return array(
            'left' => 'Left e.g: IDR99.99',
            'right' => 'Right e.g: 99.99IDR',
            'left_space' => 'Left with space e.g: IDR 99.99',
            'right_space' => 'Right with space e.g: 99.99 IDR',
        );
    }
}