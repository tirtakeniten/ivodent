<?php

require 'SettingInterface.php';

class Image implements SettingInterface
{
    var $_ci;
    
    public function __construct()
    {
        $this->_ci = &get_instance();
    }
    
    public function fields()
    {
        return array(
            'image_thumbnail' => array(
                'type' => 'text',
                'value' => '150x150',
                'label' => 'Image Thumnail Size'
            ),
            'image_thumbnail_crop' => array(
                'type' => 'select',
                'value' => '',
                'label' => 'Thumnail Crop',
                'options' => $this->_get_options()
            ),
            'image_medium' => array(
                'type' => 'text',
                'value' => '300x300',
                'label' => 'Image Medium Size'
            ),
            'image_medium_crop' => array(
                'type' => 'select',
                'value' => '',
                'label' => 'Image Medium Crop',
                'options' => $this->_get_options()
            ),
            'image_large' => array(
                'type' => 'text',
                'value' => '600x600',
                'label' => 'Image Large Size'
            ),
            'image_large_crop' => array(
                'type' => 'select',
                'value' => '',
                'label' => 'Image Large Crop',
                'options' => $this->_get_options()
            )
        );
    }

    public function validations()
    {
        return array(
            'image_thumbnail' => array(
                'field' => 'image_thumbnail',
                'label' => 'Image Thumbnail Size',
                'rules' => 'required'
            ),
            'image_medium' => array(
                'field' => 'image_medium',
                'label' => 'Image Medium Size',
                'rules' => 'required'
            ),
            'image_large' => array(
                'field' => 'image_large',
                'label' => 'Image Large Size',
                'rules' => 'required'
            ),
        );
    }
    
    public function page_title()
    {
        return 'Image Setting';
    }
    
    public function page_subtitle()
    {
        return '';
    }
    
    protected function _get_options()
    {
        return array(
            '0' => 'No',
            '1' => 'Yes'
        );
    }
}