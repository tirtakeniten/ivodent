<?php

require 'SettingInterface.php';

class Image_slider implements SettingInterface
{
    var $_ci;
    
    public function __construct()
    {
        $this->_ci =&get_instance();
    }
    
    function fields()
    {
        $count = 6;
        
        $fields = array();
        
        for($i=1; $i<=$count; $i++)
        {
            $fields["slide_image{$i}"] = array(
                'type' => 'image',
                'value' => '',
                'label' => "Image {$i} ( 1140px x 420px )"
            );
            
            $fields["slide_title{$i}"] = array(
                'type' => 'text',
                'value' => '',
                'label' => "Title {$i}"
            );
            
            $fields["slide_description{$i}"] = array(
                'type' => 'text',
                'value' => '',
                'label' => "Description {$i}"
            );
        }
        
        return $fields;
    }
    
    function validations()
    {
        $count = 6;
        
        $fields = array();
        
        for($i=1; $i<=$count; $i++)
        {
            $fields["slide_image{$i}"] = array(
                'field' => "slide_image{$i}",
                'label' => "Image {$i}",
                'rules' => 'max_length[255]',
            );
            
            $fields["slide_title{$i}"] = array(
                'field' => "slide_title{$i}",
                'label' => "Title {$i}",
                'rules' => 'max_length[255]',
            );
            
            $fields["slide_description{$i}"] = array(
                'field' => "slide_description{$i}",
                'label' => "Description {$i}",
                'rules' => 'max_length[5000]',
            );
        }
        
        return $fields;
    }
    
    function page_title()
    {
        return 'Image Slider';
    }
    
    function page_subtitle()
    {
        return 'The image slider of home page.';
    }
}