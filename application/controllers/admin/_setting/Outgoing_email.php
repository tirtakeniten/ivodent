<?php

require 'SettingInterface.php';

class Outgoing_email implements SettingInterface
{
    var $_ci;
    
    public function __construct()
    {
        $this->_ci =&get_instance();
    }
    
    public function fields()
    {
        return array(
            'email_out_name' => array(
                'type' => 'text',
                'value' => 'Website',
                'label' => '"From name" header'
            ),
            'email_out_email' => array(
                'type' => 'text',
                'value' => 'name@domain.com',
                'label' => 'email_out_from_name'
            ),
        );
    }
    
    public function validations()
    {
        return array(
            'email_out_from' => array(
                'field' => 'email_out_from_name',
                'label' => '"From name" header',
                'rules' => 'required'
            ),
            
        );
    }
    
    public function page_title()
    {
        return 'Email Setting';
    }
    
    public function page_subtitle()
    {
        return 'Setting form outgoing email header.';
    }
}