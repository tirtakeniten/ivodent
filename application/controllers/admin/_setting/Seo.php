<?php

require 'SettingInterface.php';

class Seo implements SettingInterface
{
    var $_ci;
    
    public function __construct()
    {
        $this->_ci =&get_instance();
    }
    
    public function fields()
    {
        return array(
            'default_title' => array(
                'type' => 'text',
                'value' => '',
                'label' => 'Default Title'
            ), 
            'default_description' => array(
                'type' => 'text',
                'value' => '',
                'label' => 'Default Description'
            ),
            'default_og_image' => array(
                'type' => 'image',
                'value' => '',
                'label' => 'Default OG Image'
            ),
            'google_site_verification' => array(
                'type' => 'text',
                'value' => '',
                'label' => 'Google Site Verification'
            ),
            'google_analytics' => array(
                'type' => 'textarea',
                'value' => '',
                'label' => 'Google Analytics',
                'rows' => 9
            ),
            'twitter_account' => array(
                'type' => 'text',
                'value' => '',
                'label' => 'Twitter Account'
            ),
        );
    }
    
    public function validations()
    {
        return array(
            'default_title' => array(
                'field' => 'default_title',
                'label' => 'Default Title',
                'rules' => 'required'
            ),
            'default_description' => array(
                'field' => 'default_description',
                'label' => 'Default Description',
                'rules' => 'required'
            ),
            'default_og_image' => array(
                'field' => 'default_og_image',
                'label' => 'Default OG Image',
                'rules' => 'required'
            ),
            'google_site_verification' => array(
                'field' => 'google_site_verification',
                'label' => 'Google Site Verification',
            ),
            'google_analytics' => array(
                'field' => 'google_analytics',
                'label' => 'Google Analytics',
            ),
            'twitter_account' => array(
                'field' => 'twitter_account',
                'label' => 'Twitter Account',
            )
        );
    }
    
    public function page_title()
    {
        return 'Currency Setting';
    }
    
    public function page_subtitle()
    {
        return '';
    }
}