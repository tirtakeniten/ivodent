<?php

interface SettingInterface
{
    function fields();
    
    function validations();
    
    function page_title();
    
    function page_subtitle();
}