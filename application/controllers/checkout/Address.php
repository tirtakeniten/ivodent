<?php

class Address extends Member_Controller {
    
    public function __construct () {
        parent::__construct();
        $this->load->model(array(
            'user_address_model'
        ));
    }
    
    public function index () {
        $data['user'] = $this->auth->user();
        
        $data['addresses'] = $this->user_address_model->get_many_by(array(
            'user_id' => $data['user']->id
        ));
        
        $data['seo_title'] = 'Select Your Shipping Address - '.get_site_config('default_title');
        
        $this->stencil->data($data);
        
        $this->stencil->paint('checkout/address');
    }
    
    public function select ($address_id) {
        $data['user'] = $this->auth->user();
        
        $address = $this->user_address_model->get_by(array(
            'user_id' => $data['user']->id,
            'id' => $address_id
        ));
        if (is_null($address)) {
            $this->session->set_flashdata('error', 'Address not found. Please select another address or add a new address.');
            redirect('checkout/address');
        } else {
            $this->checkout_address->set_address($address);
            redirect('checkout');
        }
    }
}