<?php

class Home extends Member_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('cart_factory');
        $this->load->model(array(
            'order_model',
            'order_detail_model'
        ));
        $this->load->helper(array(
            'config/date_helper'
        ));
        
    }
    
    public function index () {
        $contents = $this->cart->contents();
        if(count($contents) == 0) { redirect('cart'); }
        
        if ($this->input->method() == 'get') {
            $this->_order_preview();
        } elseif ($this->input->method() == 'post') {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('agree_term', 'Term', 'required', array(
                'required' => 'You must agree our terms and conditions to confirm your order'
            ));
            
            if ($this->form_validation->run() == FALSE) {
                $this->_order_preview();
            } else {
                $this->_save_order();
            }
        }
    }
    
    private function _order_preview () {
        /* check the cart */
        $cart = $this->cart_factory->contents(TRUE);
        if (count($cart) == 0) {
            $this->session->set_flashdata('error', 'You don\'t have any item on your cart.');
            redirect('cart');
        }
        
        /* check the shipping */
        $shipping = $this->checkout_shipping->get_shipping();
        if (is_null($shipping)) {
            redirect('checkout/shipping');
        }
        
        /* check the address */
        $address = $this->checkout_address->get_address();
        if (is_null($address)) {
            redirect('checkout/address');
        }
        
        $data['cart_factory'] = $this->cart_factory;
        $data['cart_contents'] = $cart;
        $data['shipping'] = $shipping;
        $data['shipping_raw'] = $this->checkout_shipping->get_raw_data();
        $data['shipping_destination'] = $this->checkout_shipping->get_city();
        $data['address'] = $address;
        
        $data['seo_title'] = 'Order Review - '.get_site_config('default_title');
        
        $this->stencil->data($data);
        
        $this->stencil->paint('checkout/home');
    }
    
    private function _save_order () {
        $user = $this->auth->user();
        $shipping = $this->checkout_shipping->get_shipping();
        $address = $this->checkout_address->get_address();
        $shipping_destination = $this->checkout_shipping->get_city();
        
        /* ORDER DATA */
        $order = array(
            'code'         => '', 
        	'date'         => date('Y-m-d'), 
        	'status_id'    => 1, 
        	'user_id'      => $user->id, 
        	'total'        => $this->cart_factory->cart()->total(), 
        	'total_item'   => $this->cart_factory->cart()->total_items(), 
        	'shipping_cost'=> $shipping->cost->cost[0]->value, 
        	'grandtotal'    => $this->cart_factory->grandtotal(), 
        	'address'      => $address, 
        	'shipping_info'=> $shipping_destination
        );
        
        $order_id = $this->order_model->insert($order);
        
        /* ORDER DETAIL */
        $order_details = array();
        foreach ($this->cart_factory->contents(true) as $item) {
            $order_details = array(
                'order_id'     => $order_id, 
            	'price'        => $item['price'],
            	'qty'          => $item['qty'], 
            	'subtotal'     => $item['subtotal'], 
            	'product_raw_data' => json_encode($item['product'])
            );
            $this->order_detail_model->insert($order_details);
        }
        
        $this->send_email($order_id);
        
        $this->cart_factory->cart()->destroy();
        
        redirect('checkout/success');
    }
    
    public function send_email ($id) {
        $user = $this->auth->user();
        $order = $this->order_model
                        ->with('user')
                        ->with('status')
                        ->get($id);
        $data['order'] = $order; 
        
        $data['order_details'] = $this->order_detail_model
                        ->get_by_order($id);
        
        $email_customer = $this->load->view('mail/order_to_customer', $data, true);
        $email_admin = $this->load->view('mail/order_to_admin', $data, true);
        
        //echo $email_customer;
        
        $this->load->library('email');
        
        /* to customer */
        $this->email->mailtype = 'html';
        $this->email->from('no-reply@ivodent.com', 'Ivodent');
        $this->email->to($user->email);
        $this->email->reply_to('tirta@balibagus.com');
        
        $this->email->subject('[Ivodent] Purchase Information');
        $this->email->message($email_customer);
        
        $this->email->send();
        
        /* to admin */
        $this->email->initialize(array());
        $this->email->mailtype = 'html';
        $this->email->from('no-reply@ivodent.com', 'Ivodent');
        $this->email->to('tirta@balibagus.com');
        $this->email->reply_to($user->email);
        
        $this->email->subject('Order Confirmation '.$order->code);
        $this->email->message($email_admin);
        
        $this->email->send();
        
        if ($this->input->is_ajax_request()) {
            echo json_encode(array('status' => 1));
        }
    }
}