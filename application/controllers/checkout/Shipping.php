<?php

class Shipping extends Member_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('ongkir_factory');
        
        /* Origin city */
        $city_code = '114'; //Denpasar
        $this->ongkir_factory->originCity($city_code);
    }
    
    public function index () {        
        $data['provinces'] = $this->ongkir_factory->getProvince();
        
        $data['seo_title'] = 'Select Shipping - '.get_site_config('default_title');
        
        $province_dropdown = array( 0 => '-- Select Province --' );
        
        foreach ($data['provinces'] as $province) {
            $province_dropdown[$province->province_id] = $province->province;
        }
        $data['province_dropdown'] = $province_dropdown;
        
        $this->stencil->data($data);
        
        if ($this->input->method() == 'get') {
            $this->stencil->paint('checkout/shipping'); 
        } elseif ($this->input->method() == 'post') {
            $this->checkout_shipping->set_shipping($this->input->post());
            $shipping = $this->checkout_shipping->get_shipping();
            redirect('checkout');
        }
    }
    
    public function get_shipping_city () {
        if ($this->input->is_ajax_request() && $this->input->method() == 'post' ) {
            $cities = $this->ongkir_factory->getCity($this->input->post('province_id'));
            
            $city_dropdown = array('0' => '-- Select City --');
            foreach ($cities as $city) {
                $city_dropdown[$city->city_id] = $city->type.' '.$city->city_name;
            }
            
            echo form_dropdown(
                'city_id', 
                $city_dropdown, 
                set_value('city_id', @$province_id),
                'class="form-control" id="city-dropdown" onchange="getCost(this);"' 
            );
        }
    }
    
    public function get_shipping_cost () {
        if ($this->input->is_ajax_request() && $this->input->method() == 'post' ) {
            $costs = $this->ongkir_factory->getCost($this->input->post('city_id'), 1000);
            
            $free_shipping_code = array(
                $this->ongkir_factory->originCity(), //Denpasar
            );
            
            if (count($costs[0]->costs) == 0) {
                echo 'NULL';
            } else {
                $this->load->view('shipping-get-cost', array(
                    'costs' => $costs[0],
                    'free_shipping_dest_city_code' => $free_shipping_code,
                    'dest_city' => $this->ongkir_factory->getCity(
                        $this->input->post('province_id'),
                        $this->input->post('city_id'))
                ));
            }
        }
    }
}