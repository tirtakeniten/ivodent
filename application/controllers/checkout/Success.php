<?php

class Success extends Member_Controller {
    
    var $featureProductID = 7;
    
    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'product_rel_category_model',
            'product_model',
            'page_model'
        ));
    }
    
    public function index () {
        /* Featured products */
        $featuredProductID = $this->product_rel_category_model
                                    ->product_by_category_id($this->featureProductID);
        if (count($featuredProductID) > 0) {
            $this->db->where_in('id', $this->featureProductID);
            $data['featured_products'] = $this->product_model->get_all();
        }else{
            $data['featured_products'] = $this->product_model
                                                ->limit(6)
                                                ->get_all();
        }
        
        $data['seo_title'] = 'Hei, You made it! - '.get_site_config('default_title');
        
        $this->stencil->paint('checkout/success', $data);
    }
}