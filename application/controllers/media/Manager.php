<?php

class Manager extends Media_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'file_model'
        ));
    }
    
    public function index()
    {
        $data['files'] = $this->file_model->get_all();
        $this->stencil->paint($this->view_prefix.'/manager', $data);
    }
    
    public function form()
    {
        $data = array();
        $this->stencil->paint($this->view_prefix.'/manager-form', $data);
    }
    
    public function upload()
    {
        if( $this->input->method() == 'post' )
        {
            $this->load->library('image_upload');
            $upload = $this->image_upload->do_upload('file');
            echo json_encode($_FILES);
        }
    }
    
    public function get($id)
    {
        $data = $this->file_model->get($id);
        $data->size = array();
        $data->size['original'] = image_url($data, 'original');
        
        $image_setting = get_image_setting();
        foreach($image_setting as $dir => $sett)
        {
            $data->size[$dir] = image_url($data, $dir);
        }
        
        echo json_encode(array(
            'file' => $data
        ));
    }
}