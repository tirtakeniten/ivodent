<?php

use OpenBuildings\PayPal\Payment as Payment;

class Paypal extends CI_Controller {
    
    var $paypal_config = [];
    var $payment_instance;
    var $order;
    
    public function __construct() {
        parent::__construct();
        
        /* Models */
        $this->load->model(array(
            'user_model',
            'user_address_model',
            'order_model',
            'order_detail_model',
            'payment_paypal_model'
        ));
        
        /* helper */
        $this->load->helper(array(
            'config/date',
            'number'
        ));
        
        /* the config */
        $this->paypal_config = [
            'username'  => 'marikguizot-facilitator_api1.gmail.com',
            'password'  => 'MX82L9UJUV8VNH3Q',
            'signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31AkzfU3.2hBHEpAMu-XdbugTiw8BQ',
            'email'     => 'marikguizot-facilitator@gmail.com',
        ];
        
        /* the payment instance */
        $this->payment_instance = Payment::instance('ExpressCheckout')
                ->config($this->paypal_config);
        
        /* orders */
        $this->order = [
            'items_price' => 10,
            'shipping_price' => 3,
            'total_price' => 13
        ];
    }
    
    public function index () {
        
        $orderData = $this->_getOrder();
        
        $expressCheckoutParam = [];
        $total = 0;
        
        foreach ($orderData['order_details'] as $k => $orderDetail) {
            $usdPrice = idr_to_usd($orderDetail->price);
            
            $expressCheckoutParam['L_PAYMENTREQUEST_0_NAME'.$k] = $orderDetail->product->name;
            $expressCheckoutParam['L_PAYMENTREQUEST_0_NUMBER'.$k] = $orderDetail->product->sku;
            $expressCheckoutParam['L_PAYMENTREQUEST_0_DESC'.$k] = strip_tags($orderDetail->product->description);
            $expressCheckoutParam['L_PAYMENTREQUEST_0_AMT'.$k] = number_format($usdPrice, 2);
            $expressCheckoutParam['L_PAYMENTREQUEST_0_QTY'.$k] = $orderDetail->qty;
            
            $total += $expressCheckoutParam['L_PAYMENTREQUEST_0_AMT'.$k] * $expressCheckoutParam['L_PAYMENTREQUEST_0_QTY'.$k];
        }
        
        $shippingCostUSD = number_format(idr_to_usd($orderData['order']->shipping_cost), 2);
        
        $this->order = [
            'items_price' => $total,
            'shipping_price' => $shippingCostUSD,
            'total_price' => $total + $shippingCostUSD
        ];
        
        /*
        echo "<pre>";
        print_r($this->order );
        print_r($expressCheckoutParam);
        exit;
        */
              
        $express_checkout = $this->payment_instance
            ->order($this->order)
            ->return_url(site_url('pay/paypal/return_url').'?id='.$orderData['order']->id)
            ->cancel_url(site_url('pay/paypal/cancel_url').'?id='.$orderData['order']->id)
            ->notify_url(site_url('pay/paypal/notify_url').'?id='.$orderData['order']->id);
        
        $response = $express_checkout
                    ->set_express_checkout($expressCheckoutParam);
        
        $webscr_url = Payment::webscr_url('_express-checkout', [
            'token' => $response['TOKEN']
        ]);
        
        redirect($webscr_url);
    }
    
    public function get_details ($token) {
        $express_checkout = $this->payment_instance;
        $checkout_details = $express_checkout->get_express_checkout_details([
            'TOKEN' => $token
        ]);
        return $checkout_details;
    }
    
    public function return_url () {
        $token = $this->input->get('token');
        
        if (empty($token)) redirect(site_url());
        
        $checkout_details = $this->get_details($this->input->get('token'));
        
        $express_checkout = $this->payment_instance
            ->order([
                'items_price'   => $checkout_details['ITEMAMT'],
                'shipping_price'=> $checkout_details['SHIPPINGAMT'],
                'total_price'   => $checkout_details['AMT']
            ]);
        $response = $express_checkout->do_express_checkout_payment(
            $checkout_details['TOKEN'],
            $checkout_details['PAYERID']
        );
        
        /* Save Paypal Response Data into Database */
        $paymentId = $this->payment_paypal_model->insert([
           'order_id'   => $this->input->get('id'), 
	       'paypal_data'=> $response
        ]);
        
        /* send email payment */
        $this->_sendPaypalNotification($paymentId);
        
        /* change order status */
        /* Current order status must be "Awaiting Payment Confirmation" */
        $statusId = 6;
        $this->order_model->update($this->input->get('id'), ['status_id' => $statusId]);
        
        /* send email status change */
        $this->order_model->sendEmailStatus($this->input->get('id'));
        
        redirect($checkout_details['NOTIFYURL']);
    }
    
    public function cancel_url () {
        redirect(site_url('account/order/'.$this->input->get('id')));
    }
    
    public function notify_url() {
        $this->session->set_flashdata('success', 'Thank you for making payment');
        redirect(site_url('account/order/'.$this->input->get('id')));
    }
    
    private function _getOrder ($orderCode = NULL) {
        if ($orderCode == NULL) {
            $orderCode = $this->input->get('code');
        }
        
        $data['order'] = $this->order_model
                        ->with('user')
                        ->with('status')
                        ->get_by(array(
                            'code' => $orderCode
                        ));
        
        $data['order_details'] = $this->order_detail_model
                        ->get_by_order($data['order']->id);
        
        return $data;
    }
    
    private function _sendPaypalNotification ($paymentId = 1) {
        /* Get paypal data */
        $data['paypal'] = $this->payment_paypal_model->get($paymentId);
        
        /* Get order code */
        $orderData = $this->order_model->get($data['paypal']->order_id); 
        
        /* Get full order data */
        $data['order'] = $this->_getOrder($orderData->code);
        
        /* The message */
        $message = $this->load->view('mail/payment_paypal', $data, TRUE);
        
        /* SEND EMAIL */
        $this->load->library('email');
            
        /* to customer */
        $this->email->mailtype = 'html';
        $this->email->from($this->config->item('contact_email_from'), $this->config->item('contact_name_from'));
        $this->email->to($data['order']['order']->user->email);
        $this->email->reply_to($this->config->item('contact_email_reply'));
        
        $this->email->subject('[Ivodent] Thank you for making payment');
        $this->email->message($message);
        
        $this->email->send();
    }
}