<?php

class Login extends CI_Controller
{
    var $view_file = 'pages/user-login.php';
    
    public function __construct()
    {
        parent::__construct();        
        $this->load->library('auth');
        self::_SCAN();
    }
    
    public function index()
    {
        /* Redirection */
        $redirect = $this->input->get('redirect');
        $data['redirect'] = $redirect;
        
        /* if post */
        if( $this->input->method() == 'post' )
        {
            /* posted variable */
            $user = array(
                $this->input->post('email'),
                $this->input->post('password'),
            );
            
            /* start authentication */
            $auth = $this->auth->start($user);
            
            /* if valid */
            if($auth)
            {                
                if(empty($redirect))
                {
                    $active_user = $this->auth->user();
                    if($active_user->role == 'admin')
                    {
                        redirect('admin');
                    }elseif($active_user->role == 'member'){
                        redirect('account');
                    }
                }
                
                redirect($redirect);
            }else{
                
                /* user not valid */
                $data['message'] = $this->auth->message();
                $this->load->view($this->view_file, $data);
            }
            
        /* else */
        }else{
            $this->load->view($this->view_file, $data);
        }
    }
    
    protected function _SCAN()
    {
        /* restart */
        if( $this->auth->restart() )
        {
            $redirect = $this->input->get('redirect');
            if(empty($redirect))
            {
                redirect('admin');
            }else{
                $active_user = $this->auth->user();
                if($active_user->role == 'admin')
                {
                    redirect('admin');
                }elseif($active_user->role == 'member'){
                    redirect('account');
                }
            }
            redirect($redirect);
        }else{
            
        }
    }
}