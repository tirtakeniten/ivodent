<?php

class Logout extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
    }
    
    public function index()
    {
        $this->auth->restart();
        
        $active_user = $this->auth->user();
        
        $role = $active_user->role;
        
        $this->auth->end();
        
        if($role == 'admin')
        {
            redirect('admin');
            
        }elseif($role == 'member'){
            
            redirect('account');
        }
    }
}