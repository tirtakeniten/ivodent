<?php

class Lost_password extends Front_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'user_model'
        ));
        $this->load->helper(array(
            'string'
        ));
    }
    
    public function index () {
        
        if ($this->input->method() == 'get') {
            
            $this->stencil->paint('user-lost-password');
            
        } elseif ($this->input->method() == 'post') {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('email', 'Email', 'required|valid_emails');
            
            if ($this->form_validation->run() == FALSE) {
                
                $this->stencil->paint('user-lost-password');
                
            } else {
                
                $user = $this->user_model->get_by(array(
                    'email' => $this->input->post('email')
                ));
                
                if ($user != NULL) {
                    
                    $random_str = random_string();
                    $this->user_model->update($user->id, array(
                        'password' => $random_str
                    ));
                    
                    $data['user'] = $this->user_model->get_by(array(
                        'email' => $this->input->post('email')
                    ));
                    $data['new_password'] = $random_str;
                    $data['seo_title'] = 'Lost Password - '.get_site_config('default_title');
                    $to_customer = $this->load->view('mail/reset_password', $data, TRUE);
                    $this->load->library('email');
                    
                    /* to customer */
                    $this->email->mailtype = 'html';
                    $this->email->from($this->config->item('contact_email_from'), $this->config->item('contact_name_from'));
                    $this->email->to($this->input->post('email'));
                    $this->email->reply_to($this->config->item('contact_email_reply'));
                    
                    $this->email->subject('Reset password confirmation');
                    $this->email->message($to_customer);
                    
                    $this->email->send();
                    
                }
                
                $this->session->set_flashdata('success', 'Your new password has been sent to your email');
                redirect('user/lost_password');
                
            }
            
        } else {
            show_404();
        }
        
    }
}