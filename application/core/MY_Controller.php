<?php

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->config('url');
        $this->load->library(array(
            'auth', 
            'stencil_factory'
        ));
    }
}

/* The Application Core Path */
$core_path = APPPATH.'core/';

/* The Core Controller Path */
$core_controller_path = $core_path.'controllers/';

/* List of Core Controllers */
$core_controllers = array(

    /* Auth Controller */
    'Auth_Controller.php',
    
    /* Admin Controller */
    'Admin_Controller.php',
    
    /*  */
    'Front_Controller.php',
    
    /*  */
    'Media_Controller.php',
    
    /*  */
    'Member_Controller.php',
    
);

/* Load */
foreach($core_controllers as $core_controller)
{
    require_once $core_controller_path.$core_controller;
}