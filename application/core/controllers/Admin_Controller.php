<?php

use Tirta\Adminlte as Lte;

class Admin_Controller extends Auth_Controller
{
    var $view_prefix = 'admin/';
    
    public function __construct()
    {
        parent::__construct();
        $this->_check_admin();
        $this->_initialize();
    }
    
    protected function _initialize()
    {
        /* the layout */
        $this->stencil->layout('admin-layout.php');
        
        /* JS */
        $this->stencil->js(array(
            base_url( Lte\Plugin::jQuery('jQuery-2.1.4.min.js') ),
            base_url( Lte\Bootstrap::js('bootstrap.min.js') ),
            base_url( Lte\Plugin::slimScroll('jquery.slimscroll.min.js') ),
            base_url( Lte\Plugin::fastclick('fastclick.min.js') ),
            base_url( Lte\Plugin::datatables('jquery.dataTables.min.js') ),
            base_url( Lte\Plugin::datatables('dataTables.bootstrap.min.js') ),
            base_url( Lte\Plugin::iCheck('icheck.min.js') ),
            asset_url( 'plugins/tinymce/tinymce.min.js' ),
            asset_url( 'plugins/tinymce/jquery.tinymce.min.js' ),
            asset_url( 'plugins/treeview/treed.js' ),
            'product/lib/jquery.mousewheel-3.0.6.pack.js',
            'product/jquery.fancybox.js',
            base_url( Lte\App::js('app.min.js') ),
            'admin.js',
        ));
        
        /* CSS */
        $this->stencil->css(array(
            base_url( Lte\Bootstrap::css('bootstrap.min.css') ),
            'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
            'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
            base_url( Lte\Plugin::datatables('dataTables.bootstrap.css') ),
            base_url( Lte\Plugin::iCheck('all.css') ),
            asset_url('js/product/jquery.fancybox.css?v=2.1.5'),
            asset_url( 'plugins/treeview/treed.css' ),
            base_url( Lte\App::css('AdminLTE.min.css') ),
            base_url( Lte\App::css('skins', '_all-skins.min.css') ),
        )); 
        
        /* slices */
        $this->stencil->slice(array(
            'sidebar' => $this->view_prefix.'sidebar'
        ));
        
        /* default data */
        $this->stencil->data(array(
            'page_title' => '',
            'page_subtitle' => ''
        ));
    }
    
    protected function _check_admin () {
        $user = $this->auth->user();
        if ($user->role == 'admin') {
            
        } else {
            redirect('account');
        }
    }
}