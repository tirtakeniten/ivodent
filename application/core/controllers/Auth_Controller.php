<?php

class Auth_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
        self::_INITIALIZE();
    }
    
    protected function _INITIALIZE()
    {
        if( $this->auth->restart() )
        {
            
        }else{
            $redirectString = $this->uri->uri_string();
            redirect(site_url('user/login').'?redirect='.$redirectString);
        }
    }
}