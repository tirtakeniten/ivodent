<?php

use Tirta\Adminlte as Lte;

class Front_Controller extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_initialize();
    }
    
    protected function _initialize()
    {
        $this->stencil_factory->make();
    }
}