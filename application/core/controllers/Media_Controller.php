<?php

use Tirta\Adminlte as Lte;

class Media_Controller extends MY_Controller
{
    var $view_prefix = 'media/';
    
    public function __construct()
    {
        parent::__construct();
        $this->_initialize();
    }
    
    protected function _initialize()
    {
        /* the layout */
        $this->stencil->layout('media-layout.php');
        
        /* JS */
        $this->stencil->js(array(
            base_url( Lte\Plugin::jQuery('jQuery-2.1.4.min.js') ),
            base_url( Lte\Bootstrap::js('bootstrap.min.js') ),
            base_url( Lte\Plugin::slimScroll('jquery.slimscroll.min.js') ),
            base_url( Lte\App::js('app.min.js') ),
            'dropzone.js',
            'media.manager.js'
        ));
        
        /* CSS */
        $this->stencil->css(array(
            base_url( Lte\Bootstrap::css('bootstrap.min.css') ),
            'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
            'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
            base_url( Lte\App::css('AdminLTE.min.css') ),
            base_url( Lte\App::css('skins', '_all-skins.min.css') ),
            'dropzone.css'
        )); 
    }
}