<?php

class Member_Controller extends MY_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array(
            'auth',
            'checkout_shipping',
            'checkout_address'
        ));
        $this->_INITIALIZE();
        $this->stencil_factory->make();
        $this->stencil->slice(array(
            'account_sidebar' => 'account_sidebar'
        ));
    }
    
    protected function _INITIALIZE()
    {
        if( $this->auth->restart() )
        {
            
        }else{
            $redirectString = $this->uri->uri_string();
            redirect(site_url('account-login').'?redirect='.$redirectString);
        }
    }
}