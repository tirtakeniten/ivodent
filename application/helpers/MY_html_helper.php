<?php

if(!function_exists('category_tree'))
{
    function category_tree($categories)
    {
        $compiled = '';
        
        foreach($categories as $category)
        {
            $compiled .= '<li>';
            $compiled .= '<a href="'.site_url('category/'.$category->slug).'" >'.$category->name.'</a>';
            
            if(count($category->children) > 0)
            {
                $compiled .= '<ul>';
                $compiled .= category_tree($category->children);
                $compiled .= '</ul>';
            }
            
            $compiled .= '</li>';
        }
        
        return $compiled;
    }
}