<?php

if(!function_exists('format_number'))
{
    function format_number($number)
    {
        $decimal = get_site_config('number_decimal');
        $decimal_sep = get_site_config('decimal_separator');
        $thousand_sep = get_site_config('thousand_separator');
        return number_format($number, $decimal, $decimal_sep, $thousand_sep);
    }
}

if(!function_exists('currency_format'))
{
    function currency_format($number)
    {
        $pos = get_site_config('currency_position');
        $currency = get_site_config('currency');
        $formatedNumber = format_number($number);
        
        if($pos == 'left')
        {
            $formatedNumber = $currency.''.$formatedNumber; 
        }
        
        if($pos == 'right')
        {
            $formatedNumber .= ''.$currency;
        }
        
        if($pos == 'left_space')
        {
            $formatedNumber = $currency.' '.$formatedNumber;
        }
        
        if($pos == 'right_space')
        {
            $formatedNumber .= ' '.$currency;
        }
        
        return $formatedNumber;
    }
}

if (!function_exists('db_currency_conversion')) {
    function db_currency_conversion () {
        return get_site_config('usd_idr');
    } 
}

if (!function_exists('one_usd_to_idr')) {
    function one_usd_to_idr()
    {
        $CI = &get_instance();
        if ($CI->config->item('usd_idr')) {
            return $CI->config->item('usd_idr');
        } else {
            $httpAdapter = new \Ivory\HttpAdapter\FileGetContentsHttpAdapter();
        
            // Create the Yahoo Finance provider
            $googleProvider = new \Swap\Provider\GoogleFinanceProvider($httpAdapter);
            
            $converted = FALSE;
            try{
                // Create Swap with the provider
                $swap = new \Swap\Swap($googleProvider);
                
                $rate = $swap->quote('USD/IDR');
                
                $converted = $rate->getValue();
                
                $CI->config->set_item('usd_idr', $converted);
            }catch(Exception $e){
                $e->getMessage();
            }
            
            return one_usd_to_idr();
        }
    }
}

if(!function_exists('usd_to_idr'))
{
    /**
     * Convert USD to IDR
     * 
     * @param float
     * @param str online | db
     **/
    function usd_to_idr ($number = 1, $source = 'online') {
        if ($source == 'online') {
            return one_usd_to_idr()*$number;
        } elseif ($source == 'db') {
            return db_currency_conversion()*$number;
        }
    }
}

if(!function_exists('idr_to_usd'))
{
    /**
     * Convert IDR to USD
     * 
     * @param float
     * @param str online | db
     **/
    function idr_to_usd ($number = 1, $source = 'online') {
        if ($source == 'online') {
            return $number/one_usd_to_idr();
        } elseif ($source == 'db') {
            return $number/db_currency_conversion();
        }
    }
}
