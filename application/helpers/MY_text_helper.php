<?php

if(!function_exists('replace_base_url')){
    function remove_base_url($content){
        
        $content = str_replace(base_url(), '[base_url]', $content);
        
        return $content;
    }
}

if(!function_exists('fill_base_url')){
    function fill_base_url($content){
        
        $content = str_replace('[base_url]', base_url(), $content);
        
        return $content;
    }
}