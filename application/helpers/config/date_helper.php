<?php

if (!function_exists('user_date_format')) {
    function user_date_format($str_date, $format = NULL) {
        $format = get_site_config('date_format');
        return date($format, strtotime($str_date));
    }
}