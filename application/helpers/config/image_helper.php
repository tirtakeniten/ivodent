<?php

if(!function_exists('parse_wh'))
{
    /**
    * Parse width and height
    * @param string e.g 100x200
    * @return array
    **/
    function parse_wh($val)
    {
        return explode('x', $val);
    }
}

if(!function_exists('get_image_setting'))
{
    function get_image_setting()
    {
        return array(
            'thumbnail' => image_thumbnail_setting(),
            'medium' => image_medium_setting(),
            'large' => image_large_setting()
        );
    }
}

if(!function_exists('image_thumbnail_setting'))
{
    function image_thumbnail_setting()
    {
        $config = get_site_config('image_thumbnail');
        $configCrop = get_site_config('image_thumbnail_crop');
        $configR = parse_wh($config);

        return array(
            'width' => $configR[0],
            'height'=> $configR[1],
            'crop'  => (boolean) $configCrop,
            'string'=> $config
        );
    }
}

if(!function_exists('image_medium_setting'))
{
    function image_medium_setting()
    {
        $config = get_site_config('image_medium');
        $configCrop = get_site_config('image_medium_crop');
        $configR = parse_wh($config);

        return array(
            'width' => $configR[0],
            'height'=> $configR[1],
            'crop'  => (boolean) $configCrop,
            'string'=> $config
        );
    }
}

if(!function_exists('image_large_setting'))
{
    function image_large_setting()
    {
        $config = get_site_config('image_large');
        $configCrop = get_site_config('image_large_crop');
        $configR = parse_wh($config);

        return array(
            'width' => $configR[0],
            'height'=> $configR[1],
            'crop'  => (boolean) $configCrop,
            'string'=> $config
        );
    }
}

if(!function_exists('image_url'))
{
    /**
     * Get image URL
     *
     * @param object The file object
     * @param string The size thumbnail|medium|large|original|all
     **/
    function image_url($file, $size = 'original')
    {
        $baseUrl = 'uploads/images/';

        if($size == 'original')
        {
            $url = asset_url($baseUrl.$file->filename);
        }else{

            $image_setting = get_image_setting();
            $available_size = array_keys($image_setting);

            if(in_array($size, $available_size))
            {
                $url = asset_url($baseUrl."{$size}/".$file->filename);
            }else{
                $url = image_url($file, 'original');
            }
        }

        return $url;
    }
}

if(!function_exists('images_urls'))
{
    function image_urls($file)
    {
        $sizes = array(
            'thumbnail',
            'medium',
            'large',
            'original'
        );

        $url = array();

        foreach($sizes as $s)
        {
            $url[$s] = image_url($file, $s);
        }

        return $url;
    }
}
