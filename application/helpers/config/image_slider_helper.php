<?php 

if(!function_exists( 'get_image_slider' ))
{
    function get_image_slider()
    {
        $CI = &get_instance();
        $CI->load->model('file_model');
        
        require APPPATH.'controllers/admin/_setting/Image_slider.php';
        $sliderSetting = new Image_slider();
        
        $homeSlider = '';
        foreach($sliderSetting->fields() as $k => $field)
        {
            $homeSlider[$k] = get_site_config($k);
        }
        
        $count = count($homeSlider)/3;
        $return = array();
        for($i=1; $i<=$count; $i++)
        {
            if($homeSlider["slide_image{$i}"] != '')
            {
                $image = $CI->file_model->get( $homeSlider["slide_image{$i}"] );
                
                $return[] = array(
                    'image' => image_url($image),
                    'title' => $homeSlider["slide_title{$i}"],
                    'description' => $homeSlider["slide_description{$i}"],
                );
            }
        }
        return $return;
    }
}