<?php

if(!function_exists('get_site_config'))
{
    /**
     * Get the config value
     * 
     * @param string Config key
     * @param bool if false then return entire row value,
     *          return only the value
     * @return string or object
     **/
    function get_site_config( $configKey, $valueOnly = true)
    {
        $CI = &get_instance();
        return $CI->siteconfig->getConfig($configKey, $valueOnly);
    }
}