<?php

class Auth
{
    protected $userdata;
    protected $session_name = 'auth_user';
    protected $CI;
    protected $table = 'users';
    protected $fields = array(
        'username' => 'email',
        'password' => 'password',
        'token' => 'token'
    );
    protected $DB;
    protected $message;
    
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->DB = $this->CI->db;
    }
    
    /**
    * Start the authentication
    **/
    public function start($conditions)
    {
        /* encryot the password */
        $where = array();
        $where[$this->fields['username']] = $conditions[0];
        
        /* set where */
        $this->DB->where($where);
        
        /* user */
        $user = $this->DB->get($this->table)->row();
        
        /* no user */
        if($user == NULL)
        {
            $this->set_message('No account');
            return false;
        }
        
        /* match password */
        if( $this->decrypt( $user->{$this->fields['password']} ) != $conditions[1] )
        {
             /* Wrong password, bitch! */
             $this->set_message('Wrong password');
             return false;
        }
        
        /* set the userdata */
        $userdata = array(
            'token' => $this->generate_token( $user->{$this->fields['username']} )
        );
        $this->update_token($user->{$this->fields['username']}, $userdata['token']);
        $this->set_userdata($userdata);
        
        /* password match */
        return true;
    }
    
    /**
    * Reauth, check the user if already logged in
    **/
    public function restart()
    {
        $where = $this->userdata();
        
        if(empty($where))
        {
            return false;
        }
        
        /* set where */
        $this->DB->where($where);
        
        /* user */
        $user = $this->DB->get($this->table)->row();
        
        if($user == NULL)
        {
            $this->unset_userdata();
            return false;
        }
        
        return true;
    }
    
    /**
    * Stop! Delete all sessions
    **/
    public function end()
    {
        $user = $this->user();
        $this->update_token($this->fields['username'], '');
        return $this->CI->session->unset_userdata($this->session_name);
    }
    
    /**
    * Get the logged user
    **/
    public function userdata()
    {
        return $this->CI->session->userdata($this->session_name);
    }
    
    /**
    * Set the logged user
    **/
    public function set_userdata($user)
    {
        return $this->CI->session->set_userdata($this->session_name, $user);
    }
    
    /**
    * Remove the logged user
    **/
    public function unset_userdata()
    {
        return $this->CI->session->unset_userdata($this->session_name);
    }
    
    /**
    * Proceed token
    **/
    public function update_token($user, $token)
    {
        $this->DB->where(array(
            "{$this->fields['username']}" => $user
        ));
        
        $update = $this->DB->update($this->table, array(
            "{$this->fields['token']}" => $token
        ));
    }
    
    public function user()
    {
        $where = $this->userdata();
        
        if(is_null($where)) return NULL;
        
        /* set where */
        $this->DB->where($where);
        
        /* user */
        $user = $this->DB->get($this->table)->row();
        
        return $user;
    }
    
    public function message()
    {
        return $this->message;
    }
    
    public function set_message($message)
    {
        $this->message = $message;
    }
    
    /**
    * AUTH HELPER
    **/
    public function encrypt($password)
    {
        $this->CI->load->library('encrypt');
        return $this->CI->encrypt->encode($password);
    }
    
    public function decrypt($password)
    {
        $this->CI->load->library('encrypt');
        return $this->CI->encrypt->decode($password);
    }
    
    public function generate_token($username)
    {
        return $this->encrypt($username);
    }
}