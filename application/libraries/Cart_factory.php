<?php

class Cart_factory {
    
    private $_ci;
    private $_cart;
    private $_db;
    
    public function __construct() {
        
        $this->_ci = &get_instance();
        
        $this->_ci->load->library('cart');
        
        $this->_ci->load->model(array(
            'product_model'
        ));
        
        $this->_cart = $this->_ci->cart;
        
        return $this;
    }
    
    public function insert($product_id, $qty) {
        
        $product = $this->_ci->product_model->get($product_id);
        
        if (is_null($product)) throw new Exception('Product not found.'); 
        
        if ($qty <= 0) throw new Exception('Quantity must greater than 0.'); 
        
        $cart_row = array(
            'id'      => $product->id,
            'qty'     => $qty,
            'price'   => ($product->sale_price == 0) ? $product->price : $product->sale_price,
            'name'    => $product->name,
        );
        
        $cart_row['price'] = usd_to_idr($cart_row['price'], 'db');
        
        return $this->_cart->insert($cart_row);
    }
    
    public function contents ($with_product = FALSE) {
        $cart_content = $this->_cart->contents();
        
        if ($with_product) {
            foreach ($cart_content as $row_id => $cart_c) {
                $cart_content[$row_id]['product'] = $this->_ci->product_model->get($cart_c['id']);
            }
        }
        
        return $cart_content;
    }
    
    public function cart() {
        return $this->_cart;
    }
    
    public function grandtotal () {
        $total = $this->_cart->total();
        $shipping_data = $this->_ci->checkout_shipping->get_shipping();
        return $shipping_data->cost->cost[0]->value + $total;
    }
}