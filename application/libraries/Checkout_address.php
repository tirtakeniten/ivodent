<?php

class Checkout_address {
    
    private $_CI;
    private $_session;
    private $_session_name;
    
    public function __construct () {
        $this->_CI =&get_instance();
        $this->_session = $this->_CI->session;
        $this->_session_name = 'checkout_address';
    }
    
    public function set_address ($data = array()) {
        $this->_session->set_userdata($this->_session_name, $data);
        return $this;
    }
    
    public function get_address () {
        $data = $this->_session->userdata($this->_session_name);
        if (count($data) == '') { return NULL; }
        return (object) $data;
    }
    
    public function unset_shipping () {
        $this->_session->unset_userdata($this->_session_name);
        return $this;
    }
}