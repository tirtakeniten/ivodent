<?php

class Checkout_shipping {
    
    private $_CI;
    private $_session;
    private $_session_name;
    
    public function __construct () {
        $this->_CI =&get_instance();
        $this->_CI->load->library('ongkir_factory');
        $this->_session = $this->_CI->session;
        $this->_session_name = 'checkout_shipping';
    }
    
    public function set_shipping ($shipping_data = array()) {
        $this->_session->set_userdata($this->_session_name, $shipping_data);
        return $this;
    }
    
    public function get_shipping () {
        $data = $this->_session->userdata($this->_session_name);
        if (isset($data['cost'])) {
            $data['cost'] = json_decode($data['cost']);
        } else {
            return NULL;
        }
        return (object) $data;
    }
    
    public function unset_shipping () {
        $this->_session->unset_userdata($this->_session_name);
        return $this;
    }
    
    public function get_city () {
        $shipping = $this->get_shipping();
        $city = $this->_CI->ongkir_factory->getCity($shipping->province_id, $shipping->city_id);
        return $city;
    }
    
    public function get_province () {
        $shipping = $this->get_shipping();
        $province = $this->_CI->ongkir_factory->getProvince($shipping->province_id);
        return $province;
    }
    
    public function get_raw_data()
    {
        $shipping = $this->get_shipping();
        $shipping_raw = $this->_CI->ongkir_factory->getCost($shipping->city_id);
        return $shipping_raw;
    }
}