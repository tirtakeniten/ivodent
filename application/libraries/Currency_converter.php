<?php

class Currency_converter
{
    var $defaultCurrency;
    var $to;
    
    public function __construct()
    {
        $this->defaultCurrency = get_site_config('currency_position');
    }
    
    public function initialize()
    {
        
    }
    
    public function to($to)
    {
        $this->to = $to;
        return $this;
    }
    
    public function covert($number)
    {
        
    }
}