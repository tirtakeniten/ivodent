<?php

use Gregwar\Image\Image as Img;

class Image_upload
{
    protected $_CI;
    protected $_libs;
    protected $_db;
    
    var $upload_config = array();
    var $upload_error;
    
    public function __construct()
    {
        /* CI superclass */
        $this->_CI = &get_instance();
        
        /* libs */
        $this->_CI->load->library('upload');
        $this->_libs = $this->_CI->upload;
        
        /* model */
        $this->_CI->load->model('file_model');
        $this->_db = $this->_CI->file_model;
        
        $this->set_upload_config();
    }
    
    /**
     * Do upload
     **/
    public function do_upload($field)
    {
        /* setting */
        $this->_libs->initialize($this->upload_config);
        
        /* upload */
        $upload = $this->_libs->do_upload($field);
        
        if($upload)
        {
            $upload_data = $this->_libs->data();
            
            $this->save_to_db($upload_data);
            
            if($upload_data['is_image'])
            {
                $this->resize($upload_data);
            }
            return $upload_data;
        }else{
            $this->upload_error = $this->_libs->display_errors();
            return FALSE;
        }
    }
    
    /**
     * Returns Upload Error Messages
     **/
    public function upload_errors()
    {
        return $upload_error;
    }
    
    /**
     * Set Upload Configuration
     **/
    public function set_upload_config($config = array())
    {
        $this->upload_config = array(
            'upload_path'   => BASEPATH.'../assets/uploads/images/',
            'allowed_types' => '*'
        );
    }
    
    /* Resize picture */
    public function resize($data)
    {
        /* Get all image size */
        $image_sizes = get_image_setting();
        
        /* Sources */
        $source = $data['full_path'];
        
        foreach($image_sizes as $dir => $image_size)
        {
            /* open the image */
            $img = Img::open($source);
            
            /* target save path */
            $target = $data['file_path']."/{$dir}/".$data['file_name'];
            
            /* need a crop */
            if($image_size['crop'])
            {
                /* Crop! */
                $img->zoomCrop($image_size['width'], $image_size['height']);
            }else{
                /* Just resize */
                $img->cropResize($image_size['width'], $image_size['height']);
            }
            
            /* Then save */
            $img->save($target, $data['image_type'], 100);
        }
        
        /* BOOM, It's done */
    }
    
    /* save to database */
    public function save_to_db($data)
    {
        $values = array(
            'filename' => $data['file_name'], 
        	'type'     => $data['file_type'], 
        	'size'     => $data['file_size'], 
        	'metadata' => json_encode($data), 
        	'created_at'=> date("Y-m-d H:i:s")
        );
        
        $this->_db->insert($values);
    }
}