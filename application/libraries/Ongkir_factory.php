<?php

use hok00age\RajaOngkir as Ongkir;

class Ongkir_factory {
    
    protected $_shippingProvider = 'tiki';
    protected $_client;
    protected $_apiKey = 'c8b951ba25b1868a6f752d0b15df25ed';
    protected $_originCityID;
    
    public function __construct() {
        $this->_client = new Ongkir($this->_apiKey);
        $this->_originCityID = '114'; //Denpasar
        return $this;
    }
    
    /**
     * Get Province on RajaOngkir
     * @access public
     * @param null
     * @return array
     **/
    public function getProvince($provinceID = NULL) {
        $provinces = $this->_client->getProvince($provinceID);
        return self::getRajaOngkirResult($provinces);
    }
    
    /**
     * Get Cities on Province
     * @access public
     * @param int ProvinceID
     * @return array
     **/
    public function getCity($provinceID = NULL, $cityID = NULL ) {
        $cities = $this->_client->getCity($provinceID, $cityID);
        return self::getRajaOngkirResult($cities);
    }
    
    /**
     * Get Cost
     **/
    public function getCost($cityDestinationID, $weight = 1000) {
        $cost = $this->_client->getCost(
            $this->_originCityID, 
            $cityDestinationID, 
            $weight, 
            $this->_shippingProvider);
        return self::getRajaOngkirResult($cost);
    }
    
    /**
     * Get instance of "Client"
     * @access public
     * @param null
     * @return object
     **/
    public function client()
    {
        return $this->_client;
    }
    
    public function originCity ($city_code = NULL) {
        if ($city_code == NULL) {
            return $this->_originCityID;
        } else {
            $this->_originCityID = $city_code; 
            return $this;
        }
    }
    
    /**
     * Generate result of RajaOngkir's Object'
     * @param object
     * @return object or null
     **/
    protected static function getRajaOngkirResult($rajaOngkirObject) {
        
        $rajaongkir = $rajaOngkirObject->body->rajaongkir;
        if ($rajaongkir->status->code == 200) {
            return $rajaongkir->results;
        }
        
        return NULL;
    }
}