<?php

class SiteConfig
{
    protected $CI;
    protected $m;
    protected $config;
    
    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('setting_model');
        $this->m = $this->CI->setting_model;
    }
    
    public function setConfig($configKey, $value = null, $updateDatabase = false)
    {
        $conf = $this->getConfig($configKey, false);
        
        /* the value */
        if( is_null($value) )
        {
            return null;    
        }
        
        if(is_array($value))
        {
            $val = array(
                'label' => @$value['label'], 
            	'key' => @$value['key'], 
            	'value' => @$value['value']
            );
        }else{
            $val = array(
                'label' => $configKey,
                'key' => $configKey,
            	'value' => $value
            );
        }
        
        /* no config on database */
        if(is_null($conf))
        {            
            /* set the config */
            $this->config[$configKey] = (object) $val;
            
            if($updateDatabase)
            {
                $this->m->insert($val);
            }
            
        }else{
            $this->config[$configKey] = (object) $val;
            
            if($updateDatabase)
            {
                $this->m->update_by(array(
                    'key' => $configKey
                ), $val);
            }
        }
    }
    
    public function getConfig($configKey, $valueOnly = true)
    {
        if( !isset($this->config[$configKey]) )
        {
            $dbConf = $this->m->get_by(array(
                'key' => $configKey
            ));
            
            if(is_null($dbConf))
            {
                return NULL;
            }
            
            $this->config[$configKey] = $dbConf;
        }
        
        if($valueOnly)
        {
            return $this->config[$configKey]->value;
        }
        
        return $this->config[$configKey];
    }
}