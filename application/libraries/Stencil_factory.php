<?php

use Tirta\Adminlte as Lte;

class Stencil_factory
{
    var $CI;
    
    public function __construct()
    {
        $this->CI = &get_instance();
    }
    
    public function make()
    {
        /* the layout */
        $this->CI->stencil->layout('front-layout.php');
        
        /* JS */
        $this->CI->stencil->js(array(
            base_url( Lte\Plugin::jQuery('jQuery-2.1.4.min.js') ),
            'bootstrap.min.js',
            //'http://maps.google.com/maps/api/js?sensor=false',
            //'jquery.ui.map.js',
            'owl.carousel.min.js',
            'rate/jquery.raty.js',
            'labs.js',
            'product/lib/jquery.mousewheel-3.0.6.pack.js',
            'product/jquery.fancybox.js?v=2.1.5',
            asset_url('plugins/easyzoom/easyzoom.js'),
            'shop.js',
            'checkout.js'
        ));
        
        /* CSS */
        $this->CI->stencil->css(array(
            asset_url('font-awesome/css/font-awesome.min.css'),
            'bootstrap.min.css',
            asset_url('plugins/easyzoom/easyzoom.css'),
            'style.css',
            'owl.carousel.css',
            'owl.transitions.css',
            asset_url('js/product/jquery.fancybox.css?v=2.1.5'),
        )); 
        
        /* slices */
        $this->CI->stencil->slice(array(
            'head' => 'head',
            'header' => 'header',
            'footer' => 'footer'
        ));
        
        /* seo data */
        $this->CI->stencil->data(array(
            'seo_title' => get_site_config('default_title'),
            'seo_description' => get_site_config('default_description'),
            'seo_og_image' => $this->_default_og_imge( get_site_config('default_og_image') ),
            'google_site_verification' => get_site_config('google_site_verification'),
            'google_analytics' => get_site_config('google_analytics'),
            'twitter_account' => get_site_config('twitter_account'),
            'twitter_image' => $this->_default_tc_imge( get_site_config('default_og_image') ),
        ));
    }
    
    private function _default_og_imge ($id) {
        $image = $this->_seo_image($id);
        return image_url($image, 'medium');
    }
    
    private function _default_tc_imge ($id) {
        $image = $this->_seo_image($id);
        return image_url($image);
    }
    
    private function _seo_image ($id) {
        $this->CI->load->model('file_model');
        $image = $this->CI->file_model->get(get_site_config('default_og_image'));
        return $image;
    }
}