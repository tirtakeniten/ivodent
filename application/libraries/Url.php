<?php

class Url
{
    var $urls;
    protected $_messages = array();
    protected $_ci;
    
    public function __construct()
    {
        $this->_ci = &get_instance();
        $this->_ci->config->load('url');
        $this->urls = $this->_ci->config->item('url');
    }
    
    public function __call($function, $args)
    {
        if(count($args) == 0)
        {
            $base_url = '';
            
            $arg = '_base';
            $argURIsegment = '';
            
        }else{
            $base_url = $this->urls[$function]['_base'];
            
            $xArgs = $args;
            $arg = array_shift($xArgs);
            $argURIsegment = implode('/', $xArgs); 
        }
        
        if(isset($this->urls[$function][$arg])) 
            return site_url($base_url.$this->urls[$function][$arg].$argURIsegment);
        
        $this->_add_message("URL in {$function} / $arg not found!");
        
        return NULL;
    }
    
    public function message()
    {
        return $this->_messages;
    }
    
    private function _add_message($message)
    {
        $this->_messages = array_merge($this->_messages, array($message));
        return $this;
    }
}