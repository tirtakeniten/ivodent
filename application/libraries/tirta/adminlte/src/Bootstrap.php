<?php

namespace Tirta\Adminlte;

class Bootstrap
{
    public static function __callStatic($function, $args)
    {
        return self::_call($function, $args);
    }
    
    public function __call($function, $args)
    {
        return self::_call($function, $args);
    }
    
    protected static function _call($function, $args)
    {
        $base = Config::bootstrapPath();
        $dir = $base.$function.'/';
        
        $function = str_replace('_','-', $function);
        
        if(count($args) == 0)
        {
            return $dir; 
        }else{
            $a = implode('/', $args);
            return $dir.$a;
        }
    }
}