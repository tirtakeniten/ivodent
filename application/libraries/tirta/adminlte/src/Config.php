<?php

namespace Tirta\Adminlte;

class Config
{
    public static function basePath()
    {
        return 'vendor/almasaeed2010/adminlte/';
    }
    
    public static function bootstrapPath()
    {
        return self::basePath().'bootstrap/';
    }
    
    public static function appPath()
    {
        return self::basePath().'dist/';
    }
    
    public static function pluginsPath()
    {
        return self::basePath().'plugins/';
    }
}