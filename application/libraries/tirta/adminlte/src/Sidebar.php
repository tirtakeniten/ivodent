<?php

namespace Tirta\Adminlte;

class Sidebar
{
    public static $instance;
    
    protected $menus;
    
    public function __construct()
    {
        
    }
    
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new Sidebar();
        }
        return self::$instance;
    }
    
    public function addMenu($key, $label = '', $url = '', $order = 1, $overwrite = false)
    {
        $menus[$key] = array(
            'order' => $order,
            'label' => $label,
            'url' => $url,
            'subMenu' => array()
        );
        
        if(isset($this->menus[$key])){
            if($overwrite){
                $this->menus[$key] = $menus[$key];
            }else{
                return false;
            }
        }else{
            $this->menus[$key] = $menus[$key];
        }
    }
    
    public function addSubMenu($parentKey, $label, $url, $order)
    {
        
    }
    
    public function menuArray()
    {
        return $this->menus;
    }
}