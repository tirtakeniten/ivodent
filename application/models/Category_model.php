<?php

class Category_model extends MY_Model
{
    var $_table = 'categories';
    
    var $after_get = array(
        'featured_image'
    );
    
    var $before_create = array(
        'slugify'
    );
    
    var $before_update = array(
        'slugify'
    );
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'product_rel_category_model',
            'file_model'
        ));
        $this->load->helper('html');
    }
    
    public function slugify($row)
    {
        if(isset($row['slug']))
        {
            if(empty($row['slug']))
            {
                $row['slug'] = $row['name'];
            }
            
            $row['slug'] = url_title($row['slug'], '-', TRUE);
        }
        return $row;
    }
    
    public function with_parent()
    {
        $this->after_get = array_merge($this->after_get, array('parent'));
        return $this;
    }
    
    public function parent($row)
    {
        if(!empty($row->parent_id))
        {
            $row->parent = $this->get($row->parent_id);
        }
        
        return $row;
    }
    
    public function delete_rel_products($category_id)
    {
        $this->product_rel_category_model->delete_by(array('category_id' => $category_id));
    }
    
    public function list_dropdown($exclude = NULL)
    {
        $dropdown = ['0' => '--: Parent :--'];
        $categories = $this->get_hierarchical(0, $exclude);
        
        /* Parent Category */
        foreach( $categories as $category )
        {
            $dropdown[ $category->id ] = $category->name;
            
            /* Children Category */
            foreach( $category->children as $children_category )
            {
                $dropdown[ $children_category->id ] = ' - '.$children_category->name;
                
                /* Grandchildren Category */
                foreach( $children_category->children as $grandchildren_category )
                {
                    $dropdown[ $grandchildren_category->id ] = ' -- '.$grandchildren_category->name;
                }
                /* Grandchildren Category */
                
            }
            /* Children Category */
        }
        /* Parent Category */
        
        return $dropdown;
    }
    
    public function get_hierarchical($parent_id = 0, $exclude = NULL)
    {
        $this->db->where(array(
            'parent_id' => $parent_id
        ));
        
        if($exclude != NULL)
        {
            $this->db->where_not_in('id', array(
                $exclude
            ));
        }
        
        $categories = $this->get_all();
        
        foreach($categories as $key => $category)
        {
            $categories[$key]->children = $this->get_hierarchical($category->id, $exclude);
        }
        
        return $categories;
    }
    
    public function featured_image($row)
    {
        /* content */
        if( isset($row->featured_image) && 
            !empty($row->featured_image) )
        {
            $file = $this->file_model->get($row->featured_image);
            $row->featured_image_url = image_urls($file);
        }
        
        return $row;
    }  
}