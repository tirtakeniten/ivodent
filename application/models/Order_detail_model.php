<?php

class Order_detail_model extends MY_Model
{
    var $_table = 'order_details';
    
    var $belongs_to = array(
        'product' => array(
            'model' => 'product_model',
            'primary_key' => 'prduct_id'
        )
    );
    
    var $after_get = array(
        'json_decode'
    );
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_by_order ($order_id) {
        $this->_database->where(array(
            'order_id' => $order_id
        ));
        return $this->get_all();
    }
    
    public function json_decode($row) {
        if (isset($row->product_raw_data)) {
            $row->product = json_decode($row->product_raw_data);
        }
        return $row;
    }
}