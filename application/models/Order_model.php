<?php

class Order_model extends MY_Model
{
    var $_table = 'orders';
    
    var $prefix = 'IVODENT';
    
    var $before_create = array(
        'fill_data',
        'json_encode'
    );
    
    var $before_update = array(
        'json_encode'
    );
    
    var $after_get = array(
        'json_decode'
    );
    
    var $belongs_to = array(
        'user' => array(
            'model'       => 'user_model',
            'primary_key' => 'user_id'
        ),
        'status' => array(
            'model'       => 'order_status_model',
            'primary_key' => 'status_id'
        )
    );
    
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model(array(
            'order_detail_model'
        ));
    }
    
    public function fill_data ($row) {
        if (isset($row['code'])) {
            $row['code'] = $this->generate_code();
        }
        return $row;
    }
    
    public function generate_code () {
        $date = date('Ymd');
        $prefix = $this->prefix.$date;
        
        $this->_database->like('code', $prefix);
        $last_order = $this->order_by('id', 'desc')
                            ->limit(1)
                            ->get_all();
                            
        if (count($last_order) == 0) {
            $last_code = $prefix.'000';
        } else {
            $last_code = $last_order[0]->code;
        }
        
        $code = str_replace($prefix, '', $last_code);
        $code = (int) $code;
        $code++;
        while (strlen($code) < 4) {
            $code = '0'.$code;
        }
        return $prefix.$code;
    }
    
    public function json_encode ($row) {
        if (isset($row['address'])) {
            $row['address'] = json_encode($row['address']);
        }
        if (isset($row['shipping_info'])) {
            $row['shipping_info'] = json_encode($row['shipping_info']);
        }
        return $row;
    }
    
    public function json_decode ($row) {
        if (isset($row->address)) {
            $row->address = json_decode($row->address);
        }
        if (isset($row->shipping_info)) {
            $row->shipping_info = json_decode($row->shipping_info);
        }
        return $row;
    }
    
    public function sendEmailStatus ($id) {
        /* Get full order data */
        $data = $this->getData($id);
        
        /* The message */
        $message = $this->load->view('mail/email_order_status', $data, TRUE);
        
        //echo $message;
        
        /* SEND EMAIL */
        $this->load->library('email');
            
        /* to customer */
        $this->email->mailtype = 'html';
        $this->email->from($this->config->item('contact_email_from'), $this->config->item('contact_name_from'));
        $this->email->to($data['order']->user->email);
        $this->email->reply_to($this->config->item('contact_email_reply'));
        
        $this->email->subject('[Ivodent] Your order is moving forward');
        $this->email->message($message);
        
        $this->email->send();
    }
    
    public function getData ($id) {
        $data['order'] = $this->with('user')
                            ->with('status')
                            ->get($id);
        
        $data['order_details'] = $this->order_detail_model
                        ->get_by_order($data['order']->id);
        
        return $data;
    }
}