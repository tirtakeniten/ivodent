<?php

class Page_model extends MY_Model
{
    var $_table = 'pages';
    
    var $before_create = array(
        'fill_data',
        'timestamp_create'
    );
    
    var $before_update = array(
        'fill_data',
        'timestamp_update'
    );
    
    var $after_get = array(
        'after_get'
    );
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('file_model');
    }
    
    public function after_get($row)
    {
        /* content */
        if( isset($row->content) && 
            $row->content != '' )
                $row->content = fill_base_url($row->content);
                
        /* content */
        if( isset($row->featured_image) && 
            $row->featured_image != '' )
        {
            $file = $this->file_model->get($row->featured_image);
            $row->featured_image_url = image_urls($file);
        }
        
        return $row;
    }
    
    public function fill_data($row)
    {
        /* slug */
        if(isset($row['slug']) && $row['slug'] == '') $row['slug'] = $row['title'];
        if(isset($row['slug'])) $row['slug'] = url_title($row['slug'], '-', TRUE);
        
        /* content */
        if( isset($row['content']) && 
            $row['content'] != '' )
                $row['content'] = remove_base_url($row['content']);
        
        /* excerpt */
        if( isset($row['excerpt']) && 
            $row['excerpt'] == '' && 
            isset($row['seo_description']) )
                $row['excerpt'] = $row['seo_description'];
        
        /* seo title */
        if( isset($row['seo_title']) && 
            $row['seo_title'] == '')
                $row['seo_title'] = $row['title'];
        
        /* seo description */
        if( isset($row['seo_description']) && 
            $row['seo_description'] == '' && 
            isset($row['excerpt']) )
                $row['seo_description'] = $row['excerpt'];
        
        return $row;
    }
    
    public function timestamp_create($row)
    {
        $row['created_at'] = date("Y-m-d H:i:s");
        
        return $row;
    }
    
    public function timestamp_update($row)
    {
        $row['updated_at'] = date("Y-m-d H:i:s"); 
        
        return $row;
    }
}