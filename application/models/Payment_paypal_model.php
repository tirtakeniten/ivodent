<?php

class Payment_paypal_model extends MY_Model
{
    var $_table = 'payment_paypal';
    
    var $before_create = array(
        'jsonEncode'
    );
    
    var $before_update = array(
        'jsonEncode'
    );
    
    var $after_get = array(
        'jsonDecode'
    );
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function jsonEncode ($row) {
        if (isset($row['paypal_data'])) {
            $row['paypal_data'] = json_encode($row['paypal_data']);
        }
        return $row;
    }
    
    public function jsonDecode ($row) {
        if (isset($row->paypal_data)) {
            $row->paypal_data = json_decode($row->paypal_data);
        }
        return $row;
    }
}
