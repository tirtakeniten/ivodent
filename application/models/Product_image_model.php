<?php

class Product_image_model extends MY_Model
{
    var $_table = 'product_images';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function exist($product_id, $file_id)
    {
        $record = $this->get_by(array(
            'product_id' => $product_id, 
            'file_id' => $file_id
        ));
        
        if($record) return true;
        return FALSE;
    }
    
    public function get_images_id_of($product_id)
    {
        $records = $this->get_many_by(array(
            'product_id' => $product_id
        ));
        
        $file_ids = array();
        
        foreach($records as $records)
        {
            $file_ids[] = $records->file_id;
        }
        
        return $file_ids;
    }
}