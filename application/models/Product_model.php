<?php

class Product_model extends MY_Model
{
    var $_table = 'products';

    var $after_get = array(
        'apply_images'
    );
    
    var $before_create = array(
        'slugify'
    );
    
    var $before_edit = array(
        'slugify'
    );
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'product_rel_category_model',
            'product_image_model',
            'file_model',
            'varian_model'
        ));
    }
    
    public function get_inventory_options()
    {
        return array(
            '1' => 'Yes',
            '0' => 'No'
        );
    }

    public function slugify($row)
    {
        if(isset($row['slug']))
        {
            if(empty($row['slug']))
            {
                $row['slug'] = $row['name'];
            }
            
            $row['slug'] = url_title($row['slug'], '-', TRUE);
        }
        return $row;
    }

    public function set_categories($product_id, $category_ids)
    {
        if(is_array($category_ids))
        {
            /* add categories */
            foreach($category_ids as $category_id)
            {
                $this->set_categories($product_id, $category_id);
            }

            /* remove category */
            $current_cat_ids = $this->product_rel_category_model->get_category_id_of($product_id);
            $diff = array_diff($current_cat_ids, $category_ids);

            if(count($diff) > 0)
            {
                $this->db->where_in('category_id', $diff);
                $this->product_rel_category_model->delete_by(array(
                    'product_id' => $product_id
                ));
            }

        }else{
            /* check if exist */
            if(!$this->product_rel_category_model->exist($product_id, $category_ids))
            {
                $this->product_rel_category_model->insert(array(
                    'product_id' => $product_id,
                    'category_id' => $category_ids
                ));
            }
        }
    }

    public function set_images($product_id, $files_id)
    {
        if(is_array($files_id))
        {
            /* add categories */
            foreach($files_id as $file_id)
            {
                $this->set_images($product_id, $file_id);
            }

            /* remove category */
            $current_img_ids = $this->product_image_model->get_images_id_of($product_id);
            $diff = array_diff($current_img_ids, $files_id);

            if(count($diff) > 0)
            {
                $this->db->where_in('file_id', $diff);
                $this->product_image_model->delete_by(array(
                    'product_id' => $product_id
                ));
            }

        }else{
            /* check if exist */
            if(!$this->product_image_model->exist($product_id, $files_id))
            {
                $this->product_image_model->insert(array(
                    'product_id' => $product_id,
                    'file_id' => $files_id
                ));
            }
        }
    }

    public function delete_rel_categories($product_id)
    {
        $this->product_rel_category_model->delete_by(array('product_id' => $product_id));
    }

    public function delete_varians($product_id)
    {
        $this->varian_model->delete_by(array('product_id' => $product_id));
    }

    public function apply_images($row)
    {
        if(isset($row->id))
        {
            $imagesID = $this->product_image_model->get_many_by(array(
                'product_id' => $row->id
            ));

            $images = array();

            foreach($imagesID as $imageID)
            {
                $file = $this->file_model->get($imageID->file_id);
                $images[] = image_urls($file);
            }

            $row->images = $images;
        }

        return $row;
    }
}
