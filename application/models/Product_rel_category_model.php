<?php

class Product_rel_category_model extends MY_Model
{
    var $_table = 'products_rel_categories';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function exist($product_id, $category_id)
    {
        $record = $this->get_by(array(
            'product_id' => $product_id, 
            'category_id' => $category_id
        ));
        
        if($record) return true;
        return FALSE;
    }
    
    public function get_category_id_of($product_id)
    {
        $records = $this->get_many_by(array(
            'product_id' => $product_id
        ));
        
        $cat_ids = array();
        
        foreach($records as $records)
        {
            $cat_ids[] = $records->category_id;
        }
        
        return $cat_ids;
    }
    
    public function product_by_category_id($category_id)
    {
        $records = $this->get_many_by(array(
            'category_id' => $category_id
        ));
        
        $product_ids = array();
        
        foreach($records as $records)
        {
            $product_ids[] = $records->product_id;
        }
        
        return $product_ids;
    }
}