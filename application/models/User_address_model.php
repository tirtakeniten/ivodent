<?php

class User_address_model extends MY_Model
{
    var $_table = 'user_addresses';
    
    var $after_get = array(
        'parse_username'
    );
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
    } 
    
    public function parse_username($row)
    {
        if(!is_null($row) && isset($row->name))
        {
            $name = explode(' ', $row->name);
            $row->first_name = $name[0];
            unset($name[0]);
            $row->last_name = implode(' ', $name);
        }
        
        return $row;
    }   
}