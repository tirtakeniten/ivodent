<?php

class User_model extends MY_Model
{
    var $_table = 'users';
    
    var $before_create = array(
        'encrypt_password'
    );
    
    var $before_update = array(
        'encrypt_password'
    );
    
    var $after_create = array(
        'send_email'
    );
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
    }
    
    public function encrypt_password($row)
    {
        if (isset($row['password']) && $row['password'] != '') {
            $row['password'] = $this->auth->encrypt($row['password']);
        }
        return $row;
    }
    
    public function parse_username($row)
    {
        if(!is_null($row) && isset($row->username))
        {
            $name = explode(' ', $row->username);
            $row->first_name = $name[0];
            unset($name[0]);
            $row->last_name = implode(' ', $name);
        }
        
        return $row;
    }
    
    public function send_email ($row) {
        
        $user = $this->get($row);
        
        $data = array(
            'user' => $user,
            'auth' => $this->auth,
        );
        
        $message = $this->load->view('mail/user-registration', $data, true);
        
        $this->load->library('email');
        
        /* to customer */
        $this->email->mailtype = 'html';
        $this->email->from('no-reply@ivodent.com', 'Ivodent');
        $this->email->to($user->email);
        $this->email->reply_to('tirta@balibagus.com');
        
        $this->email->subject('[Ivodent] Your Account Successfully Created');
        $this->email->message($message);
        
        $this->email->send();
        
        return $row;
    }    
}