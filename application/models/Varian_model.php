<?php

class Varian_model extends MY_Model
{
    var $_table = 'varians';
    
    var $after_get = array(
        'process_title'
    );
    
    var $value_separator = ' &bull; ';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function process_title($row)
    {
        $values = array();
        $title = array();
        
        $keys_field = array(
            'varian1_key', 
        	'varian2_key',  
        	'varian3_key', 
        );
        $vals = array(
        	'varian1_value',
        	'varian2_value', 
        	'varian3_value'
        );
        
        foreach($keys_field as $key_field)
        {
            if( isset($row->{$key_field}) ) $title[] = $row->{$key_field};
        }
        $row->options = implode(
            $this->value_separator, 
            array_filter($title)
        );
        
        foreach($vals as $val)
        {
            if( isset($row->{$val}) ) $values[] = $row->{$val};
        }
        $row->values = implode(
            $this->value_separator, 
            array_filter($values)
        );
        
        return $row;
    }
}