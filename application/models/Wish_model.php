<?php

class Wish_model extends MY_Model
{
    var $_table = 'wishes';
    
    var $belongs_to = array(
        'product' => array(
            'model' => 'product_model',
            'primary_key' => 'product_id'
        )
    );
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
    }
    
    public function is_exist ($product_id)
    {
        $user = $this->auth->user();
        if (is_null($user)) {
            return false;
        } else {
            $wish = $this->get_by(array(
                'user_id' => $user->id,
                'product_id' => $product_id
            ));
            if ( is_null($wish) ) {
                return false;
            }
            
            return true;
        }
    }
}