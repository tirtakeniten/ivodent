<?php
    $CI = &get_instance();
    $url = new Url();
    $activeUser = $CI->auth->user();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo get_site_config('site_name'); ?> | Admin Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="<?php echo $url->admin(); ?>" name="siteURL"/>
    
    <?php echo $css ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue-light sidebar-mini fixed">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="../../index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><i class="fa fa-tachometer"></i></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Admin</b>Panel</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php echo $activeUser->username; ?>
                  <i class="fa fa-user"></i>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> Edit Profile
                        </a>
                    </li>
                    <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> Change Password
                        </a>
                    </li>
                    <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> My account 
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('user/logout'); ?>">
                          <i class="fa fa-users text-aqua"></i> Logout 
                        </a>
                    </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <?php echo $sidebar; ?>

      <?php echo $content; ?>

      <footer class="main-footer">
        <p class="">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <?php echo $js; ?>
  </body>
</html>
