<!DOCTYPE html>
<html lang="en">
    <?php echo $head; ?>
    <body>
        <div id="wrapper">
        <?php echo $header; ?>
        
        <?php echo $content; ?>
        
        <?php echo $footer; ?>    
        </div>
        <?php echo $js; ?>
        
        <div id="ajax-loading">
        </div>
    </body>
</html>
