<?php 
    $CI = &get_instance();
?>
Dear Ivodent,
<br />
<br />
We have received the following request from your website <?php echo site_url(); ?>:
<br />
<br />
PERSONAL INFORMATION<br />
Name: <?php echo $data['name'] ?><br />
Email: <?php echo $data['email'] ?><br />
Subject: <?php echo $data['subject'] ?><br />
Message: <?php echo $data['message'] ?><br />
Timestamp: <?php echo user_date_format(strtotime('now')); ?><br />
User Agent: <?php echo $CI->input->user_agent(); ?><br />
IP Address: <?php echo $CI->input->ip_address(); ?><br />
<br />
<br />
Please proceed customer's request immediately.
<br />
<br />
Thank you 