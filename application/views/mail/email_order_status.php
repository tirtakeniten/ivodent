<?php

$CI =& get_instance();

?>
<div>
    Dear <?php echo $order->user->username; ?>,
    <br />
    <br />
    Thank you for shopping with us. We have update your order information as detailed below:<br />
    <br />
    <table>
        <tr>
            <td>Order code: </td>
            <td><strong><?php echo $order->code; ?></strong></td>
        </tr> 
        <tr>
            <td>Date: </td>
            <td><strong><?php echo user_date_format($order->date); ?></strong></td>
        </tr>   
        <tr>
            <td>Status: </td>
            <td><strong><?php echo $order->status->name; ?></strong></td>
        </tr>
        <tr>
            <td>Total Purchase: </td>
            <td><strong>IDR <?php echo format_number($order->grandtotal); ?></strong></td>
        </tr>
        <tr>
            <td>Total Items: </td>
            <td><strong><?php echo format_number($order->total_item); ?></strong></td>
        </tr>
        <tr>
            <td>Order Detail URL: </td>
            <td><?php echo site_url('account/order/'.$order->id); ?></td>
        </tr>
    </table> 
    <br />
    <br />
    Thank you,
    <br />
    <br />
    Ivodent
</div>