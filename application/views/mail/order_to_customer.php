<html>
    <head>
        <title>Ivodent Order Number <?php echo $order->code; ?></title>
    </head>
    <body style="background-color: #f6f6f6; font-family: sans-serif;">
        <div id="wrapper" style="max-width: 600px; color: #555; margin: 0 auto;">
            <h1>Hi <?php echo $order->user->username; ?>,</h1>
            <p>Thanks for making order with us. Below is a summary of your recent order.</p>
            
            <table style="font-family: sans-serif; color: #555; font-size: 13px;">
                <tr>
                    <td>Order code: </td>
                    <td><strong><?php echo $order->code; ?></strong></td>
                </tr> 
                <tr>
                    <td>Date: </td>
                    <td><strong><?php echo user_date_format($order->date); ?></strong></td>
                </tr>   
                <tr>
                    <td>Status: </td>
                    <td><strong><?php echo $order->status->name; ?></strong></td>
                </tr>
                <tr>
                    <td>Total Purchase: </td>
                    <td><strong>IDR <?php echo format_number($order->grandtotal); ?></strong></td>
                </tr>
                <tr>
                    <td>Total Items: </td>
                    <td><strong><?php echo format_number($order->total_item); ?></strong></td>
                </tr>
            </table>
            
            <p>You have bought:</p>
            
            <table style="font-family: sans-serif; color: #555; font-size: 14px; border-collapse: collapse;">
                <thead>
                    <tr style="background-color: #444; color: #f7f7f7;">
                        <th colspan="2" style="padding: 6px 13px; text-align: left;">ITEM DETAILS</th>
                        <th style="padding: 6px 13px; text-align: right;">SUBTOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($order_details as $details) : ?>
                    <?php $product = $details->product; ?>
                    <tr style="border-bottom: 1px solid #444; ">
                        <td style="vertical-align: top; padding: 6px 13px; text-align: left; width: 10%;">
                            <img src="<?php echo $product->images[0]->thumbnail; ?>" style="max-width: 100px;" />
                        </td>
                        <td style="vertical-align: top; padding: 6px 13px; text-align: left; width: 50%;">
                            <p style="margin-top: 10px;">
                                <a href="<?php  ?>" style="color: #3C72D8; text-decoration: none;"><?php echo $product->name; ?></a>
                            </p>
                            <p><strong><?php echo format_number($details->qty) ?> &times; IDR <?php echo format_number($details->price) ?></strong></p>
                        </td>
                        <td style="vertical-align: top; padding: 6px 13px; text-align: right;">
                            <p style="margin-top: 10px;">
                                <strong>IDR <?php echo format_number($details->subtotal); ?></strong>
                            </p>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="2" style="vertical-align: top; padding: 16px 13px 6px; text-align: right;">
                            <strong>Total:</strong>
                        </th>
                        <th style="vertical-align: top; padding: 16px 13px 6px; text-align: right;">
                            <strong>IDR <?php echo format_number($order->total); ?></strong>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="2" style="vertical-align: top; padding: 6px 13px; text-align: right;">
                            <strong>Shipping Cost:</strong>
                        </th>
                        <th style="vertical-align: top; padding: 6px 13px; text-align: right;">
                            <strong>IDR <?php echo format_number($order->shipping_cost); ?></strong>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="2" style="vertical-align: top; padding: 6px 13px; text-align: right;">
                            <strong>Grandtotal:</strong>
                        </th>
                        <th style="vertical-align: top; padding: 6px 13px; text-align: right;">
                            <strong>IDR <?php echo format_number($order->grandtotal); ?></strong>
                        </th>
                    </tr>
                </tfoot>
            </table>
            <hr />
            <p>Shipping address:</p>
            <p>
                <?php echo $order->address->name; ?><?php if ($order->address->company != '') { echo "(".$order->address->company.")"; } ?><br />
                <?php echo $order->address->address; ?><br />
                <?php echo $order->address->city; ?> - 
                <?php echo $order->address->province; ?> <?php echo $order->address->post_code; ?><br />
                <?php echo $order->address->email; ?> / <?php echo $order->address->phone; ?>
            </p>
            <hr />
            <p>Payment:</p>
            <p><a href="<?php echo site_url('pay/paypal'); ?>?code=<?php echo $order->code; ?>" class="btn btn-info btn-lg" style="padding: 10px 20px; background-color: #5bc0de; color: #fff;">Pay with PayPal</a></p>
        </div>
    </body>
</html>