<?php

$CI =& get_instance();

?>
<div>
    Dear <?php echo $order['order']->user->username; ?>,
    <br />
    <br />
    You have made payment on Ivodent by PayPal as detailed below:<br />
    <br />
    PayPal Transaction ID: <?php echo $paypal->paypal_data->PAYMENTINFO_0_TRANSACTIONID; ?><br />
    Currency: <?php echo $paypal->paypal_data->PAYMENTINFO_0_CURRENCYCODE; ?><br />
    Amount: <?php echo $paypal->paypal_data->PAYMENTINFO_0_AMT; ?><br />
    <br />
    We thank you for making payment. If you see anything wrong on the details above, please feel free to contact us. 
    <br />
    <br />
    Thank you,
    <br />
    <br />
    Ivodent
</div>