<?php
    $CI = &get_instance();
?>
Dear <?php echo $user->username; ?>,
<br />
<br />
Your login information for Ivodent is below:
<br />
<br />
Email: <?php echo $user->email; ?><br />
Password: <?php echo $new_password; ?>
<br />
<br />
Now you can login at Ivodent by folowing this link: <br />
<?php echo site_url('account-login'); ?><br />
Please change your password after login.<br />
<br />
<br />
Regards,<br />
Ivodent