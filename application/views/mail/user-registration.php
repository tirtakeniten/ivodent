<html>
    <head>
        <title>Your Account Successfully Created</title>
    </head>
    <body style="background-color: #f6f6f6; font-family: sans-serif; padding-top: 20px;">
        <div id="wrapper" style="max-width: 600px; color: #555; margin: 0 auto;">
            <h1>Hi <?php echo $user->username; ?>,</h1>
            <p>Thank you for registering at Ivodent. Below is you account login information.</p>
            <p>Your Name: <?php echo $user->username; ?><br />
            Email: <?php echo $user->email; ?><br />
            Password: <?php echo $auth->decrypt($user->password); ?></p>
            <p>To login please go to this url: <?php echo site_url('account-login'); ?></p>
        </div>
    </body>
</html>