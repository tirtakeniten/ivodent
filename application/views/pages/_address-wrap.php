<p><?php echo $primary_address->name; ?> (<?php echo $primary_address->company; ?>)</p>
<p><?php echo $primary_address->address; ?></p>
<p><?php echo $primary_address->city; ?> - <?php echo $primary_address->post_code; ?>, <?php echo $primary_address->country; ?></p>
<p><?php echo $primary_address->email; ?></p>
<p><?php echo $primary_address->phone; ?></p>
<p>
<a href="<?php echo site_url('account/address-form/'.$primary_address->id); ?>">Edit address</a>
<?php if ($enable_show_address): ?>
 | <a href="<?php echo site_url('account/address'); ?>">Show My Address</a>
<?php endif; ?>
<?php if ($enable_delete): ?>
<br /> <a href="<?php echo site_url('account/address-form/'.$primary_address->id); ?>">Delete address</a>
<?php endif; ?>
<?php if ($show_default): ?>
<br />
<?php if ($primary_address->default != 1): ?>
<a href="<?php echo site_url('account/address-default/'.$primary_address->id); ?>">Make default address</a>
<?php else: ?>
<br />
<?php endif; ?>
<?php endif; ?>
</p>