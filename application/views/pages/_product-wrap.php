<div class="productwrap">
    <div class="pr-img">
        <a href="<?php echo site_url('product/'.$product_wrap->slug); ?>" title="<?php echo $product_wrap->name; ?>">
            <img src="<?php echo @$product_wrap->images[0]['medium'] ?>" alt="<?php echo $product_wrap->name; ?>" class="img-responsive"/>
        </a>
    </div>
        <h5 class="smalltitle"><a href="<?php echo site_url('product/'.$product_wrap->slug); ?>"><?php echo $product_wrap->name; ?></a></h5>
        <h5 class="smalltitle">Product Code: <?php echo $product_wrap->sku; ?></h5>
        <p class="pricewrap">
            <?php if($product_wrap->sale_price > 0): ?>
            <span>Our Price: <br />
            <strong>IDR <?php echo format_number(usd_to_idr($product_wrap->sale_price, 'db')); ?></strong>
            </span><span style="display: none;"><?php echo currency_format($product_wrap->sale_price); ?></span><br />
            <span>Retail Price: </span><br />
            <span class="strikeout">
            IDR <?php echo format_number(usd_to_idr($product_wrap->price)); ?>
            (<?php echo currency_format($product_wrap->price); ?>)
            </span><br />
            <?php else: ?>
            <span>Our Price: 
            <?php echo currency_format($product_wrap->price); ?>
            </span>
            <?php endif; ?>
        </p>
        <?php echo form_open(site_url('cart/add'), 'role="form" class="productwrap-form"'); ?>
        <input type="hidden" value="<?php echo $product_wrap->id ?>" name="product_id"/>
        <div class="form-group">
            <label>Qty: </label>
            <input type="number" name="qty" min="1" />
        </div>
        <p><button class="btn" type="submit">Add To Cart</button></p>
        <?php echo form_close(); ?>
</div>