<div class="productwrap">
    <div class="pr-img">
        <a href="<?php echo site_url('product/'.$product_wrap->slug); ?>" title="<?php echo $product_wrap->name; ?>">
            <img src="<?php echo @$product_wrap->images[0]['medium'] ?>" alt="<?php echo $product_wrap->name; ?>" class="img-responsive"/>
        </a>
    </div>
        <h5 class="smalltitle"><a href="<?php echo site_url('product/'.$product_wrap->slug); ?>"><?php echo $product_wrap->name; ?></a></h5>
        <h5 class="smalltitle">Product Code: <?php echo $product_wrap->sku; ?></h5>
        <p class="pricewrap">
            <?php if($product_wrap->sale_price > 0): ?>
            <span>Our Price: 
            <?php echo currency_format($product_wrap->sale_price); ?>
            </span><br />
            <span class="strikeout">Retail Price: 
            <?php echo currency_format($product_wrap->price); ?>
            </span><br />
            <span>You Save: 
            <?php echo currency_format($product_wrap->price - $product_wrap->sale_price); ?>
            </span><br />
            <?php else: ?>
            <span>Our Price: 
            <?php echo currency_format($product_wrap->price); ?>
            </span>
            <?php endif; ?>
        </p>
        <p>
            <a href="<?php echo site_url('wishlist/remove/'.$wish->id); ?>" class="btn btn-red btn-sm del-wishlist">
                <i class="fa fa-trash-o"></i> Remove from Wishlist
            </a>
        </p>
</div>