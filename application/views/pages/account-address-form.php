<section>    
    <div class="container">
        <!-- Promotional content -->
        <div id="title-bg">
			<div class="title"><?php echo $page_title; ?></div>
		</div>
        <?php echo form_open(current_url().'?redirect='.$this->input->get('redirect'), 'class="form-horizontal checkout"'); ?>
        <input type="hidden" value="<?php echo set_value('id', @$address->id); ?>" name="id" />
        <input type="hidden" value="<?php echo set_value('user_id', @$account->id); ?>" name="user_id" />
        <div class="row">
            <div class="col-sm-6 bill">
                <?php if (validation_errors()) : ?>
                <div class="alert alert-danger">
                    <?php echo validation_errors(); ?>
                </div>
                <?php endif; ?>
                <div class="form-group dob">
					<div class="col-sm-6">
						<input type="text" name="fname" value="<?php echo set_value('fname', @$address->first_name); ?>" class="form-control" id="name" placeholder="First Name">
					</div>
					<div class="col-sm-6">
						<input type="text" name="lname" value="<?php echo set_value('lname', @$address->last_name); ?>" class="form-control" id="last_name" placeholder="Last Name">
					</div>
				</div>
                <div class="form-group dob">
					<div class="col-sm-12">
						<input type="text" name="company" value="<?php echo set_value('company', @$address->company); ?>" class="form-control" id="company" placeholder="Company">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" name="address" value="<?php echo set_value('address', @$address->address); ?>" class="form-control" id="address" placeholder="Address">
					</div>
				</div>
				<div class="form-group dob">
					<div class="col-sm-6">
						<input type="text" name="city" value="<?php echo set_value('city', @$address->city); ?>" class="form-control" id="city" placeholder="City">
					</div>
					<div class="col-sm-6">
						<input type="text" name="post_code" value="<?php echo set_value('post_code', @$address->post_code); ?>" class="form-control" id="post_code" placeholder="Post Code">
					</div>
				</div>
				<div class="form-group dob">
					<div class="col-sm-6">
						<input type="text" name="province" value="<?php echo set_value('province', @$address->province); ?>" class="form-control" id="province" placeholder="Province">
					</div>
                    <div class="col-sm-6">
						<input type="text" name="country" value="<?php echo set_value('country', 'Indonesia'); ?>" class="form-control" id="country" placeholder="Country" value="Indonesia" readonly="">
					</div>
				</div>
				<div class="form-group dob">
                    <div class="col-sm-6">
						<input type="text" name="phone" value="<?php echo set_value('phone', @$address->phone); ?>" class="form-control" id="phone" placeholder="Phone Number">
					</div>
                    <div class="col-sm-6">
						<input type="text" name="email" value="<?php echo set_value('email', @$address->email); ?>" class="form-control" id="email" placeholder="Email">
					</div>
				</div>
                <button class="btn btn-default btn-red"><?php echo $page_title; ?></button>
            </div>
            <div class="col-md-3 col-md-push-3">
                <?php echo $account_sidebar; ?>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- Promotional content -->
    </div>
    
    <div class="spacer"></div>
</section>