<section>    
    <div class="container">
        <!-- Promotional content -->
        <div id="title-bg">
			<div class="title">Change Password</div>
		</div>
        <?php echo form_open(current_url(), 'class="form-horizontal checkout"'); ?>
        <div class="row">
            <div class="col-sm-6 bill">
                <?php if (validation_errors()): ?>
                <div class="alert alert-danger">
                    <?php
                        echo validation_errors();
                    ?>
                </div>
                <?php endif; ?>
                <div class="form-group dob">
					<div class="col-sm-8">
						<input type="password" name="password" value="" class="form-control" id="name" placeholder="Current password">
					</div>
				</div>
                <div class="form-group dob">
					<div class="col-sm-8">
						<input type="password" name="new_password" value="" class="form-control" id="name" placeholder="New password">
					</div>
				</div>
                <div class="form-group dob">
					<div class="col-sm-8">
						<input type="password" name="cofirm_new_password" value="" class="form-control" id="name" placeholder="Confirm New password">
					</div>
				</div>
                <button class="btn btn-default btn-red">Update Password</button>
            </div>
            <div class="col-md-3 col-md-push-3">
                <?php echo $account_sidebar; ?>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- Promotional content -->
    </div>
    
    <div class="spacer"></div>
</section>