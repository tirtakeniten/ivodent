<div class="container">
    <div class="row">
        <div class="col-md-6 al-cus">
			<div id="title-bg">
				<div class="title">Already Customer</div>
			</div>
			<?php echo form_open(current_url().'?redirect='.$redirect); ?>
				<p>
					I am returning customer
				</p>
                <?php if(isset($message)): ?>
                <div class="alert alert-danger">
                    Wrong email or password.
                </div>
                <?php endif; ?>
				<div class="form-group">
					<input type="text" name="email" class="form-control" id="exampleInput" placeholder="Username">
				</div>
				<div class="form-group">
					<input type="password" name="password" class="form-control" id="exampleInpupas" placeholder="Password">
				</div>
				<button class="btn btn-default btn-red">Login</button>
                <p style="margin-top: 20px;"><a href="<?php echo site_url('user/lost-password'); ?>" >I lost my password</a></p>
			<?php echo form_close(); ?>
		</div>
        <div class="col-md-6 new-cus">
			<div id="title-bg">
				<div class="title">Don't Have An Account?</div>
			</div>
            <div>
				<a href="<?php echo site_url('register'); ?>" class="btn btn-default btn-red">Register Now</a>
                 <p><br />
					By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have made.
				</p>
			</div>
		</div>
    </div>
    <div class="spacer"></div>
</div>