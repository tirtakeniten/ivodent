<?php
    $CI =&get_instance();
    $enable_show_address = FALSE;
    $enable_delete = TRUE;
    $show_default = TRUE;
?>
<section>    
    <div class="container">
        <!-- Promotional content -->
        <div id="title-bg">
			<div class="title">Order Detail</div>
		</div>
        <div class="row">
            <div class="col-md-9">
                <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="links-box table-responsive">
                            <h4 class="pull-left">Order Code: <?php echo $order->code; ?></h4>
                            <h4 class="pull-right">Status: <?php echo $order->status->name; ?></h4>
                            <div class="clearfix"></div>
                            <hr />
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="2">ITEM DETAILS</th>
                                        <th>SUBTOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($order_details as $details) : ?>
                                    <?php $product = $details->product; ?>
                                    <tr>
                                        <td>
                                            <img src="<?php echo $product->images[0]->thumbnail; ?>" style="max-width: 100px;" />
                                        </td>
                                        <td>
                                            <p>
                                                <a href="<?php  ?>"><?php echo $product->name; ?></a>
                                            </p>
                                            <p><strong><?php echo format_number($details->qty) ?> &times; IDR <?php echo format_number($details->price) ?></strong></p>
                                        </td>
                                        <td>
                                            <p>
                                                <strong>IDR <?php echo format_number($details->subtotal); ?></strong>
                                            </p>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="2">
                                            <strong>Total:</strong>
                                        </th>
                                        <th>
                                            <strong>IDR <?php echo format_number($order->total); ?></strong>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">
                                            <strong>Shipping Cost:</strong>
                                        </th>
                                        <th>
                                            <strong>IDR <?php echo format_number($order->shipping_cost); ?></strong>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="2">
                                            <strong>Grandtotal:</strong>
                                        </th>
                                        <th>
                                            <strong>IDR <?php echo format_number($order->grandtotal); ?></strong>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                            <hr />
                            <h4>Shipping address:</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $order->address->name; ?><?php if ($order->address->company != '') { echo "({$order->address->company})"; } ?><br />
                                    <?php echo $order->address->address; ?><br />
                                    <?php echo $order->address->city; ?> - 
                                    <?php echo $order->address->province; ?> <?php echo $order->address->post_code; ?><br />
                                    <?php echo $order->address->email; ?> / <?php echo $order->address->phone; ?>
                                    <br /><br />
                                </div>
                                <div class="col-md-6">
                                    <div class="text-right">
                                        <a href="<?php echo site_url('checkout/home/send_email/'.$order->id); ?>" class="resend-order btn btn-lg btn-red"><i class="fa fa-envelope-o"></i> Resend Email</a>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <h4>Payment:</h4>
                            <?php if ($order->status->id == 1): ?>
                            <p id="payment">Please select payment method:</p>
                            <div class="row">
                                <div class="col-md-6 text-center">
                                    <a href="<?php echo site_url('pay/paypal'); ?>?code=<?php echo $order->code; ?>" class="btn btn-info btn-lg">Pay with PayPal</a>
                                </div>
                                <!--div class="col-md-6 text-center">
                                    <a href="" class="btn btn-primary btn-lg">Bank Transfer</a>
                                </div-->
                            </div>
                            <?php endif; ?>
                            <div class="spacer"></div>
                            <p class="text-center"><a href="<?php echo site_url('account/orders'); ?>">Back to Purchase History</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <?php echo $account_sidebar; ?>
            </div>
        </div>
        <!-- Promotional content -->
    </div>
    
    <div class="spacer"></div>
</section>