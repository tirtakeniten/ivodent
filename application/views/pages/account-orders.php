<?php
    $CI =&get_instance();
    $enable_show_address = FALSE;
    $enable_delete = TRUE;
    $show_default = TRUE;
?>
<section>    
    <div class="container">
        <!-- Promotional content -->
        <div id="title-bg">
			<div class="title">Your Purchase History</div>
		</div>
        <div class="row">
            <div class="col-md-9">
                <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="links-box">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Order Date</th>
                                        <th class="text-right">Total Items</th>
                                        <th class="text-right">Amount</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($orders as $order) : ?>
                                    <tr>
                                        <td><a href="<?php echo site_url('account/order/'.$order->id); ?>"><?php echo $order->code; ?></a></td>
                                        <td><?php echo user_date_format($order->date); ?></td>
                                        <td class="text-right"><?php echo format_number($order->total_item); ?></td>
                                        <td class="text-right">IDR <?php echo format_number($order->grandtotal); ?></td>
                                        <td><?php echo $order->status->name; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <?php echo $account_sidebar; ?>
            </div>
        </div>
        <!-- Promotional content -->
    </div>
    
    <div class="spacer"></div>
</section>