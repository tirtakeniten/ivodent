<section>    
    <div class="container">
        <!-- Promotional content -->
        <div id="title-bg">
			<div class="title">Account Update</div>
		</div>
        <?php echo form_open(current_url(), 'class="form-horizontal checkout"'); ?>
        <div class="row">
            <div class="col-sm-6 bill">
                <?php if(form_error('fname') ||
                           form_error('lname') ||
                           form_error('email') || 
                           form_error('password') ): ?>
                <div class="alert alert-danger">
                    <?php
                        echo form_error('fname');
                        echo form_error('lname');
                        echo form_error('email');
                        echo form_error('password');
                    ?>
                </div>
                <?php endif; ?>
                <div class="form-group dob">
					<div class="col-sm-6">
						<input type="text" name="fname" value="<?php echo set_value('fname', @$account->first_name); ?>" class="form-control" id="name" placeholder="First Name">
					</div>
					<div class="col-sm-6">
						<input type="text" name="lname" value="<?php echo set_value('lname', @$account->last_name); ?>" class="form-control" id="last_name" placeholder="Last Name">
					</div>
				</div>
				<div class="form-group dob">
					<div class="col-sm-12">
						<input type="text" name="email" value="<?php echo set_value('email', @$account->email); ?>" class="form-control" id="email" placeholder="Email">
					</div>
				</div>
                <button class="btn btn-default btn-red">Update Account</button>
            </div>
            <div class="col-md-3 col-md-push-3">
                <?php echo $account_sidebar; ?>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- Promotional content -->
    </div>
    
    <div class="spacer"></div>
</section>