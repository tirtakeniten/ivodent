<?php
    $CI =&get_instance();
    $enable_show_address = TRUE;
    $enable_delete = FALSE;
    $show_default = FALSE;
?>
<section>    
    <div class="container">
        <!-- Promotional content -->
        <div id="title-bg">
			<div class="title">Account Summary</div>
		</div>
        <div class="row">
            <div class="col-md-9">
                <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                <div class="links-box">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>Contact Information</h4>
                            <p>Name: <?php echo $account->username; ?></p>
                            <p>Email: <?php echo $account->email; ?></p>
                            <p>&nbsp;</p>
                            <p><a href="<?php echo site_url('account/update'); ?>" title="Update Your Account">Update Account</a></p>
                            <p><a href="">Change Password</a></p>
                            <p><a href="">Log out</a></p>
                        </div>
                        <div class="col-sm-6">
                            <h4>Your Primary Address</h4>
                            <?php if (is_null($primary_address)) : ?>
                            <div class="alert alert-warning">
                                You don't have any address yet.
                            </div>
                            <a href="<?php echo site_url('account/address-form'); ?>" class="btn btn-dark">Add Address Now</a>
                            <?php else: ?>
                            <p><?php echo $primary_address->name; ?> (<?php echo $primary_address->company; ?>)</p>
                            <p><?php echo $primary_address->address; ?></p>
                            <p><?php echo $primary_address->city; ?> - <?php echo $primary_address->post_code; ?>, <?php echo $primary_address->country; ?></p>
                            <p><?php echo $primary_address->email; ?></p>
                            <p><?php echo $primary_address->phone; ?></p>
                            <p><a href="<?php echo site_url('account/address-form/'.$primary_address->id); ?>">Edit this address</a> | <a href="<?php echo site_url('account/address'); ?>">Show My Address</a></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <?php echo $account_sidebar; ?>
            </div>
        </div>
        <!-- Promotional content -->
    </div>
    
    <div class="spacer"></div>
</section>