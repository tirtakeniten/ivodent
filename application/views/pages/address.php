<?php
    $CI =&get_instance();
    $enable_show_address = FALSE;
    $enable_delete = TRUE;
    $show_default = TRUE;
?>
<section>    
    <div class="container">
        <!-- Promotional content -->
        <div id="title-bg">
			<div class="title">Your Adresses</div>
		</div>
        <div class="row">
            <div class="col-md-9">
                <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                <div class="row">
                    <?php foreach($addresses as $primary_address): ?>
                    <div class="col-md-4">
                        <div class="links-box">
                        <?php require '_address-wrap.php'; ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <div class="col-md-4">
                        <div class="links-box">
                            <p style="font-size: 133px;" class="text-center">
                                <a href="<?php echo site_url('account/address-form'); ?>"><i class="fa fa-plus"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-md-3">
                <?php echo $account_sidebar; ?>
            </div>
        </div>
        <!-- Promotional content -->
    </div>
    
    <div class="spacer"></div>
</section>