<?php 
    $url = new Url();
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
        
        <?php echo form_open(); ?>
    	<div class="row">
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-body">
                        <?php if(validation_errors()): ?>
                        <div class="alert alert-danger">
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php endif; ?>
                        <!-- ID -->
                        <input type="hidden" value="<?php echo set_value('id', @$category->id); ?>" name="id"/>
                        <!-- /ID -->
                        <div class="form-group">
                            <label>Code</label>
                            <input value="<?php echo set_value('code', @$category->code); ?>" type="text" name="code" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <input value="<?php echo set_value('name', @$category->name); ?>" type="text" name="name" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>Slug</label>
                            <input value="<?php echo set_value('slug', @$category->slug); ?>" type="text" name="slug" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>Parent Category</label>
                            <?php
                                echo form_dropdown(
                                    'parent_id',
                                    $parent_categories,
                                    set_value('parent_id', @$category->parent_id),
                                    'class="form-control"'
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" class="form-control" rows="5"><?php echo set_value('description', @$category->description); ?></textarea>
                        </div>
                        <div class="form-group">
                            <?php 
                                $mediaId = "featured_image";
                                $mediaLabel = "Picture";
                                $mediaControlName = "featured_image";
                                $mediaValue = @set_value('featured_image', @$category->featured_image);
                            ?>
                            <div>
                                <?php include __DIR__.'/../media/_media-container.php'; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>SEO Title</label>
                            <input value="<?php echo set_value('seo_title', @$category->seo_title); ?>" type="text" name="seo_title" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>SEO Description</label>
                            <input value="<?php echo set_value('seo_description', @$category->seo_description); ?>" type="text" name="seo_description" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>SEO Keyword</label>
                            <input value="<?php echo set_value('seo_keyword', @$category->seo_keyword); ?>" type="text" name="seo_keyword" class="form-control" >
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->