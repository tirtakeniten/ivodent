<?php 
    $CI = &get_instance();
    $url = new Url();
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
        
            <?php if($CI->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('success'); ?>
            </div>
            <?php endif;?>
            
            <?php if($CI->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('error'); ?>
            </div>
            <?php endif;?>
            
            <div class="nav-tabs-custom"> 
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#table-view" data-toggle="tab" aria-expanded="true">Table View</a></li>
                    <li class=""><a href="#hierarchical-view" data-toggle="tab" aria-expanded="false">Hierarchical View</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="table-view">
                        <table class="table dataTables table-hover">
                            <thead>
                                <tr>
                                    <th>Category Name</th>
                                    <th>Code</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($categories as $category): ?>
                                <tr>
                                    <td><?php echo $category->name; ?></td>
                                    <td><?php echo $category->code; ?></td>
                                    <td><?php echo $category->seo_description; ?></td>
                                    <td>
                                        <a href="<?php echo $url->admin('category_form', $category->id); ?>" class="btn btn-info btn-sm">Edit</a>
                                        <a href="<?php echo $url->admin('category_delete', $category->id); ?>" class="btn btn-danger btn-sm data-delete">Delete</a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="hierarchical-view" style="min-height: 400px;">
                        <div class="row">
                            <div class="col-md-4">
                                <ul id="tree3">
                                <?php
                                /* Parent Category */
                                foreach( $category_tree as $category )
                                {
                                    echo '<li><a href="'.$url->admin('category_detail', $category->id).'">'.$category->name.'</a>';
                                    
                                    /* Children Category */
                                    if(count($category->children) > 0) echo '<ul>';
                                    foreach( $category->children as $children_category )
                                    {
                                        echo '<li><a href="'.$url->admin('category_detail', $children_category->id).'">'.$children_category->name.'</a>';
                                        
                                        /* Grandchildren Category */
                                        if(count($children_category->children) > 0) echo '<ul>';
                                        foreach( $children_category->children as $grandchildren_category )
                                        {
                                            echo '<li><a href="'.$url->admin('category_detail', $grandchildren_category->id).'">'.$grandchildren_category->name.'</a>';
                                            
                                            /* GrandGrandchildren Category */
                                            if(count($grandchildren_category->children) > 0) echo '<ul>';
                                            foreach( $grandchildren_category->children as $grandgrandchildren_category )
                                            {
                                                echo '<li><a href="'.$url->admin('category_detail', $grandgrandchildren_category->id).'">'.$grandgrandchildren_category->name.'</a>';
                                                echo '</li>';
                                            }
                                            if(count($grandchildren_category->children) > 0) echo '</ul>';
                                            /* GrandGrandchildren Category */
                                            
                                            echo '</li>';
                                        }
                                        if(count($children_category->children) > 0) echo '</ul>';
                                        /* Grandchildren Category */
                                        
                                        echo '</li>';
                                    }
                                    if(count($category->children) > 0) echo '</ul>';
                                    /* Children Category */
                                    
                                    echo '</li>';
                                }
                                /* Parent Category */
                                ?>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="box box-primary box-solid" id="categoryTarget">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Category Detail</h3>
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        
                                    </div><!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->