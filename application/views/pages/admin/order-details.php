<?php 
    $CI = &get_instance();
    $url = new Url();
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
        
            <?php if($CI->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('success'); ?>
            </div>
            <?php endif;?>
            
            <?php if($CI->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('error'); ?>
            </div>
            <?php endif;?>
            
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Order Code: <?php echo $order->code; ?></h3>
                </div>
                
                <div class="box-body">
                    <table class="table table-striped">
                        <tr>
                            <td>Date: </td>
                            <td><strong><?php echo user_date_format($order->date); ?></strong></td>
                        </tr>   
                        <tr>
                            <td>Status: </td>
                            <td><strong><?php echo $order->status->name; ?></strong></td>
                        </tr>
                        <tr>
                            <td>Total Purchase: </td>
                            <td><strong>IDR <?php echo format_number($order->grandtotal); ?></strong></td>
                        </tr>
                        <tr>
                            <td>Total Items: </td>
                            <td><strong>IDR <?php echo format_number($order->total_item); ?></strong></td>
                        </tr>
                    </table>
                    
                    <h3>Purchased items:</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($order_details as $details) : ?>
                            <?php $product = $details->product; ?>
                            <tr>
                                <td><a href="<?php  ?>"><?php echo $product->name; ?></a><br /></td>
                                <td>
                                    IDR <?php echo format_number($details->price) ?>
                                </td>
                                <td>
                                    <?php echo format_number($details->qty) ?>
                                </td>
                                <td>
                                    <strong>IDR <?php echo format_number($details->subtotal); ?></strong>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3" style="text-align: right;">
                                    <strong>Total:</strong>
                                </th>
                                <th>
                                    <strong>IDR <?php echo format_number($order->total); ?></strong>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="3" style="text-align: right;">
                                    <strong>Shipping Cost:</strong>
                                </th>
                                <th>
                                    <strong>IDR <?php echo format_number($order->shipping_cost); ?></strong>
                                </th>
                            </tr>
                            <tr>
                                <th colspan="3" style="text-align: right;">
                                    <strong>Grandtotal:</strong>
                                </th>
                                <th>
                                    <strong>IDR <?php echo format_number($order->grandtotal); ?></strong>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                    <h3>Shipping address:</h3>
                    <p>
                        <?php echo $order->address->name; ?><?php if ($order->address->company != '') { echo "(".$order->address->company.")"; } ?><br />
                        <?php echo $order->address->address; ?><br />
                        <?php echo $order->address->city; ?> - 
                        <?php echo $order->address->province; ?> <?php echo $order->address->post_code; ?><br />
                        <?php echo $order->address->email; ?> / <?php echo $order->address->phone; ?>
                    </p>
                    
                    <h3>Update Order Status:</h3>
                    <form class="form-horizontal" role="form" method="post" action="<?php echo $url->admin('order', 'update-status') ?>">
                        <div class="form-group">
                            <label class="col-md-1 control-label">Status</label>
                            <div class="col-md-2">
                                <input type="hidden" name="order_id" value="<?php echo $order->id; ?>" />
                                <?php echo form_dropdown('status_id', $order_status_dropdown, $order->status_id, 'class="form-control"'); ?>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-flat">Update Status</button>
                        <a href="<?php echo $url->admin('print_order', $order->id); ?>" class="btn btn-primary btn-flat pull-right" target="_blank"><i class="fa fa-print"></i> Print</a>
                    </form>
                </div>
            </div>
            
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->