<?php 
    $CI = &get_instance();
    $url = new Url();
?>
<section class="invoice">
<!-- title row -->
<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">
      <i class="fa fa-globe"></i> Ivodent Dental Laboratory
      <small class="pull-right">Doc. Print: <?php echo user_date_format('now'); ?></small>
    </h2>
  </div><!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">
  <div class="col-sm-4 invoice-col">
    From
    <address>
      <strong>Ivodent Dental Laboratory</strong><br>
      Jln Patimura No. 9 Denpasar<br />
      Bali - Indonesia<br>
      Phone: (62 361) 237 777<br>
      Fax: (62 361) 226 834<br>
      Email: iguizot@sindosat.net.id
    </address>
  </div><!-- /.col -->
  <div class="col-sm-4 invoice-col">
    To
    <address>
      <strong><?php echo $order->address->name; ?></strong><br>
      <?php if ($order->address->company != '') { echo "(".$order->address->company.")"; } ?><br />
      <?php echo $order->address->address; ?><br />
      <?php echo $order->address->city; ?> - 
      <?php echo $order->address->province; ?> <?php echo $order->address->post_code; ?><br />
      Phone: <?php echo $order->address->phone; ?><br>
      Email: <?php echo $order->address->email; ?>
    </address>
  </div><!-- /.col -->
  <div class="col-sm-4 invoice-col">
    <b>Order Code: <?php echo $order->code; ?></b><br>
    <br>
    <b>Order Date:</b> <?php echo user_date_format($order->date); ?><br>
    <b>Order Status:</b> <?php echo $order->status->name; ?><br>
    <b>Total Purchase:</b> IDR <?php echo format_number($order->grandtotal); ?><br>
    <b>Total Items:</b> <?php echo format_number($order->total_item); ?>
  </div><!-- /.col -->
</div><!-- /.row -->

<!-- Table row -->
<div class="row">
  <div class="col-xs-12 table-responsive">
    <table class="table table-striped">
      <thead>
            <tr>
                <th>Qty</th>
                <th>Product</th>
                <th>Code</th>
                <th>Price</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($order_details as $details) : ?>
            <?php $product = $details->product; ?>
            <tr>
                <td>
                    <?php echo format_number($details->qty) ?>
                </td>
                <td><?php echo $product->name; ?></td>
                <td><?php echo $product->sku; ?></td>
                <td>
                    IDR <?php echo format_number($details->price) ?>
                </td>
                <td>
                    IDR <?php echo format_number($details->subtotal); ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4" style="text-align: right;">
                    <strong>Total:</strong>
                </th>
                <th>
                    <strong>IDR <?php echo format_number($order->total); ?></strong>
                </th>
            </tr>
            <tr>
                <th colspan="4" style="text-align: right;">
                    <strong>Shipping Cost:</strong>
                </th>
                <th>
                    <strong>IDR <?php echo format_number($order->shipping_cost); ?></strong>
                </th>
            </tr>
            <tr>
                <th colspan="4" style="text-align: right;">
                    <strong>Grandtotal:</strong>
                </th>
                <th>
                    <strong>IDR <?php echo format_number($order->grandtotal); ?></strong>
                </th>
            </tr>
        </tfoot>
    </table>
  </div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
  <div class="col-md-12 text-center">
    <strong>We thank you for shopping with us.</strong>
  </div>
</div><!-- /.row -->
</section>
