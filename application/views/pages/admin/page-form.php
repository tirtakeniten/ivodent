<?php 
    $url = new Url();
    $CI = &get_instance();
    $auth_user = $CI->auth->user();
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                <?php if(validation_errors()): ?>
                <div class="alert alert-danger">
                    <?php echo validation_errors(); ?>
                </div>
                <?php endif; ?>
                </div>
            </div>
            <?php echo form_open(); ?>
        	<div class="row">
                <div class="col-md-8">
                    <!-- page detail box -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Page Form</h3>
                        </div>
                        <div class="box-body">
                            <!-- ID -->
                            <input type="hidden" value="<?php echo set_value('id', @$page->id); ?>" name="id"/>
                            <!-- /ID -->
                            <!-- USER ID -->
                            <input type="hidden" value="<?php echo $auth_user->id; ?>" name="user_id"/>
                            <!-- USER ID -->
                            
                            <div class="form-group">
                                <label>Title</label>
                                <input value="<?php echo set_value('title', @$page->title); ?>" type="text" name="title" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label>Slug</label>
                                <input value="<?php echo set_value('slug', @$page->slug); ?>" type="text" name="slug" class="form-control" >
                            </div> 
                            <div class="form-group">
                                <label>Content</label>
                                <p>
                                    <a href="<?php echo site_url('media/manager'); ?>?field_id=_tinymce" class="media btn btn-default" data-fancybox-type="iframe">
                                    <i class="fa fa-picture-o"></i>
                                    Add Picture</a>
                                </p>
                                <textarea name="content" class="form-control tinymce" rows="15"><?php echo set_value('content', @$page->content); ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Excerpt</label>
                                <textarea name="excerpt" class="form-control" rows="5"><?php echo set_value('excerpt', @$page->excerpt); ?></textarea>
                            </div> 
                            <div class="form-group">
                                <label>Parent Page</label>
                                <input value="<?php echo set_value('parent_id', @$page->parent_id); ?>" type="text" name="parent_id" class="form-control" >
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-flat">Submit</button>
                            </div>
                        </div>
                    </div>
                    <!-- page detail box -->
                    
                    <!-- SEO box -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">S E O</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>SEO Title</label>
                                <input value="<?php echo set_value('seo_title', @$page->seo_title); ?>" type="text" name="seo_title" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label>SEO Description</label>
                                <input value="<?php echo set_value('seo_description', @$page->seo_description); ?>" type="text" name="seo_description" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label>SEO Keyword</label>
                                <input value="<?php echo set_value('seo_keyword', @$page->seo_keyword); ?>" type="text" name="seo_keyword" class="form-control" >
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-flat">Submit</button>
                            </div>
                        </div>
                    </div>
                    <!-- SEO box -->
                </div>
                <div class="col-md-4">
                    <!-- Gallery box -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Feature Image</h3>
                        </div>
                        <div class="box-body">
                            <?php 
                                $mediaId = "featured_image";
                                $mediaLabel = "Feature Image";
                                $mediaControlName = "featured_image";
                                $mediaValue = @set_value('featured_image', @$page->featured_image);
                            ?>
                            <div>
                                <?php include __DIR__.'/../media/_media-container.php'; ?>
                            </div>
                        </div>
                    </div>
                    <!-- Gallery box -->
                    
                    
                    <!-- Gallery box -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Gallery</h3>
                        </div>
                        <div class="box-body">
                            
                        </div>
                    </div>
                    <!-- Gallery box -->
                </div>
            </div>
            
            
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->