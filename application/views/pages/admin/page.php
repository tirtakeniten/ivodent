<?php 
    $CI = &get_instance();
    $url = new Url();
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
        
            <?php if($CI->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('success'); ?>
            </div>
            <?php endif;?>
            
            <?php if($CI->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('error'); ?>
            </div>
            <?php endif;?>
            
            <div class="box box-primary">
                <div class="box-body">
                    <table class="table dataTables table-hover">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>Created at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($pages as $page): ?>
                            <tr>
                                <td><?php echo $page->title; ?></td>
                                <td><?php echo $page->slug; ?></td>
                                <td><?php echo $page->created_at; ?></td>
                                <td>
                                    <a href="<?php echo $url->admin('page_form', $page->id); ?>" class="btn btn-info btn-sm">Edit</a>
                                    <a href="<?php echo $url->admin('page_delete', $page->id); ?>" class="btn btn-danger btn-sm data-delete">Delete</a>
                                    <a target="_blank" href="<?php echo site_url('page/'.$page->slug); ?>" class="btn btn-success btn-sm">View</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->