<?php 
    $CI = &get_instance();
    $url = new Url();
    
    function category_tree_form($categories, $product = NULL)
    {
        $compiled = '';
        
        foreach($categories as $category)
        {
            $compiled .= '<li><label>';
            $compiled .= form_checkbox(
                            'category_id[]',
                            $category->id,
                            in_array($category->id,  (
                                (isset($product)) ? $product->categories  : set_value('category_id', [])
                            )));
            $compiled .= $category->name.'</label>';
            
            if(count($category->children) > 0)
            {
                $compiled .= '<ul>';
                $compiled .= category_tree_form($category->children, $product);
                $compiled .= '</ul>';
            }
            
            $compiled .= '</li>';
        }
        
        return $compiled;
    }
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
        
        <?php echo form_open(); ?>
        <div class="row">
            <div class="col-md-12">
            <?php if(validation_errors()): ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
            <?php endif; ?>
            </div>
        </div>
    	<div class="row">
            <div class="col-md-8">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Product Details</h3>
                    </div>
                    <div class="box-body">
                        <!-- ID -->
                        <input type="hidden" value="<?php echo set_value('id', @$product->id); ?>" name="id"/>
                        <!-- /ID -->
                        
                        <div class="form-group">
                            <label>Product Name</label>
                            <input value="<?php echo set_value('name', @$product->name); ?>" type="text" name="name" class="form-control" >
                        </div>
                        
                        <div class="form-group">
                            <label>Description</label>
                            <p>
                                <a href="<?php echo site_url('media/manager'); ?>?field_id=_tinymce" class="media btn btn-default" data-fancybox-type="iframe">
                                <i class="fa fa-picture-o"></i>
                                Add Picture</a>
                            </p>
                            <textarea name="description" class="form-control tinymce" rows="10"><?php echo set_value('description', @$product->description); ?></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label>Short Description</label>
                            <p>
                                <a href="<?php echo site_url('media/manager'); ?>?field_id=_tinymce" class="media btn btn-default" data-fancybox-type="iframe">
                                <i class="fa fa-picture-o"></i>
                                Add Picture</a>
                            </p>
                            <textarea name="excerpt" class="form-control tinymce" rows="5"><?php echo set_value('excerpt', @$product->excerpt); ?></textarea>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat">Submit</button>
                        </div>
                    </div>
                </div>
                
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Images</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <?php for($i=1; $i<=4; $i++): ?>
                            <?php 
                                $mediaId = "img".$i;
                                $mediaLabel = "Image {$i}";
                                $mediaControlName = "image[]";
                                $mediaValue = @set_value('image', @$product->images)[$i-1];
                            ?>
                            <div class="col-md-3">
                                <?php include __DIR__.'/../media/_media-container.php'; ?>
                            </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">S E O</h3>
                    </div>
                    <div class="box-body">
                    
                        <div class="form-group">
                            <label>Slug</label>
                            <input value="<?php echo set_value('slug', @$product->slug); ?>" type="text" name="slug" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>SEO Title</label>
                            <input value="<?php echo set_value('seo_title', @$product->seo_title); ?>" type="text" name="seo_title" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>SEO Description</label>
                            <input value="<?php echo set_value('seo_description', @$product->seo_description); ?>" type="text" name="seo_description" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>SEO Keyword</label>
                            <input value="<?php echo set_value('seo_keyword', @$product->seo_keyword); ?>" type="text" name="seo_keyword" class="form-control" >
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat">Submit</button>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                        
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Price</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label>Price</label>
                            <input value="<?php echo set_value('price', @$product->price); ?>" type="text" name="price" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>Sale Price</label>
                            <input value="<?php echo set_value('sale_price', @$product->sale_price); ?>" type="text" name="sale_price" class="form-control" >
                        </div>
                    </div>
                </div>
                
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Inventory</h3>
                    </div>
                    <div class="box-body">
                    
                        <div class="form-group">
                            <label>SKU (Stock Keeping Unit)</label>
                            <input value="<?php echo set_value('code', @$product->sku); ?>" type="text" name="sku" class="form-control control-prevent-submit" >
                        </div>
                        <div class="form-group">
                            <label>Barcode</label>
                            <input value="<?php echo set_value('barcode', @$product->barcode); ?>" type="text" name="barcode" class="form-control control-prevent-submit" >
                        </div>
                        <div class="form-group">
                            <label>Track Inventory</label>
                            <?php 
                                echo form_dropdown(
                                    'inventory_tracking', 
                                    $inventory_options,
                                    set_value('inventory_tracking', @$product->inventory_tracking),
                                    'class="form-control"'
                                );
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Quantity</label>
                            <input value="<?php echo set_value('quantity', @$product->quantity); ?>" type="text" name="quantity" class="form-control" >
                        </div>
                        
                    </div>
                </div>
                
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Category</h3>
                    </div>
                    <div class="box-body">                        
                        <ul id="tree3">
                            <?php echo category_tree_form($category_tree, @$product); ?>
                        </ul>
                        
                    </div>
                </div>
                
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Misc</h3>
                    </div>
                    <div class="box-body">
                        
                        <div class="form-group">
                            <label>Sequence</label>
                            <input value="<?php echo set_value('sequence', @$product->sequence); ?>" type="text" name="sequence" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label>Note / Remark</label>
                            <textarea name="note" class="form-control"><?php echo set_value('note', @$product->note); ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->