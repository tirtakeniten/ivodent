<?php 
    $CI = &get_instance();
    $url = new Url();
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
        
            <?php if($CI->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('success'); ?>
            </div>
            <?php endif;?>
            
            <?php if($CI->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('error'); ?>
            </div>
            <?php endif;?>
            
            <div class="box box-primary">
                <div class="box-body">
                    <table class="table dataTables">
                        <thead>
                            <tr>
                                <th>SKU</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Sale Price</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($products as $product): ?>
                            <tr>
                                <td><?php echo $product->sku; ?></td>
                                <td><?php echo $product->name; ?></td>
                                <td><?php echo $product->quantity; ?></td>
                                <td><?php echo $product->price; ?></td>
                                <td><?php echo $product->sale_price; ?></td>
                                <td>
                                    <a href="<?php echo $url->admin('product_varian', $product->id); ?>" class="btn btn-default btn-sm">Varian</a>
                                    <a href="<?php echo $url->admin('product_form', $product->id); ?>" class="btn btn-info btn-sm">Edit</a>
                                    <a href="<?php echo $url->admin('product_delete', $product->id); ?>" class="btn btn-danger btn-sm data-delete">Delete</a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->