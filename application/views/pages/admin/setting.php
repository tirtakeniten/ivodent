<?php 
    $CI =& get_instance();
    $url = new Url();
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
        
        <?php echo form_open(); ?>
    	<div class="row">
            <div class="col-md-6">
                <div class="box box-success color-palette">
                    <div class="box-body ">
                        <?php if(validation_errors()): ?>
                        <div class="alert alert-danger">
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php endif; ?>
                        
                        <?php if($CI->session->flashdata('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $CI->session->flashdata('success'); ?>
                        </div>
                        <?php endif;?>
                        
                        <?php if($CI->session->flashdata('error')): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo $CI->session->flashdata('error'); ?>
                        </div>
                        <?php endif;?>
                        
                        <?php foreach($fields as $field_name => $field): ?>
                        <div class="form-group">
                            <label><?php echo $field['label']; ?></label>
                            <?php if($field['type'] == 'text'): ?>
                            
                            <?php /* Text input */ ?>
                            <input type="text" value="<?php echo $field['value'] ?>" name="<?php echo $field_name ?>" class="form-control" />
                            
                            <?php elseif($field['type'] == 'select'): ?>
                            
                            <?php /* Dropdown */ ?>
                            <?php echo form_dropdown($field_name, $field['options'], $field['value'], 'class="form-control"'); ?>
                            
                            <?php elseif($field['type'] == 'textarea'): ?>
                            
                            <?php /* Textarea */ ?>
                            <textarea name="<?php echo $field_name ?>" class="form-control" rows="<?php echo @$field['rows'] ?>"><?php echo $field['value'] ?></textarea>
                            
                            <?php elseif($field['type'] == 'image'): ?>
                            <?php 
                                $mediaId = $field_name;
                                $mediaLabel = '';
                                $mediaControlName = $field_name;
                                $mediaValue = set_value($field_name, $field['value']);
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                <?php include __DIR__.'/../media/_media-container.php'; ?>
                                </div>
                            </div>
                            
                            <?php endif; ?>
                        </div>
                        <?php endforeach; ?>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->