<?php 
    $CI = &get_instance();
    $url = new Url();
?>    
<?php echo form_open(current_url()); ?>

    <div class="box">
        <div class="box-header with-border">
            <?php if(!isset($var)): ?>
            <h3 class="box-title">Add New Varian</h3>
            <?php else: ?>
            <h3 class="box-title">Update Product Varian </h3>
            <?php endif; ?>
        </div>
        <div class="box-body">
            <?php if(validation_errors()): ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
            <?php endif; ?>
            
            <!-- ID -->
            <input type="hidden" value="<?php echo set_value('id', @$var->id); ?>" name="id"/>
            <input type="hidden" value="<?php echo $product->id; ?>" name="product_id"/>
            <!-- /ID -->
            <h4>Options</h4>
            <div class="row">
                <div class="col-md-4">  
                    <label>Options</label>
                </div>
                <div class="col-md-8">
                    <label>Value</label>
                </div>
            </div>
            <!-- The options -->
            <?php 
                if( !isset($var) > 0)
                {
                    $var = new StdClass();
                    $var->varian1_key = @$varians[0]->varian1_key;
                    $var->varian2_key = @$varians[0]->varian2_key;
                    $var->varian3_key = @$varians[0]->varian3_key;
                    $var->price = $product->price;
                    $var->sale_price = $product->sale_price;
                }
            ?>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <input value="<?php echo set_value('varian1_key', @$var->varian1_key); ?>" type="text" name="varian1_key" class="form-control" >
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input value="<?php echo set_value('varian1_value', @$var->varian1_value); ?>" type="text" name="varian1_value" class="form-control" >
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <input value="<?php echo set_value('varian2_key', @$var->varian2_key); ?>" type="text" name="varian2_key" class="form-control" >
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input value="<?php echo set_value('varian2_value', @$var->varian2_value); ?>" type="text" name="varian2_value" class="form-control" >
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <input value="<?php echo set_value('varian3_key', @$var->varian3_key); ?>" type="text" name="varian3_key" class="form-control" >
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <input value="<?php echo set_value('varian3_value', @$var->varian3_value); ?>" type="text" name="varian3_value" class="form-control" >
                    </div>
                </div>
            </div>
            <!-- The options -->
            
            <hr />
            <h4>Pricing</h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Price</label>
                        <input value="<?php echo set_value('price', @$var->price); ?>" type="text" name="price" class="form-control" >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Sale Price</label>
                        <input value="<?php echo set_value('sale_price', @$var->sale_price); ?>" type="text" name="sale_price" class="form-control" >
                    </div>
                </div>
            </div>
            
            <hr />
            <h4>Inventory</h4>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>SKU</label>
                        <input value="<?php echo set_value('sku', @$var->sku); ?>" type="text" name="sku" class="form-control control-prevent-submit" >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Barcode</label>
                        <input value="<?php echo set_value('barcode', @$var->barcode); ?>" type="text" name="barcode" class="form-control control-prevent-submit" >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Quantity</label>
                        <input value="<?php echo set_value('quantity', @$var->quantity); ?>" type="text" name="quantity" class="form-control" >
                    </div>
                </div>
            </div>
            
            <hr />
            <h4>Picture</h4>
            <div class="form-group">
                <label>Picture</label>
                <input value="<?php echo set_value('picture', @$var->picture); ?>" type="text" name="picture" class="form-control" >
            </div>
            
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-flat">Submit</button>
            </div>
        </div>
    </div>
    
<?php echo form_close(); ?>