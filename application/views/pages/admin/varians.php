<?php 
    $CI = &get_instance();
    $url = new Url();
?>    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $page_title; ?>
                <small><?php echo $page_subtitle; ?></small>
            </h1>
        </section>
        <!-- Content Header (Page header) -->
    
        <!-- Main content -->
        <section class="content">
        
            <?php if($CI->session->flashdata('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('success'); ?>
            </div>
            <?php endif;?>
            
            <?php if($CI->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $CI->session->flashdata('error'); ?>
            </div>
            <?php endif;?>
            
            <div class="row box-change-class">
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-body">
                            <h4><?php echo $product->name; ?></h4>
                            <p><a href="<?php echo $url->admin('product_form', $product->id); ?>" class="">Back to product</a></p>
                        </div>
                    </div><!-- .box -->
                    
                    <div class="box">
                        <div class="box-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <?php if(isset($varians[0])): ?>
                                            <?php echo $varians[0]->options; ?>
                                            <?php else: ?>
                                            Varians
                                            <?php endif; ?>
                                        </th>
                                        <th>Price</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($varians as $varian): ?>
                                    <tr>
                                        <td>
                                            <a href="<?php echo $url->admin('product_varian', $product->id, $varian->id); ?>" title="Clik to edit">
                                                <?php echo $varian->values ?>
                                            </a>
                                        </td>
                                        <td>
                                            <?php echo $varian->price; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo $url->admin('product_varian', $product->id, $varian->id); ?>" class="" title="Edit"><i class="fa fa-pencil"></i></a><span>&nbsp;&nbsp;&nbsp;</span>
                                            <a href="<?php echo $url->admin('product_varian_delete', $product->id, $varian->id); ?>" class="data-delete" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <p><a href=""></a></p>
                        </div>
                    </div><!-- .box -->
                </div>
                
                <div class="col-md-8">
                    <?php echo @$form; ?>
                </div>
                
            </div><!-- .row -->
            
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->