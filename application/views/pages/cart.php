<?php
    $CI = &get_instance();
?>
<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page-title-wrap">
					<div class="page-title-inner">
					<div class="row">
						<div class="col-md-12">
							<div class="bread"><a href="<?php echo site_url(); ?>">Home</a> &rsaquo; Shopping Cart</div>
							<div class="bigtitle">Shopping Cart</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div id="title-bg">
			<div class="title">Shopping Cart</div>
		</div>
		<?php if ($CI->session->flashdata('error')) : ?>
        <div class="alert alert-danger">
            <?php echo $CI->session->flashdata('error'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <?php endif; ?>
        <?php if ($CI->session->flashdata('success')) : ?>
        <div class="alert alert-success">
            <?php echo $CI->session->flashdata('success'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <?php endif; ?>
        <?php if (count($cart_contents) > 0) : ?>
        <?php echo form_open('cart/update'); ?>
		<div class="table-responsive">
			<table class="table table-bordered chart">
				<thead>
					<tr>
						<th>Image</th>
						<th>Name</th>
						<th>Quantity</th>
						<th>Unit Price</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
                    <?php foreach ($cart_contents as $row_id => $cart_item) : ?>
                    <?php 
                        $cart_product = $cart_item['product'];
                    ?>
					<tr>
						<td>
                            <a href="<?php echo site_url('product/'.$cart_product->slug); ?>"><img src="<?php echo $cart_product->images[0]['thumbnail']; ?>" width="100" /></a>
                        </td>
						<td>
                            <a href="<?php echo site_url('product/'.$cart_product->slug); ?>"><?php echo $cart_product->name; ?></a></td>
						<td class="text-center"><center><input type="text" value="<?php echo $cart_item['qty'] ?>" class="form-control quantity" name="qty[<?php echo $row_id; ?>]"></center><br />
                        <a href="<?php echo site_url('cart/update'); ?>?rowid=<?php echo $row_id; ?>" title="Remove from cart" class="btn btn-danger btn-sm" style="color: #fff;" onclick="return confirm('Are you sure want to remove this item from your cart?');"><i class="fa fa-trash-o" style="color: #fff;"></i> Remove</a></td>
						<td>
                            IDR <?php echo format_number($cart_item['price']); ?>
                            <span style="text-decoration: line-through;"><?php
                                if ($cart_product->sale_price != 0) {
                                    echo "<br />IDR ".format_number(usd_to_idr($cart_product->price));
                                }
                            ?></span>
                        </td>
						<td>IDR <?php echo format_number($cart_item['subtotal']); ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-3 col-md-offset-9">
			<div class="subtotal-wrap">
				<div class="total">Total : <span class="bigprice">IDR <?php echo format_number($cart_factory->cart()->total()); ?></span></div>
				<a href="<?php echo site_url('checkout'); ?>" class="btn btn-default btn-red btn-sm">Checkout</a>
				<button type="submit" class="btn btn-default btn-red btn-sm">Update Cart</button>
				<div class="clearfix"></div>
				<a href="<?php echo site_url(); ?>" class="btn btn-default btn-yellow">Continue Shopping</a>
			</div>
			<div class="clearfix"></div>
			</div>
		</div>
        <?php echo form_close(); ?>
        <?php else : ?>
        <div class="alert alert-warning">Your shopping cart is empty.</div>
        <a href="<?php echo site_url(); ?>" class="btn btn-default btn-yellow">Continue Shopping</a>
        <?php endif; ?>
		<div class="spacer"></div>
	</div>