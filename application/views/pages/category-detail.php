<?php
    $parent_category = '';
    if(isset($category->parent->id))
    {
        $parent_category = $category->parent->name;
        $category->name = $parent_category.' - '.$category->name;        
    }
?>
<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page-title-wrap">
					<div class="page-title-inner">
					<div class="row">
						<div class="col-md-12">
							<div class="bread"><a href="#">Home</a> &rsaquo; Category</div>
							<div class="bigtitle"><?php echo $category->name ?></div>
                            <p><?php echo $category->description; ?></p>
                            <div class="row">
                                <?php foreach($current_category as $cc_childrent): ?>
                                <div class="col-md-2 text-center">
                                    <a href="<?php echo site_url('category/'.$cc_childrent->slug); ?>" title="">
                                        <img src="<?php echo $cc_childrent->featured_image_url['thumbnail']; ?>" class="img-responsive" />
                                    </a>
                                    <span><?php echo $cc_childrent->name ?></span>
                                </div>
                                <?php endforeach; ?>
                            </div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-md-3"><!--sidebar-->
				<div id="title-bg">
					<div class="title">Categories</div>
				</div>
				<div class="categorybox">
					<ul class="tree1">
						<?php echo category_tree($category_tree); ?>
					</ul>
                    <div class="clearfix"></div>
				</div>
                
                <div id="title-bg">
					<div class="title">Last Viewed Products</div>
				</div>
                <div class="categorybox">
					<ul class="tree1">
						
					</ul>
                    <div class="clearfix"></div>
				</div>
                
			</div><!--sidebar-->
			<div class="col-md-9"><!--Main content-->
				<div id="title-bg">
					<div class="title">Products of <?php echo $category->name ?></div>
				</div>
				<div class="row prdct"><!--Products-->
                    <?php if(isset($products)): ?>
					<?php foreach($products as $product_wrap): ?>
					<div class="col-md-4">
        				<?php include '_product-wrap.php'; ?>
        			</div>
					<?php endforeach; ?>
                    <?php else: ?>
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            No product found for category "<?php echo $category->name ?>".
                        </div>
                    </div>
                    <?php endif; ?>
				</div><!--Products-->
				<ul class="pagination shop-pag"><!--pagination-->
					<li><a href="#"><i class="fa fa-caret-left"></i></a></li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#"><i class="fa fa-caret-right"></i></a></li>
				</ul><!--pagination-->

			</div>
		</div>
		<div class="spacer"></div>
	</div>
