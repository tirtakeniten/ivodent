<?php 
    $CI = &get_instance();
    $enable_show_address = FALSE;
    $enable_delete = TRUE;
    $show_default = TRUE;
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<div id="title-bg">
				<div class="title">Select Your Address</div>
			</div>
		</div>
        <?php if ($this->session->flashdata('success')): ?>
        <div class="col-md-12">
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
            </div>
        </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('error')): ?>
        <div class="col-md-12">
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php echo form_open(); ?>
        <div class="row">
            <?php foreach ($addresses as $key => $address) : ?>
            <div class="col-md-4">
                <div class="links-box address-box">
                    <p><?php echo $address->name; ?> (<?php echo $address->company; ?>)</p>
                    <p><?php echo $address->address; ?></p>
                    <p><?php echo $address->city; ?> - <?php echo $address->post_code; ?>, <?php echo $address->country; ?></p>
                    <p><?php echo $address->email; ?></p>
                    <p><?php echo $address->phone; ?></p>
                    <p><a href="<?php echo site_url('account/address-form/'.$address->id); ?>?redirect=checkout/address">Edit address</a></p>
                    <p><a href="<?php echo site_url('checkout/address/select/'.$address->id); ?>" class="btn btn-red">Select address</a></p>
                </div>
            </div>
            <?php endforeach; ?>
            <div class="col-md-12">
                
            </div>
        </div>
    <?php echo form_close(); ?>
    <div class="spacer"></div>
</div>