<?php 
    $CI = &get_instance();
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
			<div id="title-bg">
				<div class="title">Your Order Summary</div>
			</div>
            <div class="alert alert-info">
                <p>Please review your order summary.</p>
            </div>
		</div>
        <?php if ($this->session->flashdata('success')): ?>
        <div class="col-md-12">
            <div class="alert alert-success">
                <?php echo $this->session->flashdata('success'); ?>
            </div>
        </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('error')): ?>
        <div class="col-md-12">
            <div class="alert alert-danger">
                <?php echo $this->session->flashdata('error'); ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    
    <!-- SHOPPING CART -->
    <div class="table-responsive">
		<table class="table table-bordered chart">
			<thead>
				<tr>
					<th>Image</th>
					<th>Name</th>
                    <th>Unit Price</th>
					<th>Quantity</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
                <?php foreach ($cart_contents as $row_id => $cart_item) : ?>
                <?php 
                    $cart_product = $cart_item['product'];
                ?>
				<tr>
					<td>
                        <a href="<?php echo site_url('product/'.$cart_product->slug); ?>"><img src="<?php echo $cart_product->images[0]['thumbnail']; ?>" width="100" /></a>
                        </td>
					<td>
                        <a href="<?php echo site_url('product/'.$cart_product->slug); ?>"><?php echo $cart_product->name; ?></a>
                    </td>
					<td>
                        IDR <?php echo format_number($cart_item['price']); ?>
                        <span style="text-decoration: line-through;"><?php
                            if ($cart_product->sale_price != 0) {
                                echo "<br />IDR ".format_number(usd_to_idr($cart_product->price));
                            }
                        ?></span>
                    </td>
                    <td class="text-center"><?php echo $cart_item['qty'] ?></td>    
					<td>IDR <?php echo format_number($cart_item['subtotal']); ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
            <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2">Total</td>
                    <td>IDR <?php echo format_number($cart_factory->cart()->total()); ?></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2" class="text-right">Total Items</td>
                    <td><?php echo $CI->cart_factory->cart()->total_items(); ?></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2" class="text-right">Shipping</td>
                    <td>IDR <?php echo format_number($shipping->cost->cost[0]->value); ?></td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2" class="text-right">Grandtotal</td>
                    <td>IDR <?php echo format_number($cart_factory->grandtotal()); ?></td>
                </tr>
            </tfoot>
		</table>
	</div>
    <div class="row">
		<div class="col-md-4 col-md-offset-8 text-right">
            <a href="<?php echo site_url('cart'); ?>" class="btn btn-default btn-red">Edit Cart</a>
			<a href="<?php echo site_url(); ?>" class="btn btn-default btn-yellow">Continue Shopping</a>
		<div class="clearfix"></div>
		</div>
	</div>
    <!-- SHOPPING CART -->
    
    <!-- ADDRESS AND SHIPPING -->
    <div class="row">
    
        <!-- ADDRESS -->
        <div class="col-md-6">
            <div id="title-bg">
				<div class="title">Shipping Address</div>
			</div>
            <div>
                <p><?php echo $address->name; ?> (<?php echo $address->company; ?>)</p>
                <p><?php echo $address->address; ?></p>
                <p><?php echo $address->city; ?> - <?php echo $address->post_code; ?>, <?php echo $address->country; ?></p>
                <p><?php echo $address->email; ?></p>
                <p><?php echo $address->phone; ?></p>
                <p><a href="<?php echo site_url('checkout/address'); ?>" class="btn btn-default btn-red">Edit Address</a></p>
            </div>
        </div>
        <!-- ADDRESS -->
        <!-- SHIPPING -->
        <div class="col-md-6">
            <div id="title-bg">
				<div class="title">Shipping Courier &amp; Service</div>
			</div>
            <div>
                <p>Courier: <?php echo $shipping_raw[0]->name; ?></p>
                <p>Destination: <?php echo "{$shipping_destination->type} {$shipping_destination->city_name} - {$shipping_destination->province} {$shipping_destination->postal_code}"; ?></p>
                <p>Service type: <?php echo "{$shipping->cost->description} ({$shipping->cost->service})"; ?></p>
                <p>Cost: IDR <?php echo format_number($shipping->cost->cost[0]->value); ?></p>
                <p><a href="<?php echo site_url('checkout/shipping'); ?>" class="btn btn-default btn-red">Edit Shipping Courier &amp; service</a></p>
            </div>
        </div>
        <!-- SHIPPING -->
        
    </div>
    <!-- ADDRESS AND SHIPPING -->
    
    <div id="title-bg"></div>
    <noscript>
        <div class="alert alert-danger">
            Please enable javascript to continue.
        </div>
    </noscript>
    <?php echo form_open('', 'id="form-confirm-order"'); ?>
        <div class="text-center">
            <div class="form-group">
                <label>
                    <input type="checkbox" name="agree_term" value="1" />
                    I agree the terms and conditions.
                </label>
            </div>
            <button class="btn btn-info btn-lg"><i class="fa fa-shopping-cart"></i> Confirm Order</button>
        </div>
    <?php echo form_close(); ?>
    <div class="spacer"></div>
</div>