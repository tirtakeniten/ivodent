<div class="container">
    <div class="row">
        <div class="col-md-12">
			<div id="title-bg">
				<div class="title">Select Shipping Destination</div>
			</div>
		</div>
        <div class="col-md-6">
            <div class="">
                <?php echo form_open('', 'id="form-shipping"'); ?>
                <input type="hidden" value="<?php echo site_url() ?>" id="site-url" />
                <div class="form-group">
                    <label>Select Province</label>
                    <?php echo form_dropdown(
                        'province_id', 
                        $province_dropdown, 
                        set_value('province_id', @$province_id),
                        'class="form-control" id="province-dropdown"' 
                    ); ?>
                </div>
                <div class="form-group">
                    <label>Select City</label>
                    <?php 
                        echo form_dropdown(
                            'city_id', 
                            array(), 
                            set_value('city_id', @$city_id),
                            'class="form-control" id="city-dropdown"' 
                        );
                    ?>
                </div>
                <div class="form-group">
                    <div id="list-cost"></div>
                </div>
                
                <div class="text-right">
                    <a href="<?php echo site_url('cart'); ?>" class="btn btn-red">Back to Cart</a>
                    <button class="btn btn-red" type="submit">Next</button>
                </div>
                
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <div class="spacer"></div>
</div>