    <div class="container">
		<div class="row">
			<div class="col-md-12"><!--Main content-->
				<div id="title-bg">
					<div class="title">Thank you for shopping with us</div>
				</div>
				<div>
                    <p>We have sent an email for your order details, please take your time to see it.</p>
                    <p>Click <a href="<?php echo site_url('account/orders') ?>">here</a> to see order history</p>
                    <p>Thank you for shopping with us</p>
                </div>
			</div><!--Main content-->
		</div>
		<div class="spacer"></div>
	</div>