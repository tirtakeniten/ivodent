    <div class="container">
		<div class="row">
			<div class="col-md-12"><!--Main content-->
				<div id="title-bg">
					<div class="title"><?php echo $page->title; ?></div>
				</div>
				<div>
                    <?php echo $page->content; ?>
                </div>
			</div><!--Main content-->
		</div>
		<div class="spacer"></div>
	</div>
    <div class="f-widget featpro">
		<div class="container">
			<div id="title-widget-bg">
				<div class="title-widget">Featured Products</div>
				<div class="carousel-nav">
					<a class="prev"></a>
					<a class="next"></a>
				</div>
			</div>
			<div id="product-carousel" class="owl-carousel owl-theme">
				<?php foreach($featured_products as $product_wrap): ?>
				<div class="item">
					<?php include '_product-wrap.php'; ?>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
