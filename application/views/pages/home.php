<?php 
    $sliders = get_image_slider();
?>
<div class="container">
		<div class="main-slide">
			<div id="sync1" class="owl-carousel">
                <?php
                    foreach($sliders as $k => $slider):
                ?>
				<div class="item">
					<div class="slide-type-1">
						<img src="<?php echo $slider['image']; ?>" alt="" class="img-responsive"/>
					</div>
				</div>
                <?php
                    endforeach;
                ?>
			</div>
		</div>
		<div class="bar"></div>
		<div id="sync2" class="owl-carousel">
            <?php foreach($sliders as $k => $slider): ?>
			<div class="item">
				<div class="slide-type-1-sync">
					<h3><?php echo $slider['title'] ?></h3>
					<p><?php echo $slider['description'] ?></p>
				</div>
			</div>
            <?php endforeach; ?>
		</div>
	</div>
    <div class="f-widget featpro">
		<div class="container">
			<div id="title-widget-bg">
				<div class="title-widget">Featured Products</div>
				<div class="carousel-nav">
					<a class="prev"></a>
					<a class="next"></a>
				</div>
			</div>
			<div id="product-carousel" class="owl-carousel owl-theme">
				<?php foreach($featured_products as $product_wrap): ?>
				<div class="item">
					<?php include '_product-wrap.php'; ?>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
    <div class="container">
		<div class="row">
			<div class="col-md-12"><!--Main content-->
				<div id="title-bg">
					<div class="title">Welcome to Ivodent Dental Laboratory</div>
				</div>
				<p class="ct">Ivodent Lab, established in 1997 by experienced dentist, Indra Guizot and located in Denpasar Bali. In the beginning year, there were only two assistants supporting Guizot to operate his lab, these two staffs were focused to support his clinic operation.
                </p>
                <p class="ct">Ivodent Lab with its motto one step ahead, always tries to be the leader in dental technic's matters. Less then a years running its operation. Ivodent Lab was visited by expert Germany dentist whom introduced porcelain IPS Classic and Targis - Vectris and continuously.
                </p>
                <p class="ct">Ivodent Lab builds relationship with a Dental Technician from Europe to ensure its Dental Technologies always one step ahead.</p>
				</p>
				<a href="<?php echo site_url('page/about-us'); ?>" class="btn btn-default btn-red btn-sm">Read More</a>
			</div><!--Main content-->
		</div>
        <div class="row prdct"><!--Products-->
            <div class="col-md-12">
				<div id="title-bg">
					<div class="title">Lastest Products</div>
				</div>
            </div>
			<?php foreach($latest_products as $product_wrap): ?>
			<div class="col-md-3">
				<?php include '_product-wrap.php'; ?>
			</div>
			<?php endforeach; ?>
		</div><!--Products-->
		<div class="spacer"></div>
	</div>
