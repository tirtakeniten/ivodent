<div class="form-group">
    <label><?php echo $mediaLabel; ?></label>
    <input type="hidden" class="siteURL" value="<?php echo site_url(); ?>" />
    <input type="hidden" id="<?php echo $mediaId; ?>" class="load-img" name="<?php echo $mediaControlName; ?>" value="<?php echo $mediaValue; ?>" onchange="loadImgOn(this);" />
    <div class="thumbnail img-container">
        <img src="" class="img-responsive"/>
        <div class="caption">
            <p class="text-center">
                <a href="<?php echo site_url('media/manager'); ?>?field_id=<?php echo $mediaId; ?>" class="media btn btn-lg btn-info btn-flat" title="Replace picture" data-fancybox-type="iframe">
                    <i class="fa fa-picture-o"></i>
                </a>
                <a href="" class="remove-media btn btn-lg btn-flat btn-danger" title="Remove picture" data-id="<?php echo $mediaId; ?>" onclick="removeImage(this); return false;">
                    <i class="fa fa-trash-o"></i>
                </a>
            </p>
        </div>
    </div>
    <div class="text-center bro-container">
        <p><i class="fa fa-picture-o fa-5x"></i></p>
        <p>
            <a href="<?php echo site_url('media/manager'); ?>?field_id=<?php echo $mediaId; ?>" class="media btn btn-default btn-flat" data-fancybox-type="iframe">Browse</a>
        </p>
    </div>
</div>