<?php
    $CI = &get_instance();
    $url = new Url();
?>
<div class="margin-bottom"></div>
<div class="container-fluid text-center">
    <a href="<?php echo $url->media(); ?>" class="btn btn-lg btn-flat btn-success">Back to File List</a>
</div>
<div class="margin-bottom"></div>
<div class="container-fluid">
    <div class="row">
        <form action="<?php echo $url->media('upload'); ?>" class="dropzone"></form>
    </div>
</div>