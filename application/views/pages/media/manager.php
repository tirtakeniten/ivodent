<?php
    $CI = &get_instance();
    $url = new Url();
?>
<input type="hidden" id="siteURL" value="<?php echo site_url(); ?>" />
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">File Manager</a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo $url->media('form'); ?>">Upload</a>
                </li>
                <li>
                    <a href="">Refresh</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Order By <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Upload time (Asc)</a></li>
                        <li><a href="#">Upload time (Desc)</a></li>
                        <li><a href="#">File Name (Asc)</a></li>
                        <li><a href="#">File Name (Desc)</a></li>
                        <li><a href="#">File Size (Asc)</a></li>
                        <li><a href="#">File Size (Desc)</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container-fluid">
    <div class="row">
        <input type="hidden" name="fieldID" value="<?php echo $CI->input->get('field_id'); ?>" />
    </div>    
    <div class="row">
        <?php foreach($files as $file): ?>
        <div class="col-md-2 col-sm-3 col-xs-6">
            <div class="thumbnail">
                <a href="">
                    <img src="<?php echo image_url($file, 'thumbnail'); ?>" />
                </a>
                <p style="width: 100%; overflow: hidden; height: 20px;" class="text-center">
                    <a href="<?php echo image_url($file, 'original'); ?>" title="<?php echo image_url($file, 'original'); ?>"><?php echo $file->filename; ?></a>
                </p>
                <p class="text-center">
                    <a href="#" class="btn btn-sm btn-default" title="Copy Image URL to Clipboard">
                        <i class="fa fa-clipboard"></i>
                    </a>
                    <a href="#" class="btn btn-sm btn-default" title="Delete">
                        <i class="fa fa-trash-o"></i>
                    </a>
                    <a href="#" class="btn btn-sm btn-default" title="Apply Image" onclick="selectImg(this);" data-img="<?php echo $file->id ?>">
                        <i class="fa fa-check"></i>
                    </a>
                </p>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>