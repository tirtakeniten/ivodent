<?php
    use Gregwar\Captcha\CaptchaBuilder;

    $CI = &get_instance();
    
    $captchaBuilder = new CaptchaBuilder;
    $captchaBuilder->setIgnoreAllEffects(true);
    $captchaBuilder->build();
    
    /* save captcha data */
    $CI->session->set_flashdata('captcha_str', $captchaBuilder->getPhrase());
?>    
    <div class="container">
		<div class="row">
			<div class="col-md-12"><!--Main content-->
				<div id="title-bg">
					<div class="title"><?php echo $page->title; ?></div>
				</div>
			</div><!--Main content-->
            <div class="col-md-6">
                <?php echo $page->content; ?>
            </div>
            <div class="col-md-6">
                <?php if ($CI->session->flashdata('error')) : ?>
                <div class="alert alert-danger">
                    <?php echo $CI->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                
                <?php if ($CI->session->flashdata('success')) : ?>
                <div class="alert alert-success">
                    <?php echo $CI->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                
                <?php echo form_open('send-email/contact-us', 'id="form-contact-us"'); ?>
                <div class="form-group">
			         <input type="text" name="name" value="<?php echo set_value('name'); ?>" class="form-control" id="name" placeholder="Your Name">
                </div>
                <div class="form-group">
                    <input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control" id="email" placeholder="Your Email">
                </div>
                <div class="form-group">
			         <input type="text" name="subject" value="<?php echo set_value('subject'); ?>" class="form-control" id="subject" placeholder="Subject">
                </div>
                <div class="form-group">
                    <textarea rows="8" name="message" placeholder="Message"  class="form-control" ><?php echo set_value('message'); ?></textarea>
                </div>
                <div>
                    <img src="<?php echo $captchaBuilder->inline(); ?>" />
                </div>
                 <div class="form-group">
			         <input type="text" name="captcha" value="<?php echo set_value('subject'); ?>" class="form-control" id="captcha">
                </div>
                <div>
                    <button type="submit" class="btn btn-red"> Submit </button>
                </div>
                <?php echo form_close(); ?>
            </div>
		</div>
		<div class="spacer"></div>
	</div>
    <div class="f-widget featpro">
		<div class="container">
			<div id="title-widget-bg">
				<div class="title-widget">Featured Products</div>
				<div class="carousel-nav">
					<a class="prev"></a>
					<a class="next"></a>
				</div>
			</div>
			<div id="product-carousel" class="owl-carousel owl-theme">
				<?php foreach($featured_products as $product_wrap): ?>
				<div class="item">
					<?php include '_product-wrap.php'; ?>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
