<?php 
    $CI = &get_instance();
?>
<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page-title-wrap">
					<div class="page-title-inner">
					<div class="row">
						<div class="col-md-12">
							<div class="bread"><a href="<?php echo site_url(); ?>">Home</a> &rsaquo; <?php echo $product->name ?></div>
							<div class="bigtitle"><?php echo $product->name ?></div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12"><!--Main content-->
				<div id="title-bg">
					<div class="title"><?php echo $product->name ?></div>
				</div>
                <?php if ($CI->session->flashdata('success')): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            <?php echo $CI->session->flashdata('success'); ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
				<div class="row">
					<div class="col-md-4">
                        <div class="dt-img easyzoom easyzoom--adjacent easyzoom--with-thumbnails">
							<a href="<?php echo $product->images[0]['original']; ?>">
            					<img src="<?php echo $product->images[0]['large']; ?>" alt="" class="img-responsive"/>
            				</a>
						</div>
                        <div class="row galley-thumbnail-container">
                            <?php foreach($product->images as $k => $image): ?>
                            <div class="col-sm-3 thumb-img">
            					<a href="<?php echo $image['original']; ?>" data-standard="<?php echo $image['large']; ?>">
            						<img src="<?php echo $image['thumbnail']; ?>" alt="" class="img-responsive" />
            					</a>
            				</div>
                            <?php endforeach; ?>
                        </div>
					</div>
                    <div class="col-md-4 det-desc">
                        <div><?php echo $product->note ?></div>
                        <h3>Additionl Information</h3>
                        <div><?php echo $product->description ?></div>
                    </div>
					<div class="col-md-4 det-desc">
						<div class="productdata">
                            <p>
                                <?php if($product->sale_price > 0): ?>
                                <span class="price">
                                IDR <?php echo format_number(usd_to_idr($product->sale_price, 'db')); ?>
                                <span style="display: none;"><?php echo currency_format($product->sale_price); ?></span>
                                </span><br />
                                <span class="price-strikeout">
                                IDR <?php echo format_number(usd_to_idr($product->price)); ?>
                                </span>
                                <?php else: ?>
                                <span class="price">
                                IDR <?php echo format_number(usd_to_idr($product->price, 'db')); ?>
                                </span>
                                <?php endif; ?>
                            </p>
                            <div class="dash"></div>
							<h4>Available Options</h4>
							<form class="ava" method="post" action="<?php echo site_url('cart/add'); ?>" role="form">
                                <input type="hidden" value="<?php echo $product->id ?>" name="product_id"/>
								<?php if(isset($varians['0'])): ?>
                                <div class="form-group">
									<label for="color" class="control-label">Select <?php echo $varians['0']->options ?></label>
									<div class="">
										<select class="form-control" id="color">
											<option>Select options</option>
                                            <?php foreach($varians as $varian): ?>
											<option value="<?php echo $varian->id ?>"><?php echo $varian->values ?></option>
                                            <?php endforeach; ?>
										</select>
									</div>
                                </div>
								<div class="clearfix"></div>
								<div class="dash"></div>
                                <?php endif; ?>
                                <div class="form-group">
									<label for="qty" class="control-label">Qty</label>
                                    <div class="">
										<select class="form-control" id="qty" name="qty">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="clearfix"></div>
								</div>
                                <div class="form-group">
                                    <button class="btn btn-default btn-red btn-sm"><i class="fa fa-shopping-cart"></i> Add To Chart</button>
                                    <?php if (!$in_wishlist) : ?>
                                    <a href="<?php echo site_url('wishlist/add/'.$product->id); ?>" class="btn btn-default btn-dark btn-sm pull-right"><i class="fa fa-heart-o"></i> Add To Wishlist</a>
                                    <?php else : ?>
                                    <a href="<?php echo site_url('wishlist'); ?>" class="btn btn-default btn-yellow btn-sm pull-right"><i class="fa fa-check"></i> Wishlist</a>
                                    <?php endif; ?>
                                </div>
							</form>
							
							<div class="sharing">
								<div class="share-bt">
									<div class="addthis_toolbox addthis_default_style ">
										<a class="addthis_counter addthis_pill_style"></a>
									</div>
									<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4f0d0827271d1c3b"></script>
									<div class="clearfix"></div>
								</div>
								<div class="avatock"><span>In stock</span></div>
							</div>
							
						</div>
					</div>
				</div>

				<div class="tab-review">
					<ul id="myTab" class="nav nav-tabs shop-tab">
						<li class="active"><a href="#desc" data-toggle="tab">Description</a></li>
						<li class=""><a href="#rev" data-toggle="tab">Reviews (0)</a></li>
					</ul>
					<div id="myTabContent" class="tab-content shop-tab-ct">
						<div class="tab-pane fade active in" id="desc">
							<?php echo $product->description; ?>
						</div>
						<div class="tab-pane fade" id="rev">
							<p class="dash">
							 
							</p>
							<h4>Write Review</h4>
							<form role="form">
							<div class="form-group">
								<input type="text" class="form-control" id="name" >
							</div>
							<div class="form-group">
								<textarea class="form-control" id="text" ></textarea>
							</div>
							
							<button type="submit" class="btn btn-default btn-red btn-sm">Submit</button>
						</form>
							
						</div>
					</div>
				</div>
				
				<div id="title-bg">
					<div class="title">Related Product</div>
				</div>
				<div class="row prdct"><!--Products-->
                    <?php foreach($related_products as $product_wrap): ?>
    				<div class="col-md-4">
    					<?php include '_product-wrap.php'; ?>
    				</div>
    				<?php endforeach; ?>
				</div><!--Products-->
				<div class="spacer"></div>
			</div><!--Main content-->
		</div>
	</div>