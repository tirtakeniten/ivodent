<?php 
    $CI = &get_instance();
?>
<section>
    <div class="container">
        <!-- TITLE -->
        <div class="row">
			<div class="col-md-12">
				<div class="page-title-wrap">
					<div class="page-title-inner">
					<div class="row">
						<div class="col-md-12">
							<div class="bread"><a href="#">Home</a> &rsaquo; Register</div>
							<div class="bigtitle">Register</div>
                            <p></p>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
        <!-- TITLE -->
        <?php echo form_open(current_url() , 'class="form-horizontal checkout"'); ?>
        <div class="row">
            <div class="col-md-6 bill">
				<div id="title-bg">
					<div class="title">Account Details</div>
				</div>
                
                <?php if(form_error('fname') ||
                           form_error('lname') ||
                           form_error('email') || 
                           form_error('password') ): ?>
                <div class="alert alert-danger">
                    <?php
                        echo form_error('fname');
                        echo form_error('lname');
                        echo form_error('email');
                        echo form_error('password');
                    ?>
                </div>
                <?php endif; ?>
                
				<div class="form-group dob">
					<div class="col-sm-6">
						<input type="text" name="fname" value="<?php echo set_value('fname'); ?>" class="form-control" id="name" placeholder="Name">
					</div>
					<div class="col-sm-6">
						<input type="text" name="lname" value="<?php echo set_value('lname'); ?>" class="form-control" id="last_name" placeholder="Last Name">
					</div>
				</div>
				<div class="form-group dob">
					<div class="col-sm-12">
						<input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control" id="email" placeholder="Email">
					</div>
				</div>
				<div class="form-group dob">
					<div class="col-sm-12">
						<input type="password" name="password" value="<?php echo set_value('password'); ?>" class="form-control" id="password" placeholder="Password">
					</div>
				</div>
			</div>
            <div class="col-md-6 bill">
				<div id="title-bg">
					<div class="title">Address</div>
				</div>
                 <?php if(form_error('company') || 
                            form_error('address') || 
                            form_error('city') || 
                            form_error('post_code') || 
                            form_error('province') ): ?>
                <div class="alert alert-danger">
                    <?php
                        echo form_error('company');
                        echo form_error('address');
                        echo form_error('city');
                        echo form_error('post_code');
                        echo form_error('province');
                    ?>
                </div>
                <?php endif; ?>
                
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" name="company" value="<?php echo set_value('company'); ?>" class="form-control" id="company" placeholder="Company">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<input type="text" name="address" value="<?php echo set_value('address'); ?>" class="form-control" id="address" placeholder="Address">
					</div>
				</div>
				<div class="form-group dob">
					<div class="col-sm-6">
						<input type="text" name="city" value="<?php echo set_value('city'); ?>" class="form-control" id="city" placeholder="City">
					</div>
					<div class="col-sm-6">
						<input type="text" name="post_code" value="<?php echo set_value('post_code'); ?>" class="form-control" id="post_code" placeholder="Post Code">
					</div>
				</div>
				<div class="form-group dob">
					<div class="col-sm-6">
						<input type="text" name="province" value="<?php echo set_value('province'); ?>" class="form-control" id="province" placeholder="Province">
					</div>
					<div class="col-sm-6">
						<input type="text" name="phone" value="<?php echo set_value('phone'); ?>" class="form-control" id="phone" placeholder="Phone Number">
					</div>
				</div>
				<div class="form-group dob">
                    <div class="col-sm-12">
						<input type="text" name="country" value="<?php echo set_value('country', 'Indonesia'); ?>" class="form-control" id="country" placeholder="Country" value="Indonesia" readonly="">
					</div>
				</div>
			</div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <?php if(form_error('agree_term') ): ?>
                <div class="alert alert-danger">
                    <?php
                        echo form_error('agree_term');
                    ?>
                </div>
                <?php endif; ?>
                <div class="form-group dob">
                    <label>
                        <input type="checkbox" name="agree_term" value="1" <?php echo set_checkbox('agree_term', 1, FALSE); ?>/>
                        I agree the 
                        <a class="fancybox" data-fancybox-type="iframe" href="<?php echo site_url('home/terms_and_conditions'); ?>">Terms and Conditions</a>
                    </label>
                </div>
                <button type="submit" class="btn-lg btn-red">Register</button>
            </div>
        </div>
        </form>
    </div>
    <div class="spacer"></div>
</section>