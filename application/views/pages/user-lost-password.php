<?php 
    $CI = &get_instance();
?>
<div class="container">
    <div class="row">
        <div class="col-md-6 al-cus">
			<div id="title-bg">
				<div class="title">Reset Password</div>
			</div>
            <?php if (validation_errors()) : ?>
            <div class="alert alert-danger">
                <?php echo validation_errors(); ?>
            </div>
            <?php endif; ?>
            
            <?php if ($CI->session->flashdata('success')) : ?>
            <div class="alert alert-success">
                <?php echo $CI->session->flashdata('success'); ?>
            </div>
            <?php endif; ?>
            
			<?php echo form_open(current_url()); ?>
				<p>
					Please input your email address below. We will send your temporary password.
				</p>
				<div class="form-group">
					<input type="text" name="email" class="form-control" id="exampleInput" placeholder="Email">
				</div>
				<button class="btn btn-default btn-red">Reset my password</button>
			<?php echo form_close(); ?>
		</div>
        <div class="col-md-6 new-cus">
			<div id="title-bg">
				<div class="title">Don't Have An Account?</div>
			</div>
            <div>
				<a href="<?php echo site_url('register'); ?>" class="btn btn-default btn-red">Register Now</a>
                 <p><br />
					By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have made.
				</p>
			</div>
		</div>
    </div>
    <div class="spacer"></div>
</div>