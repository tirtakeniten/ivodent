<?php
    $CI =&get_instance();
    $enable_show_address = FALSE;
    $enable_delete = TRUE;
    $show_default = TRUE;
?>
<section>    
    <div class="container">
        <!-- Promotional content -->
        <div id="title-bg">
			<div class="title">Wishlist</div>
		</div>
        <div class="row">
            <div class="col-md-9">
                <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php endif; ?>
                <?php if ($this->session->flashdata('error')): ?>
                <div class="alert alert-danger">
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php endif; ?>
                <div class="row">
                    <?php if (count($wishlist) > 0): ?>
                        <?php foreach ($wishlist as $wish) : ?>
                    <div class="col-md-4">
                            <?php $product_wrap = $wish->product; ?>
                            <?php require '_wishlist-wrap.php'; ?>
                    </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                            There is no item in your wishlist. <a href="<?php echo site_url(); ?>">Back to home</a>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-3">
                <?php echo $account_sidebar; ?>
            </div>
        </div>
        <!-- Promotional content -->
    </div>
    
    <div class="spacer"></div>
</section>