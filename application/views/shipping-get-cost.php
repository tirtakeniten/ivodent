<?php 
    $is_free_shipping = in_array($dest_city->city_id, $free_shipping_dest_city_code);
?>
<label>Select Shipping Service</label>
<?php if ($is_free_shipping): ?>
<div class="alert alert-info">
    <p>Free shipping to <?php echo "{$dest_city->type} {$dest_city->city_name} - {$dest_city->province}" ?></p>
</div>
<?php endif; ?>
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th></th>
            <th>Service</th>
            <th>Cost</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($costs->costs as $cost): ?>
        <?php
            if ($is_free_shipping) {
                $cost->cost[0]->value = 0;
            }
            $costValue = json_encode($cost);
        ?>
        <tr onclick="selectCost(this);">
            <td><?php echo form_radio('cost', $costValue); ?></td>
            <td><?php echo $cost->description ?>(<?php echo $cost->service ?>)</td>
            <td>IDR <?php echo format_number($cost->cost[0]->value); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>