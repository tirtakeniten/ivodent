<div class="links-box">
	<ul>
		<li><a href="<?php echo site_url('account'); ?>">My Account</a></li>
		<li><a href="<?php echo site_url('account/update'); ?>">Update Account</a></li>
        <li><a href="<?php echo site_url('account/change-password'); ?>">Change Password</a></li>
		<li><a href="<?php echo site_url('account/address'); ?>">My Addresses</a></li>
        <li><a href="<?php echo site_url('account/orders'); ?>">Purchase History</a></li>
        <li><a href="<?php echo site_url('wishlist'); ?>">My Wishlist</a></li>
		<li class="lastone"><a href="<?php echo site_url('user/logout'); ?>">Log out</a></li>
	</ul>
    <div class="clearfix"></div>
</div>