<?php
    $url = new Url();
?>
<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="header"></li>
    <li>
        <a href="<?php echo $url->admin(); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    <!-- Product -->
    <li class="treeview">
      <a href="#">
        <i class="fa fa-tags"></i>
        <span>Products</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo $url->admin('product'); ?>"><i class="fa fa-circle-o"></i> All Products</a></li>
        <li><a href="<?php echo $url->admin('product_form'); ?>"><i class="fa fa-circle-o"></i> Add New Product</a></li>
      </ul>
    </li>
    <!-- Product -->
    
    <!-- Order -->
    <li class="treeview">
      <a href="#">
        <i class="fa fa-file-o"></i>
        <span>Sales</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo $url->admin('order'); ?>"><i class="fa fa-circle-o"></i> Orders</a></li>
        <li><a href="<?php echo $url->admin('user'); ?>"><i class="fa fa-circle-o"></i> Customers</a></li>
      </ul>
    </li>
    <!-- Order -->
    
    <!-- Category -->
    <li class="treeview">
      <a href="#">
        <i class="fa fa-cubes"></i>
        <span>Categories</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo $url->admin('category'); ?>"><i class="fa fa-circle-o"></i> All Categories</a></li>
        <li><a href="<?php echo $url->admin('category_form'); ?>"><i class="fa fa-circle-o"></i> Add New Category</a></li>
      </ul>
    </li>
    <!-- Category -->
    
    <!-- Pages -->
    <li class="treeview">
      <a href="#">
        <i class="fa fa-file-o"></i>
        <span>Pages</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo $url->admin('page'); ?>"><i class="fa fa-circle-o"></i> All Pages</a></li>
        <li><a href="<?php echo $url->admin('page_form'); ?>"><i class="fa fa-circle-o"></i> Add New Page</a></li>
      </ul>
    </li>
    <!-- Pages -->
    
    <!-- Setting -->
    <li class="treeview">
      <a href="#">
        <i class="fa fa-wrench"></i>
        <span>Settings</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php echo $url->admin('setting', 'general'); ?>"><i class="fa fa-circle-o"></i> General</a></li>
        <li><a href="<?php echo $url->admin('setting', 'image'); ?>"><i class="fa fa-circle-o"></i> Image</a></li>
        <li><a href="<?php echo $url->admin('setting', 'image_slider'); ?>"><i class="fa fa-circle-o"></i> Image Slider</a></li>
        <li><a href="<?php echo $url->admin('setting', 'currency'); ?>"><i class="fa fa-circle-o"></i> Currency Conversion</a></li>
        <li><a href="<?php echo $url->admin('setting', 'seo'); ?>"><i class="fa fa-circle-o"></i> Search Engine</a></li>
        <!--i><a href="<?php echo $url->admin('setting', 'outgoing_email'); ?>"><i class="fa fa-circle-o"></i>Outgoing Email</a></li-->
      </ul>
    </li>
    <!-- Setting -->
  </ul>
</section>
<!-- /.sidebar -->
</aside>

<!-- =============================================== -->