<div class="f-widget"><!--footer Widget-->
		<div class="container">
			<div class="row">
				<div class="col-md-4" style="padding-top: 10px;"><!--footer twitter widget-->
					<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/balidental" data-widget-id="554534634717855744">Tweets by @balidental</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div><!--footer twitter widget-->
				<div class="col-md-4"><!--footer newsletter widget-->
					<div id="title-widget-bg">
						<div class="title-widget">Newsletter Signup</div>
					</div>
					<div class="newsletter">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</p>
						<form role="form">
							<div class="form-group">
								<label>Your Email address</label>
								<input type="email" class="form-control newstler-input" id="exampleInputEmail1" placeholder="Enter email">
								<button class="btn btn-default btn-red btn-sm">Sign Up</button>
							</div>
						</form>
					</div>
				</div><!--footer newsletter widget-->
				<div class="col-md-4"><!--footer contact widget-->
					<div id="title-widget-bg">
						<div class="title-widget-cursive">Ivodent</div>
					</div>
					<ul class="contact-widget">
						<li class="">Jln Patimura No. 9 Denpasar<br />Bali - Indonesia</li>
						<li class="">Ph. (62 361) 237 777<br />Fx. (62 361) 226 834</li>
						<li class="lastone">iguizot@sindosat.net.id</li>
					</ul>
				</div><!--footer contact widget-->
			</div>
			<div class="spacer"></div>
		</div>
	</div><!--footer Widget-->
	<div class="footer"><!--footer-->
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<ul class="footermenu"><!--footer nav-->
						<li><a href="<?php echo site_url(); ?>">Home</a></li>
						<li><a href="<?php echo site_url('page/about-us'); ?>">About Us</a></li>
                        <li><a href="<?php echo site_url('page/products'); ?>">Products</a></li>
                        <li><a href="<?php echo site_url('page/brands'); ?>">Brands</a></li>
                        <li><a href="<?php echo site_url('page/technology'); ?>">Technology</a></li>
                        <li><a href="<?php echo site_url('page/contact-us'); ?>">Contact Us</a></li>
                        <li><a href="<?php echo site_url('page/location'); ?>">Location</a></li>
					</ul><!--footer nav-->
					<div class="f-credit">
                        &copy; 2015 <a href="<?php echo site_url(); ?>">Ivodent Dental Laboratory</a>. All rights reserved. Designed and maintained by <a href="http://www.baliwebpro.com/" target="_blank">BaliWebPro.com</a>
                    </div>
					<!--a href=""><div class="payment visa"></div></a>
					<a href=""><div class="payment paypal"></div></a>
					<a href=""><div class="payment mc"></div></a>
					<a href=""><div class="payment nh"></div></a-->
				</div>
				<div class="col-md-3"><!--footer Share-->
					<div class="followon">Follow us on</div>
					<div class="fsoc">
						<a href="http://twitter.com/minimalthemes" class="ftwitter">twitter</a>
						<a href="http://www.facebook.com/pages/Minimal-Themes/264056723661265" class="ffacebook">facebook</a>
						<a href="#" class="fflickr">flickr</a>
						<a href="#" class="ffeed">feed</a>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div><!--footer Share-->
			</div>
		</div>
	</div><!--footer-->