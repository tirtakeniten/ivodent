<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $seo_title ?></title>
    
    <!-- Google SEO -->
    <meta name="description" content="<?php echo $seo_description ?>">
    <meta name="google-site-verification" content="<?php echo $google_site_verification ?>"/>
    <meta name="robots" content="index,follow">
    <script><?php echo $google_analytics ?></script>
    
    <!-- Open Graph Protocol -->
    <meta property="og:title" content="<?php echo $seo_title ?>" />
    <meta property="og:description" content="<?php echo $seo_description ?>" />
    <meta property="og:site_name" content="<?php echo get_site_config('site_name'); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo current_url(); ?>" />
    <meta property="og:image" content="<?php echo $seo_og_image; ?>" />
    
    <!-- Twitter Cards -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="<?php echo $twitter_account; ?>">
    <meta name="twitter:creator" content="<?php echo $twitter_account; ?>">
    <meta name="twitter:title" content="<?php echo $seo_title ?>">
    <meta name="twitter:description" content="<?php echo $seo_description ?>">
    <meta name="twitter:image" content="<?php echo $twitter_image ?>">
    
    <!-- Fonts  -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,400italic,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
   
    <?php echo $css; ?>
    
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>