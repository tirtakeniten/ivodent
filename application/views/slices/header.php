<?php
	$CI = &get_instance();
	$CI->load->model('category_model');
    $CI->load->library('cart_factory');
    $CI->load->helper('string');
    $loggedUser = $CI->auth->user();
    $cart_factory = $CI->cart_factory;
?>
<div class="header"><!--Header -->
		<div class="container">
			<div class="row">
				<div class="col-xs-6 col-md-4 main-logo">
					<a href="<?php echo site_url(); ?>"><img src="<?php echo asset_url('img/logo.png'); ?>" alt="logo" class="logo img-responsive" /></a>
				</div>
				<div class="col-md-8">
					<div class="pushright">
						<div class="top">
                            <?php if (is_null($loggedUser)): ?>
							<a href="#" id="reg" class="btn btn-default btn-dark">Login<span>-- Or --</span>Register</a>
							<div class="regwrap">
								<div class="row">
									<div class="col-md-6 regform">
										<div id="title-widget-bg">
											<div class="title-widget">Login</div>
										</div>
										<form role="form" method="post" action="<?php echo site_url('account_login'); ?>?redirect=<?php echo $CI->uri->uri_string(); ?>">
											<div class="form-group">
												<input type="email" name="email" class="form-control" id="email" placeholder="Email">
											</div>
											<div class="form-group">
												<input type="password" name="password" class="form-control" id="password" placeholder="password">
											</div>
											<div class="form-group">
												<button class="btn btn-default btn-red btn-sm">Sign In</button>
											</div>
										</form>
									</div>
									<div class="col-md-6">
										<div id="title-widget-bg">
											<div class="title-widget">Register</div>
										</div>
										<p>
											New User? By creating an account you be able to shop faster, be up to date on an order's status...
										</p>
										<a href="<?php echo base_url('register'); ?>" class="btn btn-default btn-yellow">Register Now</a>
									</div>
								</div>
							</div>
                            <?php else: ?>
                            <a href="<?php echo site_url('account'); ?>" id="" class="btn btn-default btn-dark">Hi, <?php echo $loggedUser->username ?></a>
                            <?php if ($loggedUser->role == 'admin'): ?>
                            <a href="<?php echo site_url('admin'); ?>" id="" class="btn btn-default btn-dark">Admin Dashboard</a>
                            <?php endif; ?>
                            <?php endif; ?>
							<div class="srch-wrap">
								<a href="#" id="srch" class="btn btn-default btn-search"><i class="fa fa-search"></i></a>
							</div>
							<div class="srchwrap">
								<div class="row">
									<div class="col-md-12">
										<form class="form-horizontal" role="form">
											<div class="form-group">
												<label for="search" class="col-sm-2 control-label">Search</label>
												<div class="col-sm-10">
													<input type="text" class="form-control" id="search">
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="dashed"></div>
	</div><!--Header -->
    <div class="main-nav"><!--end main-nav -->
		<div class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="navbar-collapse collapse">
							<ul class="nav navbar-nav">
								<li><a href="<?php echo site_url(); ?>" class="active">Home</a><div class="curve"></div></li>
								<li><a href="<?php echo site_url('page/about-us'); ?>">About Us</a></li>
                                <li><a href="<?php echo site_url('page/products'); ?>">Products</a></li>
                                <li><a href="<?php echo site_url('page/brands'); ?>">Brands</a></li>
                                <!-- Shop -->
                                <li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<?php
											$shopCategories = $CI->category_model->get_hierarchical();
											foreach($shopCategories as $shopCategory):
                                                if (count($shopCategory->children) > 0) :
										?>
                                        <li class="dropdown-submenu">
                                            <a tabindex="-1" href="<?php echo site_url('category/'.$shopCategory->slug); ?>" title="<?php echo $shopCategory->name ?>"><?php echo $shopCategory->name ?></a>
                                            <ul class="dropdown-menu">
                                                <?php foreach($shopCategory->children as $childCategory): ?>
                                                <li><a tabindex="-1" href="<?php echo site_url('category/'.$childCategory->slug); ?>" title="<?php echo $childCategory->name ?>"><?php echo $childCategory->name ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </li>
										<?php else: ?>
                                        <li><a href="<?php echo site_url('category/'.$shopCategory->slug); ?>" title="<?php echo $shopCategory->name ?>"><?php echo $shopCategory->name ?></a></li>
										<?php
                                                endif;
											endforeach;
										?>
									</ul>
								</li>
                                <!-- Shop -->
                                <li><a href="<?php echo site_url('page/technology'); ?>">Technology</a></li>
                                <li><a href="<?php echo site_url('page/contact-us'); ?>">Contact Us</a></li>
                                <li><a href="<?php echo site_url('page/location'); ?>">Location</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3 machart text-right">
						<button id="popcart" class="btn btn-default btn-chart btn-sm "><span class="mychart">Cart</span>|<span class="allprice">IDR <?php echo format_number($cart_factory->cart()->total()); ?></span></button>
                        <?php if (count($cart_factory->contents()) > 0): ?>
                        <div class="popcart">
							<table class="table table-condensed popcart-inner">
								<tbody>
                                    <?php foreach($cart_factory->contents(true) as $row_id => $cart_item) : ?><?php $cart_product = $cart_item['product']; ?>
                                    
									<tr>
										<td>
                                            <a href="<?php echo site_url('product/'.$cart_product->slug); ?>"><img src="<?php echo $cart_product->images[0]['thumbnail']; ?>" width="100" /></a>
                                            </td>
										<td><a href="<?php echo site_url('product/'.$cart_product->slug); ?>" title="<?php echo $cart_product->name; ?>"><?php echo character_limiter($cart_product->name, 20); ?></a></td>
										<td><?php echo $cart_item['qty'] ?> &times;</td>
										<td><?php echo currency_format($cart_item['price']); ?></td>
										<td>
                                            <a href="<?php echo site_url('cart/update'); ?>?rowid=<?php echo $row_id; ?>" title="Remove from cart" style="color: #fff;" onclick="return confirm('Are you sure want to remove this item from your cart?');"><i class="fa fa-times-circle fa-2x"></i></a>
                                        </td>
									</tr>
                                    <?php endforeach; ?>
								</tbody>
							</table>
							<div class="btn-popcart">
								<a href="<?php echo site_url('checkout'); ?>" class="btn btn-default btn-red btn-sm">Checkout</a>
								<a href="<?php echo site_url('cart'); ?>" class="btn btn-default btn-red btn-sm">More</a>
							</div>
							<div class="popcart-tot">
								<p>
									Total<br/>
									<span>IDR <?php echo format_number($cart_factory->cart()->total()); ?></span>
								</p>
							</div>
							<div class="clearfix"></div>
						</div>
                        <?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div><!--end main-nav -->
    <div class="container">
		<ul class="small-menu"><!--small-nav -->
			<li><a href="<?php echo site_url('account'); ?>" class="myacc">My Account</a></li>
			<li><a href="<?php echo site_url('cart'); ?>"  class="myshop">Shopping Chart</a></li>
			<li><a href="<?php echo site_url('checkout'); ?>"  class="mycheck">Checkout</a></li>
		</ul><!--small-nav -->
		<div class="clearfix"></div>
		<div class="lines"></div>
    </div><!-- .container -->
