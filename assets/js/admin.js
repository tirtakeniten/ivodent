/* load image from text */
function loadImgOn(ele){
    var imgID = $(ele).val();
    var siteURL = $('.siteURL').val();
    var container = $(ele).next('.img-container');
    
    if(imgID != ''){
        $.ajax({
            url: siteURL + 'media/manager/get/' + imgID,
            type: 'GET',
            dataType: 'json',
            success: function(data){
                container.find('img').prop('src', data.file.size.thumbnail);
            }
        });
        
        container.show();
        container.next('.bro-container').hide();
    }
}

function removeImage(ele){
    var imgID = $(ele).data('id');    
    //$('#'+imgID).prop('src', '');
    $('#'+imgID).val('').trigger('change');
    
    var container = $('#'+imgID).next('.img-container');
    container.hide();
    container.next('.bro-container').show();
    return false;
}

function loadExistingImages(){
    var controls = $('.load-img');
    
    controls.each(function(index){
        $(this).trigger('onchange');
    });
}

$(function(){
    $('.img-container').hide();
    loadExistingImages();
    
    /* datatables */
    $('.dataTables').DataTable();
    
    /* delete confirmation */
    $('.data-delete').click(function(){
        return confirm('Are you sure want to delete this data?');
    });
    
    /* prevent submit on keypress */
    $('.control-prevent-submit').keypress(function(event){
        if(event.keyCode == 13){
            return false;
        }
    });
    
    //Flat red color scheme for iCheck
    $('input[type="checkbox"], input[type="radio"]').iCheck({
      checkboxClass: 'icheckbox_minimal-green',
      radioClass: 'icheckbox_minimal-green'
    });
    
    //TinyMCE
    //$('.tinymce').tinymce();
    tinyMCE.init({
        selector:'.tinymce',
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar: [
            "bold italic strikethrough underline bullist numlist blockquote hr alignleft aligncenter alignright alignfull alignjustify link unlink fullscreen code",
            "styleselect forecolor pastetext removeformat table charmap outdent indent undo redo image"
        ],
        relative_urls: false,
        remove_script_host: false
    });
    
    /* multiple boxes */
    $('.box-change-class .box').click(function(){
        $('.box-change-class .box').removeClass(function(index,classes){
            var matches = classes.match(/\bbox-\S+/ig);
            return (matches) ? matches.join(' ') : '';   
        });
        $(this).addClass('box-primary');
    });
    
    /* media */
    $(".media").fancybox({
		fitToView	: false,
		width		: '90%',
		height		: '90%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});