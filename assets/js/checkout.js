$(function(){
    var siteURL = $('#site-url').val();
    
    var ajaxLoading = $('#ajax-loading');
    ajaxLoading.hide();
    $(document).ajaxStart(function() {
        ajaxLoading.show();
    });
    $(document).ajaxStop(function() {
        ajaxLoading.hide();
    });
    
    /* get shipping city */
    $('#province-dropdown').change(function(e){
        $.ajax({
            url: siteURL + 'checkout/shipping/get_shipping_city',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                $('#city-dropdown').replaceWith(data);
                $('#list-cost').html('');
            }
        });
    });
    $('#province-dropdown').change();
    
    $('#form-shipping').submit(function(e){
        return validate();
    });
    
    $('#form-confirm-order').submit(function(e){
        if($(this).find('[name="agree_term"]:checked').length == 0){
            alert('You must agree our terms and conditions to confirm your order.');
            return false;
        }else{
            return true;
        }
    });
    
    $('.resend-order').click(function(e){
        $.get( $(this).prop('href') , function() {
            alert("The order summary has been sent to your email");
        });
        return false;
    });
    
});

var getCost = function(element){
    var siteURL = $('#site-url').val();
    
    $.ajax({
        url: siteURL + 'checkout/shipping/get_shipping_cost',
        type: 'POST',
        data: $(element).serialize(),
        success: function(data){
            if(data == 'NULL'){
                data = 'Sorry, the service that you requested is not available.';
            }
            $('#list-cost').html(data);
        }
    });
}

var selectCost = function(element){
    $(element).find('input').prop('checked', 1);
}

var required = function(element, errorMessage){
    var req = $(element) != 'undefined' && $(element).val() != 'undefined' && $(element).val() != '' && $(element).val() != '0';
    if(!req && typeof errorMessage != 'undefined' && errorMessage != ''){
        bootstrapErrorAlert(errorMessage);
    }
    return req;
}

var bootstrapErrorAlert = function(errorMessage){
    var ele = $('<div class="alert alert-danger"/>').html('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
    ele.append(errorMessage);
    ele.fadeTo(5000, 500).slideUp(500, function(){
        $(this).find('alert').alert('close');
    });
    $('#form-shipping').prepend(ele);
}

function validate(){
    var validCost = $('#list-cost input:checked').length > 0;
    if(!validCost && $('#list-cost input').length > 0){ bootstrapErrorAlert('Please select shipping service.') }
    var validCity = required($('#city-dropdown'), "Please select city.");
    var validProvince = required($('#province-dropdown'), "Please select province."); 
    
    var valid = validProvince && validCity && validCost;
    
    return valid;
}