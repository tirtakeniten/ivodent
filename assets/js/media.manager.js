;

function selectImg(ele){
    var img = $(ele).data("img");
    var fieldID = $('input[name="fieldID"]').val();

    if(fieldID == '_tinymce'){
        var mce = parent.tinymce.activeEditor;
        var siteURL = document.getElementById('siteURL').value;
        parent.$.ajax({
            url: siteURL + 'media/manager/get/' + img,
            type: 'GET',
            dataType: 'json',
            success: function(data){
                mce.insertContent('<img src="'+data.file.size.original+'" style="max-width: 100%;"/>');
            }
        });
    }else{
        var textTarget = parent.document.getElementById(fieldID);
        $(textTarget).val(img).trigger('change');
    }
    
    parent.$.fancybox.close();
    return false;
}

jQuery(document).ready(function($){
    
});