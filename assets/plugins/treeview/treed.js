$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon glyphicon-minus-sign';
      var closedClass = 'glyphicon glyphicon-plus-sign';
      var clickAction = function(e){}
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
        if (typeof o.clickAction != 'undefined'){
        clickAction = o.clickAction;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator " + openedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            });
            //branch.children().children().toggle();
        });
        tree.find('li[class!="branch"]').each(function () {
            $(this).find("a").click(function(e){
                clickAction($(this));
                e.preventDefault();
            });
        });
        
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                clickAction($(this));
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('#tree1').treed();

$('#tree2').treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});

$('#tree3').treed({
    openedClass:'fa fa-minus-circle', 
    closedClass:'fa fa-plus-circle',
    clickAction: function(e){
        var targetID = '#categoryTarget';
        var url = $(e).attr('href');
        
        $.ajax({
            url: url,
            success: function(data){
                var target = $(targetID).find('.box-body');
                var dataJson = $.parseJSON(data);
                
                target.html('');
                if(dataJson.featured_image != null){
                    target.append($('<img src="'+dataJson.featured_image_url.large+'" class="img-responsive">'));
                }
                target.append($('<h2>'+dataJson.name+'</h2>'));
                target.append($('<p>'+dataJson.description+'</p>'));
                
                var editLink = $('<a href="'+document.location+'/form/'+dataJson.id+'" class="btn btn-info">Edit</a>');
                var linkContainer = $('<p>').append(editLink);
                
                target.append(linkContainer);
            }
        });
    }
});
