/*
SQLyog Ultimate v8.32 
MySQL - 5.6.14 : Database - ivodent
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ivo_categories` */

DROP TABLE IF EXISTS `ivo_categories`;

CREATE TABLE `ivo_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `featured_image` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keyword` varchar(255) DEFAULT NULL,
  `created_at` time DEFAULT NULL,
  `updated_at` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_categories` */

insert  into `ivo_categories`(`id`,`parent_id`,`code`,`name`,`description`,`featured_image`,`slug`,`seo_title`,`seo_description`,`seo_keyword`,`created_at`,`updated_at`) values (1,0,'IMPLANS','Implants by Sistem','',0,'implants-by-sistem','Implants by Sistem','Implants by Sistem','Implants by Sistem','08:13:48','08:13:48'),(2,3,'SwishActive','SwishActive','',1,'swishactive','SwishActive','SwishActive','SwishActive','08:16:33','08:17:31'),(3,1,'SwishSystem','Swish System','',1,'swish-system','Swish System','Swish System','Swish System','08:17:19','08:17:19'),(4,2,'SwishActive3.3/3.0','SwishActive 3.3mmD / 3.0mmD','',2,'swishactive-33mmd-30mmd','SwishActive 3.3mmD / 3.0mmD','SwishActive 3.3mmD / 3.0mmD','SwishActive 3.3mmD / 3.0mmD','08:22:59','08:25:55'),(5,2,'SwishActive4.1/3.4','SwishActive 4.1mmD / 3.4mmD','',3,'swishactive-41mmd-34mmd','SwishActive 4.1mmD / 3.4mmD','SwishActive 4.1mmD / 3.4mmD','SwishActive 4.1mmD / 3.4mmD','08:25:34','08:25:34'),(6,2,'SwishActive4.8/3.4','SwishActive 4.8mmD / 3.4mmD','',4,'swishactive-48mmd-34mmd','SwishActive 4.8mmD / 3.4mmD','SwishActive 4.8mmD / 3.4mmD','SwishActive 4.8mmD / 3.4mmD','08:31:50','08:31:50');

/*Table structure for table `ivo_files` */

DROP TABLE IF EXISTS `ivo_files`;

CREATE TABLE `ivo_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `metadata` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_files` */

insert  into `ivo_files`(`id`,`filename`,`type`,`size`,`metadata`,`created_at`) values (1,'slide_magento-Swish-Launch.png','image/png','480.54','{\"file_name\":\"slide_magento-Swish-Launch.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/slide_magento-Swish-Launch.png\",\"raw_name\":\"slide_magento-Swish-Launch\",\"orig_name\":\"slide_magento-Swish-Launch.png\",\"client_name\":\"slide_magento-Swish-Launch.png\",\"file_ext\":\".png\",\"file_size\":480.54,\"is_image\":true,\"image_width\":1080,\"image_height\":465,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"1080\\\" height=\\\"465\\\"\"}','2015-09-11 08:15:59'),(2,'943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png','image/png','559.73','{\"file_name\":\"943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png\",\"raw_name\":\"943312_3.0mmd-body_3.3mmd-platform_1_1_1_1\",\"orig_name\":\"943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png\",\"client_name\":\"943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png\",\"file_ext\":\".png\",\"file_size\":559.73,\"is_image\":true,\"image_width\":650,\"image_height\":650,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"650\\\" height=\\\"650\\\"\"}','2015-09-11 08:22:35'),(3,'944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png','image/png','561.22','{\"file_name\":\"944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png\",\"raw_name\":\"944112_4.1mmd-body_3.4mmd-platform_1_1_2_1\",\"orig_name\":\"944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png\",\"client_name\":\"944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png\",\"file_ext\":\".png\",\"file_size\":561.22,\"is_image\":true,\"image_width\":650,\"image_height\":650,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"650\\\" height=\\\"650\\\"\"}','2015-09-11 08:25:19'),(4,'944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png','image/png','551.39','{\"file_name\":\"944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png\",\"raw_name\":\"944812_4.8mmd-body_3.4mmd-platform_1_1_1_1\",\"orig_name\":\"944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png\",\"client_name\":\"944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png\",\"file_ext\":\".png\",\"file_size\":551.39,\"is_image\":true,\"image_width\":650,\"image_height\":650,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"650\\\" height=\\\"650\\\"\"}','2015-09-11 08:31:29');

/*Table structure for table `ivo_pages` */

DROP TABLE IF EXISTS `ivo_pages`;

CREATE TABLE `ivo_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `content` text,
  `excerpt` text,
  `parent_id` int(11) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keyword` varchar(255) DEFAULT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `gallery` text,
  `created_at` time DEFAULT NULL,
  `updated_at` time DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_pages` */

insert  into `ivo_pages`(`id`,`title`,`slug`,`content`,`excerpt`,`parent_id`,`seo_title`,`seo_description`,`seo_keyword`,`featured_image`,`gallery`,`created_at`,`updated_at`,`user_id`) values (1,'About Us','about-us','<p>Ivodent Lab, established in 1997 by experienced dentist, Indra Guizot and located in Denpasar Bali. In the beginning year, there were only two assistants supporting Guizot to operate his lab, these two staffs were focused to support his clinic operation.</p>\r\n<p>Ivodent Lab with its motto one step ahead, always tries to be the leader in dental technic\'s matters. Less then a years running its operation. Ivodent Lab was visited by expert Germany dentist whom introduced porcelain IPS Classic and Targis - Vectris and continuously.</p>\r\n<p><img style=\"max-width: 100%;\" src=\"[base_url]assets/uploads/images/slide2.jpg\" alt=\"\" /></p>\r\n<p>Ivodent Lab builds relationship with a Dental Technician from Europe to ensure its Dental Technologies always one step ahead.</p>','Ivodent Lab, established in 1997 by experienced dentist, Indra Guizot and located in Denpasar Bali. In the beginning year, there were only two assistants supporting Guizot to operate his lab, these two staffs were focused to support his clinic operation.',0,'About Us','Ivodent Lab, established in 1997 by experienced dentist, Indra Guizot and located in Denpasar Bali. In the beginning year, there were only two assistants supporting Guizot to operate his lab, these two staffs were focused to support his clinic operation.','','4','','11:07:46','04:59:45',1);

/*Table structure for table `ivo_product_images` */

DROP TABLE IF EXISTS `ivo_product_images`;

CREATE TABLE `ivo_product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_product_images` */

insert  into `ivo_product_images`(`id`,`product_id`,`file_id`) values (1,1,2);

/*Table structure for table `ivo_products` */

DROP TABLE IF EXISTS `ivo_products`;

CREATE TABLE `ivo_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '1',
  `category_id` int(10) unsigned DEFAULT NULL,
  `description` text,
  `excerpt` text,
  `price` double DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  `note` text,
  `status` int(1) DEFAULT '1',
  `slug` varchar(255) DEFAULT NULL,
  `inventory_tracking` int(1) DEFAULT '1',
  `quantity` int(1) DEFAULT '0',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keyword` varchar(255) DEFAULT NULL,
  `created_at` time NOT NULL,
  `updated_at` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_products` */

insert  into `ivo_products`(`id`,`sku`,`barcode`,`name`,`sequence`,`category_id`,`description`,`excerpt`,`price`,`sale_price`,`note`,`status`,`slug`,`inventory_tracking`,`quantity`,`seo_title`,`seo_description`,`seo_keyword`,`created_at`,`updated_at`) values (1,'943308','943308','SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform',0,NULL,'<p>Product Code : 943308<br />Surface : SBM Blasted<br />Body Diameter : 3.3mmD<br />Platform/Component Diameter : 3.0mmD<br />Length : 8.0mmL<br />Product Label / Platform</p>','<p>SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform</p>',585,150,'\"Competitor\'s Price\": Bone Level Impant -$435, Closure screw- $40, Healing Abutment- $60, Transfer- $50.',1,'swishactive-implant-33mmd-x-8mml-sbm-30mmd-platform',1,10,'SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform','SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform','SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform','08:38:54','09:09:24');

/*Table structure for table `ivo_products_rel_categories` */

DROP TABLE IF EXISTS `ivo_products_rel_categories`;

CREATE TABLE `ivo_products_rel_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_products_rel_categories` */

insert  into `ivo_products_rel_categories`(`id`,`product_id`,`category_id`) values (1,1,4);

/*Table structure for table `ivo_sessions` */

DROP TABLE IF EXISTS `ivo_sessions`;

CREATE TABLE `ivo_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ivo_sessions` */

insert  into `ivo_sessions`(`id`,`ip_address`,`timestamp`,`data`) values ('0198d3e0c9b0f86b28c1e9317f18b8e976b92a75','::1',1441956036,'__ci_last_regenerate|i:1441956035;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('10c97b949b589050381b948eda09e4c7aa0d67e0','::1',1441956842,'__ci_last_regenerate|i:1441956549;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('1c99dba7b11c06bef255c8701ae5adb38f9a4ba7','::1',1441952784,'__ci_last_regenerate|i:1441952546;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('2502b661d8695256073fda12d5bcb7ee5ca4dad0','::1',1441953971,'__ci_last_regenerate|i:1441953755;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('34b3083f2415797107304ab4a51384f3b40bee80','::1',1441952459,'__ci_last_regenerate|i:1441952193;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('38e8786a9ac99b14bb1854cfc1ed63360b14c30d','::1',1442196110,'__ci_last_regenerate|i:1442196095;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('6d6be0807da77de3ca00c453cdbe19b2e673db23','::1',1441955366,'__ci_last_regenerate|i:1441955166;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('6dfb63d380f4d8d4a610442184017c9bbabf8cb1','::1',1441953257,'__ci_last_regenerate|i:1441953044;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('82edca5fdb740e3af99f353ffcd72f1adc9f5f8d','::1',1441958617,'__ci_last_regenerate|i:1441958334;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('83b4fe95e9d19fb60772c36a1260147ff987a0eb','::1',1441953614,'__ci_last_regenerate|i:1441953366;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('8e4b30a69fa9b60025658ab5a201b11688e953ec','::1',1441957491,'__ci_last_regenerate|i:1441957234;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('b7e943e8eab8f14ec8ffbee8c75a34c2c39bb29a','::1',1441957581,'__ci_last_regenerate|i:1441957581;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('c3abbbd7849baa82b51c9999d8c1fc263c96551b','::1',1441952169,'__ci_last_regenerate|i:1441951891;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('c70a4555e58a033bd5ebb8ba008b23396686e701','::1',1441958833,'__ci_last_regenerate|i:1441958658;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('c9875336e095d625ceb7322a785db18578869c6e','::1',1441957127,'__ci_last_regenerate|i:1441956907;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('c9c374d9504e439756e1934f857706de7119b660','::1',1441959359,'__ci_last_regenerate|i:1441959073;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('eb3b03070b9dc0b2aae261904827f6f15e2526c2','::1',1442195645,'__ci_last_regenerate|i:1442195529;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}');

/*Table structure for table `ivo_settings` */

DROP TABLE IF EXISTS `ivo_settings`;

CREATE TABLE `ivo_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_settings` */

insert  into `ivo_settings`(`id`,`label`,`key`,`value`) values (1,'Site Name','site_name','Ivodent'),(2,'Tagline','site_tagline','Ivodent Dental Supplies - Bali Dental Clinic - Dental Lab and Treatment In Bali - Bali Dentist'),(3,'Date Format','date_format','j F Y'),(4,'Time Format','time_format','g:i a'),(5,'Currency','currency','USD'),(6,'Currency Position','currency_position','left_space'),(7,'Thousand Separator','thousand_separator',','),(8,'Decimal Separator','decimal_separator','.'),(9,'Number of Decimals','number_decimal','0'),(10,'Image Thumnail Size','image_thumbnail','150x150'),(11,'Thumnail Crop','image_thumbnail_crop','1'),(12,'Image Medium Size','image_medium','300x300'),(13,'Image Medium Crop','image_medium_crop','0'),(14,'Image Large Size','image_large','600x600'),(15,'Image Large Crop','image_large_crop','0'),(34,'Image 1 ( 1140px x 420px )','slide_image1','1'),(35,'Title 1','slide_title1','SWISH SYSTEM'),(36,'Description 1','slide_description1','Conical Hex Bone-level Implants'),(37,'Image 2 ( 1140px x 420px )','slide_image2',''),(38,'Title 2','slide_title2',''),(39,'Description 2','slide_description2',''),(40,'Image 3 ( 1140px x 420px )','slide_image3',''),(41,'Title 3','slide_title3',''),(42,'Description 3','slide_description3',''),(43,'Image 4 ( 1140px x 420px )','slide_image4',''),(44,'Title 4','slide_title4',''),(45,'Description 4','slide_description4',''),(46,'Image 5 ( 1140px x 420px )','slide_image5',''),(47,'Title 5','slide_title5',''),(48,'Description 5','slide_description5',''),(49,'Image 6 ( 1140px x 420px )','slide_image6',''),(50,'Title 6','slide_title6',''),(51,'Description 6','slide_description6','');

/*Table structure for table `ivo_user_addresses` */

DROP TABLE IF EXISTS `ivo_user_addresses`;

CREATE TABLE `ivo_user_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `post_code` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT 'Indonesia',
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `default` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ivo_user_addresses` */

/*Table structure for table `ivo_users` */

DROP TABLE IF EXISTS `ivo_users`;

CREATE TABLE `ivo_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'member',
  `password` text,
  `token` text,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_users` */

insert  into `ivo_users`(`id`,`username`,`email`,`role`,`password`,`token`,`timestamp`) values (1,'tirta','tirta@balibagus.com','admin','nWvPTa+4ltq2tJdSPe/rGKnHkaX3fJFP7OLLByGPw7YXOnjNtVZvNQbNHhNsXahGagw4ke65lCVpJVMAn/TIVA==','0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==','2015-09-14 09:54:05'),(2,'gun','gun@balibagus.com','admin','/6JrI7TSoXXwStQ6Hfv8OIOk90X74rAaLu7jCJK491bfv0EC4ZT+PVcRjNliqW4XdnQSJRaYD7say8oX+T23+A==',NULL,'2015-08-28 14:47:13');

/*Table structure for table `ivo_varians` */

DROP TABLE IF EXISTS `ivo_varians`;

CREATE TABLE `ivo_varians` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `varian1_key` varchar(255) DEFAULT NULL,
  `varian1_value` varchar(255) DEFAULT NULL,
  `varian2_key` varchar(255) DEFAULT NULL,
  `varian2_value` varchar(255) DEFAULT NULL,
  `varian3_key` varchar(255) DEFAULT NULL,
  `varian3_value` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  `picture` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ivo_varians` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
