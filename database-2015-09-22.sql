/*
SQLyog Ultimate v8.32 
MySQL - 5.6.14 : Database - ivodent
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ivo_categories` */

DROP TABLE IF EXISTS `ivo_categories`;

CREATE TABLE `ivo_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `featured_image` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keyword` varchar(255) DEFAULT NULL,
  `created_at` time DEFAULT NULL,
  `updated_at` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_categories` */

insert  into `ivo_categories`(`id`,`parent_id`,`code`,`name`,`description`,`featured_image`,`slug`,`seo_title`,`seo_description`,`seo_keyword`,`created_at`,`updated_at`) values (1,0,'IMPLANS','Implants by Sistem','',0,'implants-by-sistem','Implants by Sistem','Implants by Sistem','Implants by Sistem','08:13:48','08:13:48'),(2,3,'SwishActive','SwishActive','',1,'swishactive','SwishActive','SwishActive','SwishActive','08:16:33','08:17:31'),(3,1,'SwishSystem','Swish System','',1,'swish-system','Swish System','Swish System','Swish System','08:17:19','08:17:19'),(4,2,'SwishActive3.3/3.0','3.3mmD / 3.0mmD','',2,'33mmd-30mmd','SwishActive 3.3mmD / 3.0mmD','SwishActive 3.3mmD / 3.0mmD','SwishActive 3.3mmD / 3.0mmD','08:22:59','09:52:08'),(5,2,'SwishActive4.1/3.4','4.1mmD / 3.4mmD','',3,'41mmd-34mmd','SwishActive 4.1mmD / 3.4mmD','SwishActive 4.1mmD / 3.4mmD','SwishActive 4.1mmD / 3.4mmD','08:25:34','09:52:13'),(6,2,'SwishActive4.8/3.4','4.8mmD / 3.4mmD','',4,'swishactive-48mmd-34mmd','SwishActive 4.8mmD / 3.4mmD','SwishActive 4.8mmD / 3.4mmD','SwishActive 4.8mmD / 3.4mmD','08:31:50','09:52:11');

/*Table structure for table `ivo_files` */

DROP TABLE IF EXISTS `ivo_files`;

CREATE TABLE `ivo_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `metadata` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_files` */

insert  into `ivo_files`(`id`,`filename`,`type`,`size`,`metadata`,`created_at`) values (1,'slide_magento-Swish-Launch.png','image/png','480.54','{\"file_name\":\"slide_magento-Swish-Launch.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/slide_magento-Swish-Launch.png\",\"raw_name\":\"slide_magento-Swish-Launch\",\"orig_name\":\"slide_magento-Swish-Launch.png\",\"client_name\":\"slide_magento-Swish-Launch.png\",\"file_ext\":\".png\",\"file_size\":480.54,\"is_image\":true,\"image_width\":1080,\"image_height\":465,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"1080\\\" height=\\\"465\\\"\"}','2015-09-11 08:15:59'),(2,'943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png','image/png','559.73','{\"file_name\":\"943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png\",\"raw_name\":\"943312_3.0mmd-body_3.3mmd-platform_1_1_1_1\",\"orig_name\":\"943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png\",\"client_name\":\"943312_3.0mmd-body_3.3mmd-platform_1_1_1_1.png\",\"file_ext\":\".png\",\"file_size\":559.73,\"is_image\":true,\"image_width\":650,\"image_height\":650,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"650\\\" height=\\\"650\\\"\"}','2015-09-11 08:22:35'),(3,'944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png','image/png','561.22','{\"file_name\":\"944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png\",\"raw_name\":\"944112_4.1mmd-body_3.4mmd-platform_1_1_2_1\",\"orig_name\":\"944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png\",\"client_name\":\"944112_4.1mmd-body_3.4mmd-platform_1_1_2_1.png\",\"file_ext\":\".png\",\"file_size\":561.22,\"is_image\":true,\"image_width\":650,\"image_height\":650,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"650\\\" height=\\\"650\\\"\"}','2015-09-11 08:25:19'),(4,'944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png','image/png','551.39','{\"file_name\":\"944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png\",\"raw_name\":\"944812_4.8mmd-body_3.4mmd-platform_1_1_1_1\",\"orig_name\":\"944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png\",\"client_name\":\"944812_4.8mmd-body_3.4mmd-platform_1_1_1_1.png\",\"file_ext\":\".png\",\"file_size\":551.39,\"is_image\":true,\"image_width\":650,\"image_height\":650,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"650\\\" height=\\\"650\\\"\"}','2015-09-11 08:31:29'),(5,'943308_b.png','image/png','306.83','{\"file_name\":\"943308_b.png\",\"file_type\":\"image\\/png\",\"file_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/\",\"full_path\":\"D:\\/xampp\\/htdocs\\/ivodent\\/assets\\/uploads\\/images\\/943308_b.png\",\"raw_name\":\"943308_b\",\"orig_name\":\"943308_b.png\",\"client_name\":\"943308_b.png\",\"file_ext\":\".png\",\"file_size\":306.83,\"is_image\":true,\"image_width\":650,\"image_height\":650,\"image_type\":\"png\",\"image_size_str\":\"width=\\\"650\\\" height=\\\"650\\\"\"}','2015-09-16 05:40:17');

/*Table structure for table `ivo_pages` */

DROP TABLE IF EXISTS `ivo_pages`;

CREATE TABLE `ivo_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `content` text,
  `excerpt` text,
  `parent_id` int(11) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keyword` varchar(255) DEFAULT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `gallery` text,
  `created_at` time DEFAULT NULL,
  `updated_at` time DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_pages` */

insert  into `ivo_pages`(`id`,`title`,`slug`,`content`,`excerpt`,`parent_id`,`seo_title`,`seo_description`,`seo_keyword`,`featured_image`,`gallery`,`created_at`,`updated_at`,`user_id`) values (1,'About Us','about-us','<p>Ivodent Lab, established in 1997 by experienced dentist, Indra Guizot and located in Denpasar Bali. In the beginning year, there were only two assistants supporting Guizot to operate his lab, these two staffs were focused to support his clinic operation.</p>\r\n<p>Ivodent Lab with its motto one step ahead, always tries to be the leader in dental technic\'s matters. Less then a years running its operation. Ivodent Lab was visited by expert Germany dentist whom introduced porcelain IPS Classic and Targis - Vectris and continuously.</p>\r\n<p><img style=\"max-width: 100%;\" src=\"[base_url]assets/uploads/images/slide2.jpg\" alt=\"\" /></p>\r\n<p>Ivodent Lab builds relationship with a Dental Technician from Europe to ensure its Dental Technologies always one step ahead.</p>','Ivodent Lab, established in 1997 by experienced dentist, Indra Guizot and located in Denpasar Bali. In the beginning year, there were only two assistants supporting Guizot to operate his lab, these two staffs were focused to support his clinic operation.',0,'About Us','Ivodent Lab, established in 1997 by experienced dentist, Indra Guizot and located in Denpasar Bali. In the beginning year, there were only two assistants supporting Guizot to operate his lab, these two staffs were focused to support his clinic operation.','','4','','11:07:46','04:59:45',1);

/*Table structure for table `ivo_product_images` */

DROP TABLE IF EXISTS `ivo_product_images`;

CREATE TABLE `ivo_product_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `file_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_product_images` */

insert  into `ivo_product_images`(`id`,`product_id`,`file_id`) values (1,1,2),(2,1,5);

/*Table structure for table `ivo_products` */

DROP TABLE IF EXISTS `ivo_products`;

CREATE TABLE `ivo_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT '1',
  `category_id` int(10) unsigned DEFAULT NULL,
  `description` text,
  `excerpt` text,
  `price` double DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  `note` text,
  `status` int(1) DEFAULT '1',
  `slug` varchar(255) DEFAULT NULL,
  `inventory_tracking` int(1) DEFAULT '1',
  `quantity` int(1) DEFAULT '0',
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` varchar(255) DEFAULT NULL,
  `seo_keyword` varchar(255) DEFAULT NULL,
  `created_at` time NOT NULL,
  `updated_at` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_products` */

insert  into `ivo_products`(`id`,`sku`,`barcode`,`name`,`sequence`,`category_id`,`description`,`excerpt`,`price`,`sale_price`,`note`,`status`,`slug`,`inventory_tracking`,`quantity`,`seo_title`,`seo_description`,`seo_keyword`,`created_at`,`updated_at`) values (1,'943308','943308','SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform',0,NULL,'<p>Product Code : 943308<br />Surface : SBM Blasted<br />Body Diameter : 3.3mmD<br />Platform/Component Diameter : 3.0mmD<br />Length : 8.0mmL<br />Product Label / Platform</p>','<p>SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform</p>',585,150,'\"Competitor\'s Price\": Bone Level Impant -$435, Closure screw- $40, Healing Abutment- $60, Transfer- $50.',1,'swishactive-implant-33mmd-x-8mml-sbm-30mmd-platform',1,10,'SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform','SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform','SwishActive Implant 3.3mmD x 8mmL SBM: 3.0mmD Platform','08:38:54','06:06:12');

/*Table structure for table `ivo_products_rel_categories` */

DROP TABLE IF EXISTS `ivo_products_rel_categories`;

CREATE TABLE `ivo_products_rel_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_products_rel_categories` */

insert  into `ivo_products_rel_categories`(`id`,`product_id`,`category_id`) values (1,1,4),(2,1,1),(3,1,3),(4,1,2),(5,1,5),(6,1,6);

/*Table structure for table `ivo_sessions` */

DROP TABLE IF EXISTS `ivo_sessions`;

CREATE TABLE `ivo_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ivo_sessions` */

insert  into `ivo_sessions`(`id`,`ip_address`,`timestamp`,`data`) values ('0007f9a59623899e36f9c1a89172b9f3bc5b2702','::1',1442891893,'__ci_last_regenerate|i:1442891849;'),('0198d3e0c9b0f86b28c1e9317f18b8e976b92a75','::1',1441956036,'__ci_last_regenerate|i:1441956035;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('0453966bd517f874b882ed91ed2f1bb9140b0cec','::1',1442816433,'__ci_last_regenerate|i:1442816134;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('04c70bd9fdb95357c937ce3e38017877eede59ee','::1',1442579839,'__ci_last_regenerate|i:1442579839;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('09c7a190f3b5fe2ae71ad44189403a4d3b5bc79b','::1',1442478039,'__ci_last_regenerate|i:1442478013;'),('0b5c484c39685a9b89f4a59fb6e8e0843a834d32','::1',1442816782,'__ci_last_regenerate|i:1442816503;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('0d2e6c25b315edb4c7cc67d55db457844a78ade3','::1',1442888721,'__ci_last_regenerate|i:1442888465;auth_user|a:1:{s:5:\"token\";s:88:\"wRCkHILdX8fT5JAHgPaQhC6ynvEalgLpSrjcmpbr8S5iqyYzDHqgi9l6H+hcywapD+E6uytnK0nXCVIjMscvvA==\";}'),('10c97b949b589050381b948eda09e4c7aa0d67e0','::1',1441956842,'__ci_last_regenerate|i:1441956549;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('10f87c06a1deac96180e0177f3586607747961f4','::1',1442477119,'__ci_last_regenerate|i:1442477022;'),('12086010e8fa75ec7664c11957b7366574137c3e','::1',1442904998,'__ci_last_regenerate|i:1442904993;auth_user|a:1:{s:5:\"token\";s:88:\"UUD6yxZMIyLXZJ0RvTofHc9KSd2p9MXIsC8P7xytT1uqYaUzW6vju5gayiNEMzVaBTYWBeO1ytUZBvRpC0Kpog==\";}success|s:33:\"This product is in your wishlist.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('1409f6917a5d2159c7396390d1e00d82e27d5aa9','::1',1442891241,'__ci_last_regenerate|i:1442891159;'),('15fe52d4c9ff0826ad32886a164b73403eaf22f6','::1',1442376080,'__ci_last_regenerate|i:1442375809;auth_user|a:1:{s:5:\"token\";s:88:\"pj/I8zDdZYPj4p2mJUNsIUK5rc4Q5kxhOP8ELDL3QHnKGFPQbpJuD+Zwe2/InN4Aa5korGRnemn698GGYtjIeg==\";}'),('1a811e99cade2f2bd78f83a87ad7b00272a44434','::1',1442814244,'__ci_last_regenerate|i:1442814240;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('1c99dba7b11c06bef255c8701ae5adb38f9a4ba7','::1',1441952784,'__ci_last_regenerate|i:1441952546;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('1d53b6db99c6d3701de9eca03c1bfc1b4314ad7a','::1',1442579781,'__ci_last_regenerate|i:1442579488;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('1e70fd7145080245915c0969076ed57657828832','::1',1442809437,'__ci_last_regenerate|i:1442809301;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('22684efedf526bba49eaa4e1eb3fb34af46457ce','::1',1442302798,'__ci_last_regenerate|i:1442302719;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}success|s:26:\"Category has been updated.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('23c9fae7b983fbfe6c542e02295c71cc79c1af92','::1',1442905457,'__ci_last_regenerate|i:1442905382;auth_user|a:1:{s:5:\"token\";s:88:\"UUD6yxZMIyLXZJ0RvTofHc9KSd2p9MXIsC8P7xytT1uqYaUzW6vju5gayiNEMzVaBTYWBeO1ytUZBvRpC0Kpog==\";}success|s:33:\"This product is in your wishlist.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('24b6493a67842aad6b69d965ab1bb0cb181b7645','::1',1442375665,'__ci_last_regenerate|i:1442375392;auth_user|a:1:{s:5:\"token\";s:88:\"pj/I8zDdZYPj4p2mJUNsIUK5rc4Q5kxhOP8ELDL3QHnKGFPQbpJuD+Zwe2/InN4Aa5korGRnemn698GGYtjIeg==\";}'),('2502b661d8695256073fda12d5bcb7ee5ca4dad0','::1',1441953971,'__ci_last_regenerate|i:1441953755;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('26a067cac99492c4d1b7597f0a6231210e231dfb','::1',1442298167,'__ci_last_regenerate|i:1442298167;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('2c0340e0640562f84d1b5bb20984458b0a227a3e','::1',1442199874,'__ci_last_regenerate|i:1442199874;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('2d42fb7e6a0384a3d60e40489f08422a16a4b6ea','::1',1442581012,'__ci_last_regenerate|i:1442581003;auth_user|a:1:{s:5:\"token\";s:88:\"FjltKEYPjuqFC0BP2T8gOiyIqKP4pWNhheyv5uhxkppH7XroPG9cFsfja6tAHxE8VjTeD87R0m7BCNAsDZ06xw==\";}'),('2dec28aa8f45d3b86277bd3d69aebbfb3514d591','::1',1442577768,'__ci_last_regenerate|i:1442577483;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('34b3083f2415797107304ab4a51384f3b40bee80','::1',1441952459,'__ci_last_regenerate|i:1441952193;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('35eb26ca88c42b1e1c522ca4919fb89e31d7cf23','::1',1442464779,'__ci_last_regenerate|i:1442464674;auth_user|a:1:{s:5:\"token\";s:88:\"wBmsOh1rwfr9cG66C1vsgMwoWWFeuOFWpYF+7li02bS8pUXUUBt7x9qkeW2Ja3Xogvgwbt8v7NCTwuontOgDGw==\";}'),('38e8786a9ac99b14bb1854cfc1ed63360b14c30d','::1',1442196110,'__ci_last_regenerate|i:1442196095;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('39508d65428cf3829db0feeadd90cef4c104321a','::1',1442297590,'__ci_last_regenerate|i:1442297354;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('3c1c6668e360e85f4e7d7c7907a6f20216627096','::1',1442473923,'__ci_last_regenerate|i:1442473628;'),('42a3f67e1bf8ed299616f222d8775b9baab46e48','::1',1442375368,'__ci_last_regenerate|i:1442375086;auth_user|a:1:{s:5:\"token\";s:88:\"pj/I8zDdZYPj4p2mJUNsIUK5rc4Q5kxhOP8ELDL3QHnKGFPQbpJuD+Zwe2/InN4Aa5korGRnemn698GGYtjIeg==\";}'),('44639572b1bbfc58049e794a6d8cc95324880707','::1',1442473022,'__ci_last_regenerate|i:1442472954;'),('49b66e4d494c1fec75437ffe77099e6263e81b61','::1',1442469825,'__ci_last_regenerate|i:1442469825;'),('4cd435f5f7fe6970870591bc3a075e5ef61508f4','::1',1442203358,'__ci_last_regenerate|i:1442203114;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('4de7285b74474b83e049350a27e16f8998a3c7b2','::1',1442579021,'__ci_last_regenerate|i:1442578818;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('50c443f7d861496d89aecf9c4ddd23804a4d5607','::1',1442888996,'__ci_last_regenerate|i:1442888774;auth_user|a:1:{s:5:\"token\";s:88:\"wRCkHILdX8fT5JAHgPaQhC6ynvEalgLpSrjcmpbr8S5iqyYzDHqgi9l6H+hcywapD+E6uytnK0nXCVIjMscvvA==\";}'),('51d2a945976a2c24a1add1082dcb142a7c8043a5','::1',1442304360,'__ci_last_regenerate|i:1442304094;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('528ad5d75eb98806d09af2b7af4c57553cf6d6d9','::1',1442203744,'__ci_last_regenerate|i:1442203468;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('546d35ee4fe4a0b632a91073bd6e3d3da701a371','::1',1442464655,'__ci_last_regenerate|i:1442464359;auth_user|a:1:{s:5:\"token\";s:88:\"wBmsOh1rwfr9cG66C1vsgMwoWWFeuOFWpYF+7li02bS8pUXUUBt7x9qkeW2Ja3Xogvgwbt8v7NCTwuontOgDGw==\";}'),('54f51613411885b0f603657f985a593591adfcdc','::1',1442375063,'__ci_last_regenerate|i:1442374767;auth_user|a:1:{s:5:\"token\";s:88:\"pj/I8zDdZYPj4p2mJUNsIUK5rc4Q5kxhOP8ELDL3QHnKGFPQbpJuD+Zwe2/InN4Aa5korGRnemn698GGYtjIeg==\";}'),('55350cca5a37de502c0d183ff2b3958b58dd5f89','::1',1442577142,'__ci_last_regenerate|i:1442577029;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('55f4d3c4e55c267acd5d3ca12c500cd1ab258089','::1',1442303655,'__ci_last_regenerate|i:1442303400;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('58a0496034c1bef1e58de3c6cf7d644f6a81c97c','::1',1442576041,'__ci_last_regenerate|i:1442575747;'),('5a21d2e3119a801dc53ceaaded949aa2076e9bdb','::1',1442576491,'__ci_last_regenerate|i:1442576287;'),('5d5abdea774e5ecbbd4be2a6d561742a75cd9e53','::1',1442813456,'__ci_last_regenerate|i:1442813343;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('61be1bd6cc09b792e98d015f4d1b98d4299f1af0','::1',1442889890,'__ci_last_regenerate|i:1442889888;'),('65aa3ce3e9a34c5f1e64d8eaa119458aca838122','::1',1442577960,'__ci_last_regenerate|i:1442577811;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('6ac5f80ce4ff93c366fc3c09bda265cc16f412e7','::1',1442469808,'__ci_last_regenerate|i:1442469778;auth_user|a:1:{s:5:\"token\";s:88:\"wBmsOh1rwfr9cG66C1vsgMwoWWFeuOFWpYF+7li02bS8pUXUUBt7x9qkeW2Ja3Xogvgwbt8v7NCTwuontOgDGw==\";}'),('6d6be0807da77de3ca00c453cdbe19b2e673db23','::1',1441955366,'__ci_last_regenerate|i:1441955166;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('6dfb63d380f4d8d4a610442184017c9bbabf8cb1','::1',1441953257,'__ci_last_regenerate|i:1441953044;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('70871972690a753dc9b2c46c76245cdcfda5f79e','::1',1442291836,'__ci_last_regenerate|i:1442291835;'),('7087601a3ef57ee2b8d0ddd97696398a2af318f0','::1',1442887166,'__ci_last_regenerate|i:1442886985;auth_user|a:1:{s:5:\"token\";s:88:\"wRCkHILdX8fT5JAHgPaQhC6ynvEalgLpSrjcmpbr8S5iqyYzDHqgi9l6H+hcywapD+E6uytnK0nXCVIjMscvvA==\";}'),('70bac9eaf29de9231c9d3f03ecb08e90462296c0','::1',1442202882,'__ci_last_regenerate|i:1442202799;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('735284f2899fe6996018329ea21fb239e91338cf','::1',1442373843,'__ci_last_regenerate|i:1442373774;'),('76d6f95107159d4c4ccb338a1a725e5a225c8d9f','::1',1442580994,'__ci_last_regenerate|i:1442580869;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('76e7de0169344a2b25a28e8191bc44f75ab2e651','::1',1442464774,'__ci_last_regenerate|i:1442464662;'),('7a6e14f8b621be518b3264b63fac0c3dabb3b8dd','::1',1442893369,'__ci_last_regenerate|i:1442893187;'),('7c9d0166e7bca701e8cb5972996633e334e9d959','::1',1442305957,'__ci_last_regenerate|i:1442305663;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('82edca5fdb740e3af99f353ffcd72f1adc9f5f8d','::1',1441958617,'__ci_last_regenerate|i:1441958334;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('8374455c9e378242308846788950a6a28ccde3e7','::1',1442580467,'__ci_last_regenerate|i:1442580185;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('83b4fe95e9d19fb60772c36a1260147ff987a0eb','::1',1441953614,'__ci_last_regenerate|i:1441953366;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('83e271558886365d2a7714c7fa369beab8cd8eaa','::1',1442473063,'__ci_last_regenerate|i:1442473018;auth_user|a:1:{s:5:\"token\";s:88:\"wBmsOh1rwfr9cG66C1vsgMwoWWFeuOFWpYF+7li02bS8pUXUUBt7x9qkeW2Ja3Xogvgwbt8v7NCTwuontOgDGw==\";}'),('87f76d2e3a0adf0c0a78a89518828b36a13a7d64','::1',1442473536,'__ci_last_regenerate|i:1442473401;'),('88bc226024a735e514991d1a1d7d0f5a8e300afa','::1',1442306830,'__ci_last_regenerate|i:1442306615;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('8bfdb4985da232cc410137c64ca7f077166e9541','::1',1442579331,'__ci_last_regenerate|i:1442579153;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('8e4b30a69fa9b60025658ab5a201b11688e953ec','::1',1441957491,'__ci_last_regenerate|i:1441957234;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('8ff6b2f8f3431574f3afdeb4daa79f9522032393','::1',1442809709,'__ci_last_regenerate|i:1442809694;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}success|s:30:\"Your account has been updated.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"old\";}'),('926640e66700feb144f6579ccc4b381f27c32776','::1',1442204620,'__ci_last_regenerate|i:1442204383;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('9331da74f564132ff2c3e38a84870dd374c6df5a','::1',1442906907,'__ci_last_regenerate|i:1442906676;auth_user|a:1:{s:5:\"token\";s:88:\"UUD6yxZMIyLXZJ0RvTofHc9KSd2p9MXIsC8P7xytT1uqYaUzW6vju5gayiNEMzVaBTYWBeO1ytUZBvRpC0Kpog==\";}'),('94feaf926d96eece0baf1996ade8f68d04c93756','::1',1442455882,'__ci_last_regenerate|i:1442455862;'),('961bd0826fcfcb5fa957bdceedd07bb5c29abad7','::1',1442582883,'__ci_last_regenerate|i:1442582739;auth_user|a:1:{s:5:\"token\";s:88:\"FjltKEYPjuqFC0BP2T8gOiyIqKP4pWNhheyv5uhxkppH7XroPG9cFsfja6tAHxE8VjTeD87R0m7BCNAsDZ06xw==\";}'),('9bc826f413d671cf5258ee1ce7844dc81f0b1e01','::1',1442808965,'__ci_last_regenerate|i:1442808739;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('9c14ad0b8cf5b267cc23415339abe897263172e6','::1',1442304720,'__ci_last_regenerate|i:1442304525;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('9e2a18660f85c5e0c0bd9eb9e3a3ecc3ab86492e','::1',1442303374,'__ci_last_regenerate|i:1442303084;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('9ef0f93d7837d493135d5ea11faee17e966f7ac3','::1',1442474145,'__ci_last_regenerate|i:1442474122;'),('a1630979c64b2bbd41857ee7e0e362eba1c40b18','::1',1442297710,'__ci_last_regenerate|i:1442297701;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('a1a2c0ea436e4374180ff50f375947d6fa4a7e32','::1',1442578688,'__ci_last_regenerate|i:1442578441;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('a37e0bc2745079d9d40c78e004c24d6294a1bbfa','::1',1442576269,'__ci_last_regenerate|i:1442576073;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('a50dddf67a151815c796f14323078c8ba3eba4ea','::1',1442298570,'__ci_last_regenerate|i:1442298527;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('a589f0eb7475e8ef0ef3f150f10363c65e1bcbc2','::1',1442815151,'__ci_last_regenerate|i:1442814872;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('a665ceedffefa9926bd9e1dbd82362f730b1a129','::1',1442807465,'__ci_last_regenerate|i:1442807416;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('a6969bf13e1f3c36cc9600009e7f54723be78342','::1',1442298968,'__ci_last_regenerate|i:1442298968;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('a70c22d3f3b80f00c564e00548bf0306af7f0d64','::1',1442901571,'__ci_last_regenerate|i:1442901447;auth_user|a:1:{s:5:\"token\";s:88:\"UUD6yxZMIyLXZJ0RvTofHc9KSd2p9MXIsC8P7xytT1uqYaUzW6vju5gayiNEMzVaBTYWBeO1ytUZBvRpC0Kpog==\";}'),('a803bcba1ec7e5172e365ac8afdf2c1a82906197','::1',1442201669,'__ci_last_regenerate|i:1442201654;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('ab30bd0f8e52906afe5f31c8736f0c57f20ac9b8','::1',1442885672,'__ci_last_regenerate|i:1442885497;auth_user|a:1:{s:5:\"token\";s:88:\"wRCkHILdX8fT5JAHgPaQhC6ynvEalgLpSrjcmpbr8S5iqyYzDHqgi9l6H+hcywapD+E6uytnK0nXCVIjMscvvA==\";}'),('aff8efd7496c53ddbdfe92f062bf42d9e1b00aca','::1',1442904634,'__ci_last_regenerate|i:1442904623;auth_user|a:1:{s:5:\"token\";s:88:\"UUD6yxZMIyLXZJ0RvTofHc9KSd2p9MXIsC8P7xytT1uqYaUzW6vju5gayiNEMzVaBTYWBeO1ytUZBvRpC0Kpog==\";}success|s:33:\"This product is in your wishlist.\";__ci_vars|a:1:{s:7:\"success\";s:3:\"new\";}'),('b352530791edd374e1baa1ab78100209b208484b','::1',1442304838,'__ci_last_regenerate|i:1442304838;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('b631596598c3f5cbcdb265998c496cb8d2aa554c','::1',1442297114,'__ci_last_regenerate|i:1442296856;'),('b7e943e8eab8f14ec8ffbee8c75a34c2c39bb29a','::1',1441957581,'__ci_last_regenerate|i:1441957581;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('b918ccff3330f148e699a46936447b50d499a488','::1',1442299971,'__ci_last_regenerate|i:1442299723;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('bc9452b7cdd77cc27e540eccff75e5f6c4049f0c','::1',1442817205,'__ci_last_regenerate|i:1442816937;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('bd25fe14d72e21a630d796832cb69677a7423528','::1',1442807360,'__ci_last_regenerate|i:1442807107;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('c3abbbd7849baa82b51c9999d8c1fc263c96551b','::1',1441952169,'__ci_last_regenerate|i:1441951891;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('c7092e2d681724e74028b2921c92c33bd902912c','::1',1442815548,'__ci_last_regenerate|i:1442815248;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('c70a4555e58a033bd5ebb8ba008b23396686e701','::1',1441958833,'__ci_last_regenerate|i:1441958658;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('c9875336e095d625ceb7322a785db18578869c6e','::1',1441957127,'__ci_last_regenerate|i:1441956907;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('c9c374d9504e439756e1934f857706de7119b660','::1',1441959359,'__ci_last_regenerate|i:1441959073;auth_user|a:1:{s:5:\"token\";s:88:\"qeiqWsoBgBi5y1lzYrayrosA5CGPyF0ElcVYJ5Vpu1PYiJU+IdjxcCuoUOBcDNi61YRIu8aNiUZCGJ2HQeXOvQ==\";}'),('cbeabf224106f7337840685b38dfc95877dce8a6','::1',1442390981,'__ci_last_regenerate|i:1442390980;'),('cc83ade1e7da82390cd4e2ebc4e86a538c5cbda3','::1',1442582915,'__ci_last_regenerate|i:1442582737;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('ccf1272cc25b6742ae5bfc3e84f7250ecb5c67dd','::1',1442202144,'__ci_last_regenerate|i:1442202144;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('d0c560fb4f0d1e77eca707bb4a6530e8fb9d5fe1','::1',1442205220,'__ci_last_regenerate|i:1442204914;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('d13c6cb5bbeba85c4b5e76dbed1293c8f93fde0e','::1',1442376466,'__ci_last_regenerate|i:1442376166;auth_user|a:1:{s:5:\"token\";s:88:\"pj/I8zDdZYPj4p2mJUNsIUK5rc4Q5kxhOP8ELDL3QHnKGFPQbpJuD+Zwe2/InN4Aa5korGRnemn698GGYtjIeg==\";}'),('d3ca12ae755de5331225d9d73d0fd66573852b84','::1',1442886938,'__ci_last_regenerate|i:1442886662;auth_user|a:1:{s:5:\"token\";s:88:\"wRCkHILdX8fT5JAHgPaQhC6ynvEalgLpSrjcmpbr8S5iqyYzDHqgi9l6H+hcywapD+E6uytnK0nXCVIjMscvvA==\";}'),('d50200ee822076a0b79b8bac4aba05abcbd697e4','::1',1442304059,'__ci_last_regenerate|i:1442303783;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('d71665e0d213db85a1b27ab1af34d429340fdda3','::1',1442887547,'__ci_last_regenerate|i:1442887289;auth_user|a:1:{s:5:\"token\";s:88:\"wRCkHILdX8fT5JAHgPaQhC6ynvEalgLpSrjcmpbr8S5iqyYzDHqgi9l6H+hcywapD+E6uytnK0nXCVIjMscvvA==\";}'),('d93a6f74ca47343df2ee9c74c793561ac19a259c','::1',1442808066,'__ci_last_regenerate|i:1442808028;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('da7864e8048b97c862a79fdfec5e4693f2a93ea2','::1',1442888282,'__ci_last_regenerate|i:1442888085;auth_user|a:1:{s:5:\"token\";s:88:\"wRCkHILdX8fT5JAHgPaQhC6ynvEalgLpSrjcmpbr8S5iqyYzDHqgi9l6H+hcywapD+E6uytnK0nXCVIjMscvvA==\";}'),('dc31e0718609292995cb56de79b39517c19393d1','::1',1442901020,'__ci_last_regenerate|i:1442900979;auth_user|a:1:{s:5:\"token\";s:88:\"UUD6yxZMIyLXZJ0RvTofHc9KSd2p9MXIsC8P7xytT1uqYaUzW6vju5gayiNEMzVaBTYWBeO1ytUZBvRpC0Kpog==\";}'),('dddc6aa8592d0672d38670696959c349e77b645b','::1',1442815630,'__ci_last_regenerate|i:1442815630;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('e59a8d466448f0840559dd14cb3cff1e032ba42f','::1',1442464071,'__ci_last_regenerate|i:1442464008;auth_user|a:1:{s:5:\"token\";s:88:\"wBmsOh1rwfr9cG66C1vsgMwoWWFeuOFWpYF+7li02bS8pUXUUBt7x9qkeW2Ja3Xogvgwbt8v7NCTwuontOgDGw==\";}'),('e86c4ba98b6e33e79fb80345f37389dff9b671f4','::1',1442806866,'__ci_last_regenerate|i:1442806667;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('e98eaabe676c9187462a5268eb8f1229a4a1f264','::1',1442306564,'__ci_last_regenerate|i:1442306255;auth_user|a:1:{s:5:\"token\";s:88:\"DbXSd6UHuMtWPFwdaqOZkwhdKmLsZtqjm1VIBW5+uDPcI8pk15ADJ3RocNZWDH+d3F5LGG7Jm286Iw2QegleKw==\";}'),('eb3b03070b9dc0b2aae261904827f6f15e2526c2','::1',1442195645,'__ci_last_regenerate|i:1442195529;auth_user|a:1:{s:5:\"token\";s:88:\"0o+d0Z9ctXP2By38A0DA88yECe7tMbNkyz48Z59gyv1bP17kuN5o/aULWv1/F3CSOip6I+ED6STzrCE0IIzgwg==\";}'),('efafa97fc1c3a4f8eefc3ce428f402eecc251f68','::1',1442576551,'__ci_last_regenerate|i:1442576424;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('f33cc8d8497fc1337ff5a96b3efdf88133b90233','::1',1442580849,'__ci_last_regenerate|i:1442580549;auth_user|a:1:{s:5:\"token\";s:88:\"223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==\";}'),('f599f40eb0d6d5e43ba840a3cb9c3eaa93d16f86','::1',1442475343,'__ci_last_regenerate|i:1442475320;'),('f5c288d2e7266db9d531d798fcb83d16d1cbd6c5','::1',1442814560,'__ci_last_regenerate|i:1442814548;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('f6c89f4da6155507db1601c200c142dc4a4b6813','::1',1442372472,'__ci_last_regenerate|i:1442372471;'),('f78eeb4e753d7da7aec0feb247ca65f323dc34a4','::1',1442902140,'__ci_last_regenerate|i:1442902140;auth_user|a:1:{s:5:\"token\";s:88:\"UUD6yxZMIyLXZJ0RvTofHc9KSd2p9MXIsC8P7xytT1uqYaUzW6vju5gayiNEMzVaBTYWBeO1ytUZBvRpC0Kpog==\";}'),('f9cc0a37c0d1b9811baa71d6047b800f5fcc85d2','::1',1442806558,'__ci_last_regenerate|i:1442806262;auth_user|a:1:{s:5:\"token\";s:88:\"J+2vDfm0BMEGyOpMeNkZNFdmLw88zNZWjwOMGYi9eGJXS7IlFzfxPALCSKYBQiYF7oN21hYxPe5U5xEMUPUhAQ==\";}'),('fe8fb7ed013896eb5a4a3ed5f41a86802355bf24','::1',1442376488,'__ci_last_regenerate|i:1442376467;auth_user|a:1:{s:5:\"token\";s:88:\"pj/I8zDdZYPj4p2mJUNsIUK5rc4Q5kxhOP8ELDL3QHnKGFPQbpJuD+Zwe2/InN4Aa5korGRnemn698GGYtjIeg==\";}');

/*Table structure for table `ivo_settings` */

DROP TABLE IF EXISTS `ivo_settings`;

CREATE TABLE `ivo_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_settings` */

insert  into `ivo_settings`(`id`,`label`,`key`,`value`) values (1,'Site Name','site_name','Ivodent'),(2,'Tagline','site_tagline','Ivodent Dental Supplies - Bali Dental Clinic - Dental Lab and Treatment In Bali - Bali Dentist'),(3,'Date Format','date_format','j F Y'),(4,'Time Format','time_format','g:i a'),(5,'Currency','currency','USD'),(6,'Currency Position','currency_position','left_space'),(7,'Thousand Separator','thousand_separator',','),(8,'Decimal Separator','decimal_separator','.'),(9,'Number of Decimals','number_decimal','0'),(10,'Image Thumnail Size','image_thumbnail','150x150'),(11,'Thumnail Crop','image_thumbnail_crop','1'),(12,'Image Medium Size','image_medium','300x300'),(13,'Image Medium Crop','image_medium_crop','0'),(14,'Image Large Size','image_large','600x600'),(15,'Image Large Crop','image_large_crop','0'),(34,'Image 1 ( 1140px x 420px )','slide_image1','1'),(35,'Title 1','slide_title1','SWISH SYSTEM'),(36,'Description 1','slide_description1','Conical Hex Bone-level Implants'),(37,'Image 2 ( 1140px x 420px )','slide_image2',''),(38,'Title 2','slide_title2',''),(39,'Description 2','slide_description2',''),(40,'Image 3 ( 1140px x 420px )','slide_image3',''),(41,'Title 3','slide_title3',''),(42,'Description 3','slide_description3',''),(43,'Image 4 ( 1140px x 420px )','slide_image4',''),(44,'Title 4','slide_title4',''),(45,'Description 4','slide_description4',''),(46,'Image 5 ( 1140px x 420px )','slide_image5',''),(47,'Title 5','slide_title5',''),(48,'Description 5','slide_description5',''),(49,'Image 6 ( 1140px x 420px )','slide_image6',''),(50,'Title 6','slide_title6',''),(51,'Description 6','slide_description6','');

/*Table structure for table `ivo_user_addresses` */

DROP TABLE IF EXISTS `ivo_user_addresses`;

CREATE TABLE `ivo_user_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `post_code` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT 'Indonesia',
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `default` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_user_addresses` */

insert  into `ivo_user_addresses`(`id`,`user_id`,`name`,`company`,`address`,`city`,`post_code`,`province`,`country`,`email`,`phone`,`default`) values (1,7,'Tirta Keniten','Balibagus.com','Jalan Padang Udayana 7','Denpasar','80117','Bali','Indonesia','keniten.tirta@gmail.com','0821115614131',1),(2,7,'Tirta Keniten','','Jalan Gunung Guntur Gang VII no 15B - Padangsambian','Denpasar','80117','Bali','Indonesia','keniten.tirta@gmail.com','0819999913171',NULL);

/*Table structure for table `ivo_users` */

DROP TABLE IF EXISTS `ivo_users`;

CREATE TABLE `ivo_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'member',
  `password` text,
  `token` text,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_users` */

insert  into `ivo_users`(`id`,`username`,`email`,`role`,`password`,`token`,`timestamp`) values (1,'tirta','tirta@balibagus.com','admin','nWvPTa+4ltq2tJdSPe/rGKnHkaX3fJFP7OLLByGPw7YXOnjNtVZvNQbNHhNsXahGagw4ke65lCVpJVMAn/TIVA==','223AiIIe+9rdPi/Vd1144LjZ0XGa8Ube43yzGywMoA5kUcXuR+O8zuUVpVrhmh8nXus4m0kX5ETZ+ZiAP/i/bQ==','2015-09-18 19:36:00'),(2,'gun','gun@balibagus.com','admin','/6JrI7TSoXXwStQ6Hfv8OIOk90X74rAaLu7jCJK491bfv0EC4ZT+PVcRjNliqW4XdnQSJRaYD7say8oX+T23+A==',NULL,'2015-08-28 14:47:13'),(7,'Tirta Keniten','keniten.tirta@gmail.com','member','e+fr+CfPsl+lMi6cveC7VRd5lT3weAbxS/YrnwPFzG+oLXeGPljnbjJA55QCTiqxNycyJ7XepfrM8QQyqcSPOQ==','UUD6yxZMIyLXZJ0RvTofHc9KSd2p9MXIsC8P7xytT1uqYaUzW6vju5gayiNEMzVaBTYWBeO1ytUZBvRpC0Kpog==','2015-09-22 13:50:20');

/*Table structure for table `ivo_varians` */

DROP TABLE IF EXISTS `ivo_varians`;

CREATE TABLE `ivo_varians` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `varian1_key` varchar(255) DEFAULT NULL,
  `varian1_value` varchar(255) DEFAULT NULL,
  `varian2_key` varchar(255) DEFAULT NULL,
  `varian2_value` varchar(255) DEFAULT NULL,
  `varian3_key` varchar(255) DEFAULT NULL,
  `varian3_value` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `sale_price` double DEFAULT NULL,
  `picture` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ivo_varians` */

/*Table structure for table `ivo_wishes` */

DROP TABLE IF EXISTS `ivo_wishes`;

CREATE TABLE `ivo_wishes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `ivo_wishes` */

insert  into `ivo_wishes`(`id`,`user_id`,`product_id`) values (1,7,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
