# Sitemap

## Admin Control Panel

1. Dashboard
2. Products
--* All Products
--* Add Product
--* Inventory
3. Categories
--* All Categories
--* Add Category
4. Orders
5. Customers
6. Discount
7. Settings

## Login to implandirect

marikguizot@gmail.com
Guizot1977

## Category Structure

1. Implants by Sistem
--* Interactive System
----* Interactive
------* 3.2mmD SBM
------* 3.7mmD SBM
------* 4.3mmD SBM
------* 5.0mmD SBM

--* Legacy System
----* Legacy 1
----* Legacy 2
----* Legacy 3
----* Legacy 4

--* Tri-lobe System

--* Swish System
----* SwishActive
------* SwishActive 3.3mmD / 3.0mmD
------* SwishActive 4.1mmD / 3.4mmD
------* SwishActive 4.8mmD / 3.4mmD

----* SwishPlus
----* SwishTapered

--* Etc

2. Prosthetics
--* Interactive System
--* Legacy System
--* Tri-lobe System
--* Etc

3. Surgical
4. Attachment Int'l
5. Biomaterials
6. Deka Lasers
7. Education
8. Custom Direct

## Checkout

1. Review Order
2. Select Shipping Method
3. Select Address
4. Review Order
5. Finish Order